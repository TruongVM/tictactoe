package com.cob.tictactoe.basse.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.cob.tictactoe.MainActivity
import com.cob.tictactoe.basse.activities.BaseActivity
import com.cob.tictactoe.basse.interfaces.FragmentHost
import com.cob.tictactoe.basse.transaction.BackHandle
import com.cob.tictactoe.basse.transaction.StyleAnimation
import com.cob.tictactoe.basse.transaction.TransactionManager
import com.cob.tictactoe.utils.AppUtils
import com.cob.tictactoe.utils.LocaleManager
import com.cob.tictactoe.utils.LogUtil


abstract class BaseFragment : Fragment(), BackHandle {
    var transactionManager: TransactionManager? = null
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        LogUtil.d("out side")
        if (context is FragmentHost) {
            LogUtil.d("in side")
            transactionManager = context.getTransactionManager()
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val content: View
        settingLanguage()

        content = inflater.inflate(getContentRes(), container, false)
        return content
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }

    fun settingLanguage() {
        LocaleManager.setUpLocale(activity!!, AppUtils.getCurrentLanguage())
    }


    fun backActionHandle() {
        transactionManager!!.goBack()
    }

    fun hideFloatButton() {
        LogUtil.d("hideFloatButton")
        (activity as MainActivity?)!!.hideFloatButton()
    }

    fun showFloatButton() {
        LogUtil.d("showFloatButton")
        (activity as MainActivity?)!!.showFloatButton()
    }

    fun setEnableDrawer(isEnable: Boolean) {
        LogUtil.d("setEnableDrawer isEnable= $isEnable")
        (activity as MainActivity?)!!.setEnableDrawer(isEnable)
    }

    fun replaceFragment(fragment: Fragment) {
        transactionManager!!.replaceFragment(
            fragment,
            isAddBackStack = true,
            styleAnimation = StyleAnimation.SLIDE_FROM_RIGHT
        )
    }

    fun goBack() {
        transactionManager!!.goBack()
    }

    fun playSoundEffect() {
        (activity as BaseActivity).playSoundEffect()
    }

    fun pauseSoundEffect() {
        (activity as BaseActivity).pauseSoundEffect()
    }


    @LayoutRes
    abstract fun getContentRes(): Int

    fun showMessage(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    fun showMessage(messageID: Int) {
        Toast.makeText(activity, resources.getString(messageID), Toast.LENGTH_SHORT).show()
    }


}
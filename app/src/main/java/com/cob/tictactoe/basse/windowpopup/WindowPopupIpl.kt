package com.cob.tictactoe.basse.windowpopup

interface WindowPopupIpl {

    fun getPopupTitle(): String

    fun handleActionYes(url: String?)

    fun handleActionNo()
}
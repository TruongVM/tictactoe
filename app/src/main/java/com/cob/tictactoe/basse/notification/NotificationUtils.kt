package com.cob.tictactoe.basse.notification

import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Intent
import android.util.Log
import java.util.*


class NotificationUtils {


    fun setNotification(timeInMilliSeconds: Long, activity: Activity, contentNotice: String) {

        if (timeInMilliSeconds > 0) {
            val alarmManager = activity.getSystemService(Activity.ALARM_SERVICE) as AlarmManager
            val alarmIntent = Intent(
                activity.applicationContext,
                AlarmReceiver::class.java
            ) // AlarmReceiver1 = broadcast receiver
            alarmIntent.putExtra("reason", "notification")
            alarmIntent.putExtra("timestamp", timeInMilliSeconds)
            alarmIntent.putExtra("content", contentNotice)

            val calendar = Calendar.getInstance()
            calendar.timeInMillis = timeInMilliSeconds


            val pendingIntent = PendingIntent.getBroadcast(
                activity,
                0,
                alarmIntent,
                0
            )
            Log.d("TruongVM","  "+calendar.timeInMillis +" and pendingIntent= "+pendingIntent)
            alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, pendingIntent)

        }


    }
}

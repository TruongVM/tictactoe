package com.cob.tictactoe.basse.interfaces

interface ActionBarLayout {
    fun getActionBarController(): ActionBarController
}
package com.cob.tictactoe.basse.interfaces

import com.cob.tictactoe.basse.transaction.TransactionManager


interface FragmentHost {
    fun getTransactionManager(): TransactionManager
}
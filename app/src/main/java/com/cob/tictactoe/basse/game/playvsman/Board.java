package com.cob.tictactoe.basse.game.playvsman;

import android.util.Log;

import com.cob.tictactoe.utils.Const;
import com.cob.tictactoe.utils.LogUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;


public class Board {

    private static int GAME_RESULT_WIN = 1;
    private static int GAME_RESULT_FAIL = 2;
    private static int GAME_RESULT_DRAW = 0;
    private int mRowNow = -1;
    private int mColumnNow = -1;
    public final int USER_1 = 1;
    public final int USER_2 = 2;
    private final int SCORE_PER_STEP_USER_1 = 1;
    private final int SCORE_PER_STEP_USER_2 = 4;
    private final int STEP_TO_WIN = 5;

    private PlayStatusListener mPlayStatusListener;

    private int[][] gameBoardArr = new int[Const.NET_SIZE][Const.NET_SIZE];
    private int[][] gameBoardCopyArr;

    private int playCounter = 1;

    public boolean gameOver = false;

    public Board(PlayStatusListener playStatusListener) {
        mPlayStatusListener = playStatusListener;
        copy();
    }

    private void copy() {
        gameBoardCopyArr = new int[Const.NET_SIZE][Const.NET_SIZE];
        for (int i = 0; i < Const.NET_SIZE; i++) {
            for (int j = 0; j < Const.NET_SIZE; j++) {
                gameBoardCopyArr[i][j] = this.gameBoardArr[i][j];
            }
        }

    }


    public boolean isValidMove(int x, int y) {
        return gameBoardArr[x][y] != SCORE_PER_STEP_USER_1 && gameBoardArr[x][y] != SCORE_PER_STEP_USER_2;
    }

    public void placePiece(int position) {

        if (!gameOver) {
            //Log.d("Board","placePiece vaild");
            int score = 0;
            switch (getCurrentPlayer()) {
                case USER_1:
                    score = SCORE_PER_STEP_USER_1;
                    break;
                case USER_2:
                    score = SCORE_PER_STEP_USER_2;
                    break;
            }

            LogUtil.d("position= " + position);
            mRowNow = (int) position / Const.NET_SIZE;
            mColumnNow = (int) position % Const.NET_SIZE;
            LogUtil.d("rowTemp= " + mRowNow);
            LogUtil.d("columnTem= " + mColumnNow);
            gameBoardArr[mRowNow][mColumnNow] = score;
            gameBoardCopyArr[mRowNow][mColumnNow] = score;


            if (getGameResult() != 4) {
                gameOver = true;
                // Log.d("Board","Game Over +Player"+getCurrentPlayer()+"Wins");
            }
            playCounter++;
//            mPlayStatusListener.setBackgroundForCurrentStep(position);
            mPlayStatusListener.soundEffectForEachMove();

        }

    }

    public int getCurrentPlayer() {
        int currentPlayer;
        if (this.playCounter % 2 == 0)
            currentPlayer = USER_2;
        else
            currentPlayer = USER_1;
        return currentPlayer;
    }

    private boolean mShouldContinue;

    public int getGameResult() {

        int gameResult = 4;

        boolean hasZero = false;
        int currentPlayer = getCurrentPlayer();
        mPlayStatusListener.currentTurn(currentPlayer);
        mShouldContinue = true;
        int currentUserScoreTemp = 0;
        int rowTemp = mRowNow;
        int columnTemp = mColumnNow;
        // check diagonalSum from  center to left top
        for (int i = 0; i < STEP_TO_WIN; i++) {
            if (rowTemp >= 0 && columnTemp >= 0) {
                currentUserScoreTemp = getUserScore(currentPlayer, rowTemp, columnTemp, currentUserScoreTemp);
            } else {
                break;
            }
            rowTemp -= 1;
            columnTemp -= 1;
            if (!mShouldContinue) {
                break;
            }

        }


        if (isGotWinner(currentPlayer, currentUserScoreTemp)) {
            return getWinner();
        }
        // check diagonalSum from  center to right bottom
        rowTemp = mRowNow + 1;
        columnTemp = mColumnNow + 1;
        mShouldContinue = true;
        for (int i = 0; i < STEP_TO_WIN; i++) {
            if (rowTemp < Const.NET_SIZE && columnTemp < Const.NET_SIZE) {
                currentUserScoreTemp = getUserScore(currentPlayer, rowTemp, columnTemp, currentUserScoreTemp);
            } else {
                break;
            }
            rowTemp += 1;
            columnTemp += 1;
            if (!mShouldContinue) {
                break;
            }

        }

        if (isGotWinner(currentPlayer, currentUserScoreTemp)) {
            return getWinner();
        }

        // check diagonalSum from  center to right top
        currentUserScoreTemp = 0;
        rowTemp = mRowNow;
        columnTemp = mColumnNow;
        mShouldContinue = true;
        for (int i = 0; i < STEP_TO_WIN; i++) {
            if (rowTemp >= 0 && columnTemp < Const.NET_SIZE) {
                currentUserScoreTemp = getUserScore(currentPlayer, rowTemp, columnTemp, currentUserScoreTemp);
            } else {
                break;
            }
            rowTemp -= 1;
            columnTemp += 1;
            if (!mShouldContinue) {
                break;
            }
        }

        if (isGotWinner(currentPlayer, currentUserScoreTemp)) {
            return getWinner();
        }
        // check diagonalSum from  center to right top

        rowTemp = mRowNow + 1;
        columnTemp = mColumnNow - 1;
        mShouldContinue = true;
        for (int i = 0; i < STEP_TO_WIN; i++) {
            if (rowTemp < Const.NET_SIZE && columnTemp >= 0) {
                currentUserScoreTemp = getUserScore(currentPlayer, rowTemp, columnTemp, currentUserScoreTemp);
            } else {
                break;
            }
            rowTemp += 1;
            columnTemp -= 1;
            if (!mShouldContinue) {
                break;
            }
        }

        if (isGotWinner(currentPlayer, currentUserScoreTemp)) {
            return getWinner();
        }

        // check column from  center to  top
        currentUserScoreTemp = 0;
        rowTemp = mRowNow;
        columnTemp = mColumnNow;
        mShouldContinue = true;
        for (int i = 0; i < STEP_TO_WIN; i++) {
            if (rowTemp >= 0) {
                currentUserScoreTemp = getUserScore(currentPlayer, rowTemp, columnTemp, currentUserScoreTemp);
            } else {
                break;
            }
            rowTemp -= 1;
            if (!mShouldContinue) {
                break;
            }
        }

        if (isGotWinner(currentPlayer, currentUserScoreTemp)) {
            return getWinner();
        }
        // check column from  center to bottom
        rowTemp = mRowNow + 1;
        columnTemp = mColumnNow;
        mShouldContinue = true;
        for (int i = 0; i < STEP_TO_WIN; i++) {
            if (rowTemp < Const.NET_SIZE) {
                currentUserScoreTemp = getUserScore(currentPlayer, rowTemp, columnTemp, currentUserScoreTemp);
            } else {
                break;
            }
            rowTemp += 1;
            if (!mShouldContinue) {
                break;
            }
        }

        if (isGotWinner(currentPlayer, currentUserScoreTemp)) {
            return getWinner();
        }

        // check row from  center to left
        currentUserScoreTemp = 0;
        rowTemp = mRowNow;
        columnTemp = mColumnNow;
        mShouldContinue = true;
        for (int i = 0; i < STEP_TO_WIN; i++) {
            if (columnTemp >= 0) {
                currentUserScoreTemp = getUserScore(currentPlayer, rowTemp, columnTemp, currentUserScoreTemp);
            } else {
                break;
            }
            columnTemp -= 1;
            if (!mShouldContinue) {
                break;
            }
        }

        if (isGotWinner(currentPlayer, currentUserScoreTemp)) {
            return getWinner();
        }
        // check row from  center to right
        columnTemp = mColumnNow + 1;
        rowTemp = mRowNow;
        mShouldContinue = true;
        for (int i = 0; i < STEP_TO_WIN; i++) {
            if (columnTemp < Const.NET_SIZE) {
                currentUserScoreTemp = getUserScore(currentPlayer, rowTemp, columnTemp, currentUserScoreTemp);
            } else {
                break;
            }
            columnTemp += 1;
            if (!mShouldContinue) {
                break;
            }
        }

        if (isGotWinner(currentPlayer, currentUserScoreTemp)) {
            return getWinner();
        }


        //check for draw
        for (int i = 0; i < Const.NET_SIZE; i++) {
            for (int j = 0; j < Const.NET_SIZE; j++) {
                if (gameBoardArr[i][j] == 0) {
                    hasZero = true;
                }
            }
        }

        if (!hasZero) {
            return GAME_RESULT_DRAW;
        }


        return gameResult;
    }

    private int getUserScore(int currentPlayer, int row, int column, int currentScoreTemp) {
        LogUtil.d("currentPlayer= " + currentPlayer + " and gameBoardArr[" + row + "][" + column + "] ");
        if (currentPlayer == USER_1) {
            if (gameBoardArr[row][column] != SCORE_PER_STEP_USER_1) {
                mShouldContinue = false;
            } else if (gameBoardArr[row][column] == SCORE_PER_STEP_USER_1) {
                currentScoreTemp += SCORE_PER_STEP_USER_1;
            }

        } else if (currentPlayer == USER_2) {
            if (gameBoardArr[row][column] != SCORE_PER_STEP_USER_2) {
                mShouldContinue = false;
            } else if (gameBoardArr[row][column] == SCORE_PER_STEP_USER_2) {
                currentScoreTemp += SCORE_PER_STEP_USER_2;
            }
        }
        return currentScoreTemp;
    }

    private boolean isGotWinner(int user, int score) {
        if (user == USER_1 && score == 5) {
            mWinner = USER_1;
            LogUtil.d("User 1 win");
            return true;
        } else if (user == USER_2 && score == 20) {
            mWinner = USER_2;
            LogUtil.d("User 2 win");
            return true;
        } else {
            LogUtil.d("Play continue current user= " + user + " score= " + score);
        }
        return false;
    }

    private int mWinner = -1;

    public int getWinner() {
        mPlayStatusListener.updateScore(mWinner);
        return mWinner;
    }

    public List<Integer> getAvailableSlots() {

        List<Integer> list = new ArrayList<>();

        int flag = 0;
        for (int i = 0; i < Const.NET_SIZE; i++) {
            for (int j = 0; j < Const.NET_SIZE; j++) {
                if (gameBoardArr[i][j] == 0) {
                    list.add(flag);
                }
                flag++;
            }
        }

        //Log.d("Board","AvaliableSolts: "+list.toString());

        return list;
    }

    private List<Integer> defenseList = new ArrayList<>();
    private List<Integer> defenseScore = new ArrayList<>();

    public void findBestDefensePosition(int position, boolean isComTurn) {
        defenseList = new ArrayList<>();
        defenseScore = new ArrayList<>();
        int currentRow = position / Const.NET_SIZE;
        int currentColumn = position % Const.NET_SIZE;
        int SCORE_PER_DUMMY_STEP = 100;
        int alreadyStep;
        // check diagonalSum from  center to left top
        int rowTemp = currentRow - 1;
        boolean hasSpace = false;
        boolean shouldReduceDummyScore = true;
        int columnTemp = currentColumn - 1;
        int rowOpposite;
        int columnOpposite;
        alreadyStep = isComTurn ? -1 : 1;
        rowOpposite = currentRow + 1;
        columnOpposite = currentColumn + 1;
        for (int i = 0; i < STEP_TO_WIN - 1; i++) {
            if (rowOpposite < Const.NET_SIZE && columnOpposite < Const.NET_SIZE) {
                if (gameBoardArr[rowOpposite][columnOpposite] != SCORE_PER_STEP_USER_1
                        && gameBoardArr[rowOpposite][columnOpposite] != SCORE_PER_STEP_USER_2) {
                    break;
                } else if (gameBoardArr[rowOpposite][columnOpposite] == SCORE_PER_STEP_USER_1) {
                    if (isComTurn) {
                        break;
                    } else {
                        alreadyStep += 1;
                    }
                } else if (gameBoardArr[rowOpposite][columnOpposite] == SCORE_PER_STEP_USER_2) {
                    if (isComTurn) {
                        break;
                    } else {
                        alreadyStep -= 1;
                    }
                }
            } else {
                break;
            }
            rowOpposite += 1;
            columnOpposite += 1;
        }
        for (int i = 0; i < STEP_TO_WIN - 1; i++) {
            if (rowTemp >= 0 && columnTemp >= 0) {
                if (gameBoardArr[rowTemp][columnTemp] != 4 && gameBoardArr[rowTemp][columnTemp] != 1) {
                    gameBoardArr[rowTemp][columnTemp] += SCORE_PER_DUMMY_STEP * alreadyStep;

                    defenseList.add(rowTemp * Const.NET_SIZE + columnTemp);
                    defenseScore.add(gameBoardArr[rowTemp][columnTemp]);
                    Log.d("TruongVM", "case 1. Added to list with pos= " + (rowTemp * Const.NET_SIZE + columnTemp) + " SCORE_PER_DUMMY_STEP= " + SCORE_PER_DUMMY_STEP + " alreadyStep= " + alreadyStep + " and score final= " + gameBoardArr[rowTemp][columnTemp]);
                    alreadyStep = isComTurn ? -1 : 1;
                    SCORE_PER_DUMMY_STEP -= 20;
                } else if (gameBoardArr[rowTemp][columnTemp] == SCORE_PER_STEP_USER_1) {
                    if (isComTurn) {
//                        SCORE_PER_DUMMY_STEP -= 20;
                        break;
                    } else {
                        alreadyStep += 1;
                    }
                } else if (gameBoardArr[rowTemp][columnTemp] == SCORE_PER_STEP_USER_2) {
                    if (isComTurn) {
                        alreadyStep -= 1;
                    } else {
//                        SCORE_PER_DUMMY_STEP -= 20;
                        break;
                    }
                    Log.d("TruongVM", "case 1.  SCORE_PER_STEP_USER_2 alreadyStep= " + alreadyStep);
                }
            } else {
                break;
            }
            rowTemp -= 1;
            columnTemp -= 1;

        }
        // check diagonalSum from  center to right bottom
        rowTemp = currentRow + 1;
        columnTemp = currentColumn + 1;
        SCORE_PER_DUMMY_STEP = 100;
        alreadyStep = isComTurn ? -1 : 1;
        rowOpposite = currentRow - 1;
        columnOpposite = currentColumn - 1;
        for (int i = 0; i < STEP_TO_WIN - 1; i++) {
            if (rowOpposite < Const.NET_SIZE && columnOpposite < Const.NET_SIZE) {
                if (gameBoardArr[rowOpposite][columnOpposite] != SCORE_PER_STEP_USER_1
                        && gameBoardArr[rowOpposite][columnOpposite] != SCORE_PER_STEP_USER_2) {
                    break;
                } else if (gameBoardArr[rowOpposite][columnOpposite] == SCORE_PER_STEP_USER_1) {
                    if (isComTurn) {
                        break;
                    } else {
                        alreadyStep += 1;
                    }
                } else if (gameBoardArr[rowOpposite][columnOpposite] == SCORE_PER_STEP_USER_2) {
                    if (isComTurn) {
                        break;
                    } else {
                        alreadyStep -= 1;
                    }
                }
            } else {
                break;
            }
            rowOpposite -= 1;
            columnOpposite -= 1;
        }
        for (int i = 0; i < STEP_TO_WIN - 1; i++) {
            if (rowTemp < Const.NET_SIZE && columnTemp < Const.NET_SIZE) {
                if (gameBoardArr[rowTemp][columnTemp] != 4 && gameBoardArr[rowTemp][columnTemp] != 1) {
                    gameBoardArr[rowTemp][columnTemp] += SCORE_PER_DUMMY_STEP * alreadyStep;

                    defenseList.add(rowTemp * Const.NET_SIZE + columnTemp);
                    defenseScore.add(gameBoardArr[rowTemp][columnTemp]);
                    Log.d("TruongVM", "case 2. Added to list with pos= " + (rowTemp * Const.NET_SIZE + columnTemp) + " SCORE_PER_DUMMY_STEP= " + SCORE_PER_DUMMY_STEP + " alreadyStep= " + alreadyStep + " and score final= " + gameBoardArr[rowTemp][columnTemp]);
                    alreadyStep = isComTurn ? -1 : 1;
                    SCORE_PER_DUMMY_STEP -= 20;
                } else if (gameBoardArr[rowTemp][columnTemp] == SCORE_PER_STEP_USER_1) {
                    if (isComTurn) {
//                        SCORE_PER_DUMMY_STEP -= 20;
                        break;
                    } else {
                        alreadyStep += 1;
                    }
                } else if (gameBoardArr[rowTemp][columnTemp] == SCORE_PER_STEP_USER_2) {
                    if (isComTurn) {
                        alreadyStep -= 1;
                    } else {
//                        SCORE_PER_DUMMY_STEP -= 20;
                        break;
                    }
                    Log.d("TruongVM", "case 2.  SCORE_PER_STEP_USER_2 alreadyStep= " + alreadyStep);
                }
            } else {
                break;
            }
            rowTemp += 1;
            columnTemp += 1;

        }

        // check diagonalSum from  center to right top
        rowTemp = currentRow - 1;
        columnTemp = currentColumn + 1;
        SCORE_PER_DUMMY_STEP = 100;
        alreadyStep = isComTurn ? -1 : 1;

        rowOpposite = currentRow + 1;
        columnOpposite = currentColumn - 1;
        for (int i = 0; i < STEP_TO_WIN - 1; i++) {
            if (rowOpposite < Const.NET_SIZE && columnOpposite >= 0) {
                if (gameBoardArr[rowOpposite][columnOpposite] != SCORE_PER_STEP_USER_1
                        && gameBoardArr[rowOpposite][columnOpposite] != SCORE_PER_STEP_USER_2) {
                    break;
                } else if (gameBoardArr[rowOpposite][columnOpposite] == SCORE_PER_STEP_USER_1) {
                    if (isComTurn) {
                        break;
                    } else {
                        alreadyStep += 1;
                    }
                } else if (gameBoardArr[rowOpposite][columnOpposite] == SCORE_PER_STEP_USER_2) {
                    if (isComTurn) {
                        break;
                    } else {
                        alreadyStep -= 1;
                    }
                }
            } else {
                break;
            }
            rowOpposite += 1;
            columnOpposite -= 1;
        }
        for (int i = 0; i < STEP_TO_WIN - 1; i++) {
            if (rowTemp >= 0 && columnTemp < Const.NET_SIZE) {
                if (gameBoardArr[rowTemp][columnTemp] != 4 && gameBoardArr[rowTemp][columnTemp] != 1) {
                    gameBoardArr[rowTemp][columnTemp] += SCORE_PER_DUMMY_STEP * alreadyStep;

                    defenseList.add(rowTemp * Const.NET_SIZE + columnTemp);
                    defenseScore.add(gameBoardArr[rowTemp][columnTemp]);
                    Log.d("TruongVM", "case 3. Added to list with pos= " + (rowTemp * Const.NET_SIZE + columnTemp) + " SCORE_PER_DUMMY_STEP= " + SCORE_PER_DUMMY_STEP + " alreadyStep= " + alreadyStep + " and score final= " + gameBoardArr[rowTemp][columnTemp]);
                    alreadyStep = isComTurn ? -1 : 1;
                    SCORE_PER_DUMMY_STEP -= 20;
                } else if (gameBoardArr[rowTemp][columnTemp] == SCORE_PER_STEP_USER_1) {
                    if (isComTurn) {
//                        SCORE_PER_DUMMY_STEP -= 20;
                        break;
                    } else {
                        alreadyStep += 1;
                    }
                } else if (gameBoardArr[rowTemp][columnTemp] == SCORE_PER_STEP_USER_2) {
                    if (isComTurn) {
                        alreadyStep -= 1;
                    } else {
//                        SCORE_PER_DUMMY_STEP -= 20;
                        break;
                    }
                    Log.d("TruongVM", "case 3.  SCORE_PER_STEP_USER_2 alreadyStep= " + alreadyStep);
                }
            } else {
                break;
            }
            rowTemp -= 1;
            columnTemp += 1;

        }

        // check diagonalSum from  center to left bottom
        rowTemp = currentRow + 1;
        columnTemp = currentColumn - 1;
        SCORE_PER_DUMMY_STEP = 100;
        alreadyStep = isComTurn ? -1 : 1;

        rowOpposite = currentRow - 1;
        columnOpposite = currentColumn + 1;
        for (int i = 0; i < STEP_TO_WIN - 1; i++) {
            if (rowOpposite >= 0 && columnOpposite < Const.NET_SIZE) {
                if (gameBoardArr[rowOpposite][columnOpposite] != SCORE_PER_STEP_USER_1
                        && gameBoardArr[rowOpposite][columnOpposite] != SCORE_PER_STEP_USER_2) {
                    break;
                } else if (gameBoardArr[rowOpposite][columnOpposite] == SCORE_PER_STEP_USER_1) {
                    if (isComTurn) {
                        break;
                    } else {
                        alreadyStep += 1;
                    }
                } else if (gameBoardArr[rowOpposite][columnOpposite] == SCORE_PER_STEP_USER_2) {
                    if (isComTurn) {
                        break;
                    } else {
                        alreadyStep -= 1;
                    }
                }
            } else {
                break;
            }
            rowOpposite -= 1;
            columnOpposite += 1;
        }
        for (int i = 0; i < STEP_TO_WIN - 1; i++) {
            if (rowTemp < Const.NET_SIZE && columnTemp >= 0) {
                if (gameBoardArr[rowTemp][columnTemp] != 4 && gameBoardArr[rowTemp][columnTemp] != 1) {
                    gameBoardArr[rowTemp][columnTemp] += SCORE_PER_DUMMY_STEP * alreadyStep;

                    defenseList.add(rowTemp * Const.NET_SIZE + columnTemp);
                    defenseScore.add(gameBoardArr[rowTemp][columnTemp]);
                    Log.d("TruongVM", "case 4. Added to list with pos= " + (rowTemp * Const.NET_SIZE + columnTemp) + " SCORE_PER_DUMMY_STEP= " + SCORE_PER_DUMMY_STEP + " alreadyStep= " + alreadyStep + " and score final= " + gameBoardArr[rowTemp][columnTemp]);
                    alreadyStep = isComTurn ? -1 : 1;
                    SCORE_PER_DUMMY_STEP -= 20;
                } else if (gameBoardArr[rowTemp][columnTemp] == SCORE_PER_STEP_USER_1) {
                    if (isComTurn) {
//                        SCORE_PER_DUMMY_STEP -= 20;
                        break;
                    } else {
                        alreadyStep += 1;
                    }
                } else if (gameBoardArr[rowTemp][columnTemp] == SCORE_PER_STEP_USER_2) {
                    if (isComTurn) {
                        alreadyStep -= 1;
                    } else {
//                        SCORE_PER_DUMMY_STEP -= 20;
                        break;
                    }
                    Log.d("TruongVM", "case 4.  SCORE_PER_STEP_USER_2 alreadyStep= " + alreadyStep);
                }
            } else {
                break;
            }
            rowTemp += 1;
            columnTemp -= 1;

        }

        // check column from  center to  top
        rowTemp = currentRow - 1;
        columnTemp = currentColumn;
        SCORE_PER_DUMMY_STEP = 100;
        alreadyStep = isComTurn ? -1 : 1;

        rowOpposite = currentRow + 1;
        columnOpposite = currentColumn;
        for (int i = 0; i < STEP_TO_WIN - 1; i++) {
            if (rowOpposite < Const.NET_SIZE) {
                if (gameBoardArr[rowOpposite][columnOpposite] != SCORE_PER_STEP_USER_1
                        && gameBoardArr[rowOpposite][columnOpposite] != SCORE_PER_STEP_USER_2) {
                    break;
                } else if (gameBoardArr[rowOpposite][columnOpposite] == SCORE_PER_STEP_USER_1) {
                    if (isComTurn) {
                        break;
                    } else {
                        alreadyStep += 1;
                    }
                } else if (gameBoardArr[rowOpposite][columnOpposite] == SCORE_PER_STEP_USER_2) {
                    if (isComTurn) {
                        break;
                    } else {
                        alreadyStep -= 1;
                    }
                }
            } else {
                break;
            }
            rowOpposite += 1;
        }
        for (int i = 0; i < STEP_TO_WIN - 1; i++) {
            if (rowTemp >= 0 && columnTemp >= 0 && columnTemp < Const.NET_SIZE) {
                if (gameBoardArr[rowTemp][columnTemp] != 4 && gameBoardArr[rowTemp][columnTemp] != 1) {
                    gameBoardArr[rowTemp][columnTemp] += SCORE_PER_DUMMY_STEP * alreadyStep;

                    defenseList.add(rowTemp * Const.NET_SIZE + columnTemp);
                    defenseScore.add(gameBoardArr[rowTemp][columnTemp]);
                    Log.d("TruongVM", "case 5. Added to list with pos= " + (rowTemp * Const.NET_SIZE + columnTemp) + " SCORE_PER_DUMMY_STEP= " + SCORE_PER_DUMMY_STEP + " alreadyStep= " + alreadyStep + " and score final= " + gameBoardArr[rowTemp][columnTemp]);
                    alreadyStep = isComTurn ? -1 : 1;
                    SCORE_PER_DUMMY_STEP -= 20;
                } else if (gameBoardArr[rowTemp][columnTemp] == SCORE_PER_STEP_USER_1) {
                    if (isComTurn) {
//                        SCORE_PER_DUMMY_STEP -= 20;
                        break;
                    } else {
                        alreadyStep += 1;
                    }
                } else if (gameBoardArr[rowTemp][columnTemp] == SCORE_PER_STEP_USER_2) {
                    if (isComTurn) {
                        alreadyStep -= 1;
                    } else {
//                        SCORE_PER_DUMMY_STEP -= 20;
                        break;
                    }
                    Log.d("TruongVM", "case 5.  SCORE_PER_STEP_USER_2 alreadyStep= " + alreadyStep);
                }
            } else {
                break;
            }
            rowTemp -= 1;

        }
        // check column from  center to  bottom
        rowTemp = currentRow + 1;
        columnTemp = currentColumn;
        SCORE_PER_DUMMY_STEP = 100;
        alreadyStep = isComTurn ? -1 : 1;


        rowOpposite = currentRow - 1;
        columnOpposite = currentColumn;
        for (int i = 0; i < STEP_TO_WIN - 1; i++) {
            if (rowOpposite >= 0) {
                if (gameBoardArr[rowOpposite][columnOpposite] != SCORE_PER_STEP_USER_1
                        && gameBoardArr[rowOpposite][columnOpposite] != SCORE_PER_STEP_USER_2) {
                    Log.d("TruongVM", "case 6.1 break");
                    break;
                } else if (gameBoardArr[rowOpposite][columnOpposite] == SCORE_PER_STEP_USER_1) {
                    if (isComTurn) {
                        Log.d("TruongVM", "case 6.2 break");
                        break;
                    } else {
                        alreadyStep += 1;
                        Log.d("TruongVM", "case 6.3 alreadyStep= " + alreadyStep);
                    }
                } else if (gameBoardArr[rowOpposite][columnOpposite] == SCORE_PER_STEP_USER_2) {
                    if (isComTurn) {
                        Log.d("TruongVM", "case 6.4 break");
                        break;
                    } else {
                        alreadyStep -= 1;
                        Log.d("TruongVM", "case 6.5 alreadyStep= " + alreadyStep);
                    }
                }
            } else {
                break;
            }
            rowOpposite -= 1;
        }

        for (int i = 0; i < STEP_TO_WIN - 1; i++) {
            if (rowTemp < Const.NET_SIZE && columnTemp >= 0 && columnTemp < Const.NET_SIZE) {
                if (gameBoardArr[rowTemp][columnTemp] != 4 && gameBoardArr[rowTemp][columnTemp] != 1) {
                    gameBoardArr[rowTemp][columnTemp] += SCORE_PER_DUMMY_STEP * alreadyStep;

                    defenseList.add(rowTemp * Const.NET_SIZE + columnTemp);
                    defenseScore.add(gameBoardArr[rowTemp][columnTemp]);
                    Log.d("TruongVM", "case 6. Added to list with pos= " + (rowTemp * Const.NET_SIZE + columnTemp) + " SCORE_PER_DUMMY_STEP= " + SCORE_PER_DUMMY_STEP + " alreadyStep= " + alreadyStep + " and score final= " + gameBoardArr[rowTemp][columnTemp]);
                    alreadyStep = isComTurn ? -1 : 1;
                    SCORE_PER_DUMMY_STEP -= 20;
                } else if (gameBoardArr[rowTemp][columnTemp] == SCORE_PER_STEP_USER_1) {
                    if (isComTurn) {
//                        SCORE_PER_DUMMY_STEP -= 20;
                        break;
                    } else {
                        alreadyStep += 1;
                    }
                } else if (gameBoardArr[rowTemp][columnTemp] == SCORE_PER_STEP_USER_2) {
                    if (isComTurn) {
                        alreadyStep -= 1;
                    } else {
//                        SCORE_PER_DUMMY_STEP -= 20;
                        break;
                    }
                    Log.d("TruongVM", "case 6.  SCORE_PER_STEP_USER_2 alreadyStep= " + alreadyStep);
                }
            } else {
                break;
            }
            rowTemp += 1;

        }

        // check column from  center to  left
        rowTemp = currentRow;
        columnTemp = currentColumn - 1;
        SCORE_PER_DUMMY_STEP = 100;
        alreadyStep = isComTurn ? -1 : 1;

        rowOpposite = currentRow;
        columnOpposite = currentColumn + 1;
        for (int i = 0; i < STEP_TO_WIN - 1; i++) {
            if (columnOpposite < Const.NET_SIZE) {
                if (gameBoardArr[rowOpposite][columnOpposite] != SCORE_PER_STEP_USER_1
                        && gameBoardArr[rowOpposite][columnOpposite] != SCORE_PER_STEP_USER_2) {
                    break;
                } else if (gameBoardArr[rowOpposite][columnOpposite] == SCORE_PER_STEP_USER_1) {
                    if (isComTurn) {
                        break;
                    } else {
                        alreadyStep += 1;
                    }
                } else if (gameBoardArr[rowOpposite][columnOpposite] == SCORE_PER_STEP_USER_2) {
                    if (isComTurn) {
                        break;
                    } else {
                        alreadyStep -= 1;
                    }
                }
            } else {
                break;
            }
            columnOpposite += 1;
        }
        for (int i = 0; i < STEP_TO_WIN - 1; i++) {
            if (rowTemp < Const.NET_SIZE && rowTemp >= 0 && columnTemp >= 0) {
                if (gameBoardArr[rowTemp][columnTemp] != 4 && gameBoardArr[rowTemp][columnTemp] != 1) {
                    gameBoardArr[rowTemp][columnTemp] += SCORE_PER_DUMMY_STEP * alreadyStep;

                    defenseList.add(rowTemp * Const.NET_SIZE + columnTemp);
                    defenseScore.add(gameBoardArr[rowTemp][columnTemp]);
                    Log.d("TruongVM", "case 7. Added to list with pos= " + (rowTemp * Const.NET_SIZE + columnTemp) + " SCORE_PER_DUMMY_STEP= " + SCORE_PER_DUMMY_STEP + " alreadyStep= " + alreadyStep + " and score final= " + gameBoardArr[rowTemp][columnTemp]);
                    alreadyStep = isComTurn ? -1 : 1;
                    SCORE_PER_DUMMY_STEP -= 20;
                } else if (gameBoardArr[rowTemp][columnTemp] == SCORE_PER_STEP_USER_1) {
                    if (isComTurn) {
//                        SCORE_PER_DUMMY_STEP -= 20;
                        break;
                    } else {
                        alreadyStep += 1;
                    }
                } else if (gameBoardArr[rowTemp][columnTemp] == SCORE_PER_STEP_USER_2) {
                    if (isComTurn) {
                        alreadyStep -= 1;
                    } else {
//                        SCORE_PER_DUMMY_STEP -= 20;
                        break;
                    }
                    Log.d("TruongVM", "case 7.  SCORE_PER_STEP_USER_2 alreadyStep= " + alreadyStep);
                }
            } else {
                break;
            }
            columnTemp -= 1;

        }

        // check column from  center to  right
        rowTemp = currentRow;
        columnTemp = currentColumn + 1;
        SCORE_PER_DUMMY_STEP = 100;
        alreadyStep = isComTurn ? -1 : 1;


        rowOpposite = currentRow;
        columnOpposite = currentColumn - 1;
        for (int i = 0; i < STEP_TO_WIN - 1; i++) {
            if (columnOpposite >= 0) {
                if (gameBoardArr[rowOpposite][columnOpposite] != SCORE_PER_STEP_USER_1
                        && gameBoardArr[rowOpposite][columnOpposite] != SCORE_PER_STEP_USER_2) {
                    break;
                } else if (gameBoardArr[rowOpposite][columnOpposite] == SCORE_PER_STEP_USER_1) {
                    if (isComTurn) {
                        break;
                    } else {
                        alreadyStep += 1;
                    }
                } else if (gameBoardArr[rowOpposite][columnOpposite] == SCORE_PER_STEP_USER_2) {
                    if (isComTurn) {
                        break;
                    } else {
                        alreadyStep -= 1;
                    }
                }
            } else {
                break;
            }
            columnOpposite -= 1;
        }
        for (int i = 0; i < STEP_TO_WIN - 1; i++) {
            if (rowTemp < Const.NET_SIZE && rowTemp >= 0 && columnTemp < Const.NET_SIZE) {
                if (gameBoardArr[rowTemp][columnTemp] != 4 && gameBoardArr[rowTemp][columnTemp] != 1) {
                    gameBoardArr[rowTemp][columnTemp] += SCORE_PER_DUMMY_STEP * alreadyStep;

                    defenseList.add(rowTemp * Const.NET_SIZE + columnTemp);
                    defenseScore.add(gameBoardArr[rowTemp][columnTemp]);
                    Log.d("TruongVM", "case 8. Added to list with pos= " + (rowTemp * Const.NET_SIZE + columnTemp) + " SCORE_PER_DUMMY_STEP= " + SCORE_PER_DUMMY_STEP + " alreadyStep= " + alreadyStep + " and score final= " + gameBoardArr[rowTemp][columnTemp]);
                    alreadyStep = isComTurn ? -1 : 1;
                    SCORE_PER_DUMMY_STEP -= 20;
                } else if (gameBoardArr[rowTemp][columnTemp] == SCORE_PER_STEP_USER_1) {
                    if (isComTurn) {
//                        SCORE_PER_DUMMY_STEP -= 20;
                        break;
                    } else {
                        alreadyStep += 1;
                    }
                } else if (gameBoardArr[rowTemp][columnTemp] == SCORE_PER_STEP_USER_2) {
                    if (isComTurn) {
                        alreadyStep -= 1;
                    } else {
//                        SCORE_PER_DUMMY_STEP -= 20;
                        break;
                    }
                    Log.d("TruongVM", "case 8.  SCORE_PER_STEP_USER_2 alreadyStep= " + alreadyStep);
                }
            } else {
                break;
            }

            columnTemp += 1;

        }


    }

    public int bestPosition() {
        int maxScoreTemp = Collections.max(defenseScore);
        Log.d("Nhinh", "maxScoreTemp= " + maxScoreTemp);
        int defenseListSize = defenseList.size();
        List<Integer> shouldBeatList = new ArrayList<>();
        Log.d("Nhinh", "defenseListSize= " + defenseListSize);
        for (int i = 0; i < defenseListSize; i++) {
            int tempPosition = defenseList.get(i);
            int tempRow = tempPosition / Const.NET_SIZE;
            int tempColumn = tempPosition % Const.NET_SIZE;
            if (maxScoreTemp == gameBoardArr[tempRow][tempColumn]) {
                shouldBeatList.add(defenseList.get(i));
            }
        }
//        for (int i = 0; i < shouldBeatList.size(); i++) {
//            Log.d("Nhinh", "should beat at " + shouldBeatList.get(i));
//        }
        Random random = new Random();
        int shouldBeatListSize = shouldBeatList.size();
        Log.d("Nhinh", " and size= " + shouldBeatListSize);
        int pos = shouldBeatList.get(random.nextInt(shouldBeatListSize));
        Log.d("Nhinh", "bes pos= " + pos + "\n\n\n");
        return pos;
    }


    public int[][] getGameBoardArr() {
        return gameBoardArr;
    }

    public interface PlayStatusListener {
        void currentTurn(int currentUser);

        void updateScore(int winner);

        void setBackgroundForCurrentStep(int position);

        void soundEffectForEachMove();
    }

}

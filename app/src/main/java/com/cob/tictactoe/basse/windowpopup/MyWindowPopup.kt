package com.cob.tictactoe.basse.windowpopup

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface

class MyWindowPopup(var windowPopupIpl: WindowPopupIpl) {

    fun displayDeleteAudioPopup(activity: Activity, url: String?) {
        lateinit var dialog: AlertDialog
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(windowPopupIpl.getPopupTitle())
        builder.setMessage("")
        val dialogClickListener = DialogInterface.OnClickListener { _, which ->
            run {
                when (which) {
                    DialogInterface.BUTTON_POSITIVE -> {
                        if (url == null) {
                            windowPopupIpl.handleActionYes("")
                        } else {
                            windowPopupIpl.handleActionYes(url)
                        }

                    }
                    DialogInterface.BUTTON_NEGATIVE -> {
                        windowPopupIpl.handleActionNo()
                    }
                }
            }

        }
        builder.setPositiveButton("Yes", dialogClickListener)
        builder.setNegativeButton("No", dialogClickListener)


        dialog = builder.create()
        dialog.show()
    }

}
package com.cob.tictactoe.basse.activities

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.cob.tictactoe.basse.effect.BackgroundSoundEffect
import com.cob.tictactoe.utils.LogUtil

@SuppressLint("Registered")
open class BaseGameActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState, persistentState)
        LogUtil.d("onCreate")
        BackgroundSoundEffect.getInstance().playSoundEffect()
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }

    override fun onPause() {
        super.onPause()
        LogUtil.d("onPause")
        BackgroundSoundEffect.getInstance().pauseSoundEffect()
    }

    override fun onDestroy() {
        super.onDestroy()
        LogUtil.d("onDestroy")
        BackgroundSoundEffect.getInstance().releaseSoundEffect()
    }

    fun showToast(message: Int) {
        Toast.makeText(this, resources.getString(message), Toast.LENGTH_SHORT).show()
    }
}
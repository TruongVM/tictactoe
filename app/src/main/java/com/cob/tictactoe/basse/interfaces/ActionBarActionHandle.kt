package com.cob.tictactoe.basse.interfaces


interface ActionBarActionHandle {
    fun doBack(): Boolean
    fun getTitle(): String
    fun leftResource(): Int
    fun doAction(actionCode: Int)
    fun setTitle(): String
}
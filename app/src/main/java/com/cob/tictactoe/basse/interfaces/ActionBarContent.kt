package com.cob.tictactoe.basse.interfaces


interface ActionBarContent {
    fun setTitle(title: String)
    fun init(actionBarContent: ActionBarLayout)
}
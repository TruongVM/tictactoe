package com.cob.tictactoe.basse.interfaces

import android.view.ViewGroup
import androidx.annotation.LayoutRes


interface ActionBarController {
    @LayoutRes
    fun getActionBarLayout(): Int

    fun findViews(parent: ViewGroup)
    fun setUpViews()
    fun setTitle(title: String)
}
package com.cob.tictactoe.basse.effect

import android.content.Context
import android.media.MediaPlayer
import com.cob.tictactoe.R

class SoundEffect private constructor() {
    private lateinit var mMediaPlayer: MediaPlayer

    private object Holder {
        val INSTANCE = SoundEffect()
    }


    companion object {
        fun getInstance(): SoundEffect {
            return Holder.INSTANCE
        }
    }

    fun initPlayer(context: Context) {
        mMediaPlayer = MediaPlayer.create(context, R.raw.effect_sound)
        mMediaPlayer.isLooping = false
        mMediaPlayer.setVolume(0.1F, 0.1F)
    }

    fun playSoundEffect() {
        if (mMediaPlayer.isPlaying) {
            mMediaPlayer.stop()
        }
        mMediaPlayer.start()
    }

    fun pauseSoundEffect() {
        if (mMediaPlayer.isPlaying) {
            mMediaPlayer.stop()
        }
    }

    fun releaseSoundEffect() {
        if (mMediaPlayer.isPlaying) {
            mMediaPlayer.release()
        }
    }

}
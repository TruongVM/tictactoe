package com.cob.tictactoe.basse.activities

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.cob.tictactoe.basse.effect.BackgroundSoundEffect
import com.cob.tictactoe.basse.interfaces.FragmentHost
import com.cob.tictactoe.basse.transaction.BackHandle
import com.cob.tictactoe.basse.transaction.TransactionManager
import com.cob.tictactoe.basse.transaction.TransactionManagerImpl
import com.cob.tictactoe.utils.LogUtil


@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity(), FragmentHost {

    var tm: TransactionManager? = null

    override fun getTransactionManager(): TransactionManager {
        if (tm == null)
            tm = TransactionManagerImpl(this, supportFragmentManager, getPlaceHolder())
        Log.d("TruongVM", "getTransactionManager tm= $tm")
        return tm!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        LogUtil.d("onCreate")
        BackgroundSoundEffect.getInstance().playSoundEffect()
    }

    open fun getPlaceHolder(): Int = 0

    override fun onDestroy() {
        super.onDestroy()
        tm?.dispose()
        tm = null
        LogUtil.d("onDestroy")
        BackgroundSoundEffect.getInstance().releaseSoundEffect()
    }


    override fun onBackPressed() {
        if (!backActionHandle()) {
            super.onBackPressed()
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (backActionHandle()) {
                return true
            }
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun backActionHandle(): Boolean {
        var isHandled = false
        val activePage = getTransactionManager().fragmentActive
        if (activePage is BackHandle) {
            isHandled = activePage.actionBack()
        }
        if (!isHandled) isHandled = getTransactionManager().goBack()
        return isHandled
    }

    fun playSoundEffect() {
        BackgroundSoundEffect.getInstance().playSoundEffect()
    }

    fun pauseSoundEffect() {
        BackgroundSoundEffect.getInstance().pauseSoundEffect()
    }
}
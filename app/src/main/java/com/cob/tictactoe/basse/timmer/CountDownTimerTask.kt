package com.cob.tictactoe.basse.timmer

import android.os.CountDownTimer
import android.util.Log

class CountDownTimerTask(var updateUi: UpdateFromTimer) {
    lateinit var mCountDownTimer: CountDownTimer
    private val startMilliSecond = 20000L
    var isRunning = false
    private var timeInMilliSecond = 0L
    private val mSecond = 20L
    private var isTimerCreated = false

    companion object {
        var MAX_PROGRESS = 20000
    }

    fun startTimer() {
        timeInMilliSecond = MAX_PROGRESS.toLong()

        mCountDownTimer = object : CountDownTimer(timeInMilliSecond, mSecond) {
            override fun onFinish() {
                updateUi.timerFinish()
            }

            override fun onTick(millisUntilFinished: Long) {
                updateUi.updateUIFromTimer(millisUntilFinished)
            }
        }
        isTimerCreated = true

        mCountDownTimer.start()
    }

    fun stopTimer() {
        if (isTimerCreated) {
            mCountDownTimer.cancel()
        }

    }

    fun stopCountDown() {
        if (isTimerCreated) {
            mCountDownTimer.cancel()
            updateUi.cancelCountDownTimer()
        }

    }

    fun resetCountDown() {
        timeInMilliSecond = startMilliSecond
    }


    interface UpdateFromTimer {
        fun updateUIFromTimer(progress: Long)
        fun timerFinish()
        fun cancelCountDownTimer() {}
    }
}
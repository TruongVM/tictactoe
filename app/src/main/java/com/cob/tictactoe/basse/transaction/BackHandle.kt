package com.cob.tictactoe.basse.transaction


interface BackHandle {
    fun actionBack(): Boolean
}
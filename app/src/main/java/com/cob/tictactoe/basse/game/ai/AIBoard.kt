package com.cob.tictactoe.basse.game.ai

import android.util.Log
import com.cob.tictactoe.utils.Const
import java.util.*
import kotlin.collections.ArrayList

class AIBoard(var playStatusListener: PlayStatusListener) {
    private var mAttachList: ArrayList<Int>
    private var mDefenseList: ArrayList<Int>

    var mArrGameBoard = ArrayList<Int>()

    private var heuristic: ArrayList<Int>
    private var random: ArrayList<Int>
    var mUnDoList: ArrayList<Int>
    var mReDoList: ArrayList<Int>
    var mCheck = 0
    private var isPlayerMoved = false

    companion object {
        const val STEP_TO_WIN = 5
    }

    init {
        for (i in 0 until Const.NET_SIZE * Const.NET_SIZE) {
            mArrGameBoard.add(0)
        }
        mAttachList = ArrayList()
        mAttachList.add(0)
        mAttachList.add(3)
        mAttachList.add(24)
        mAttachList.add(192)
        mAttachList.add(1536)
        mAttachList.add(12288)
        mAttachList.add(98304)
        mAttachList.add(786432)

        mDefenseList = ArrayList()
        mDefenseList.add(0)
        mDefenseList.add(1)
        mDefenseList.add(9)
        mDefenseList.add(81)
        mDefenseList.add(729)
        mDefenseList.add(6561)
        mDefenseList.add(59849)
        mDefenseList.add(538641)

        heuristic = ArrayList()
        random = ArrayList()
        mUnDoList = ArrayList()
        mReDoList = ArrayList()
    }

    fun unDo(): Boolean {
        if (mUnDoList.size > 1 && mUnDoList[mUnDoList.size - 1] >= 0) {
            mArrGameBoard[mUnDoList[mUnDoList.size - 1]] = 0
            mArrGameBoard[mUnDoList[mUnDoList.size - 2]] = 0
            mReDoList.add(mUnDoList[mUnDoList.size - 1])
            mReDoList.add(mUnDoList[mUnDoList.size - 2])
            mUnDoList.removeAt(mUnDoList.size - 1)
            mUnDoList.removeAt(mUnDoList.size - 1)
            return true
        } else {
            return false
        }
    }

    fun reDo() {
        if (mReDoList.size > 0) {
            mArrGameBoard[mReDoList[mReDoList.size - 1]] = -1
            mUnDoList.add(mReDoList[mReDoList.size - 1])
            mReDoList.removeAt(mReDoList.size - 1)
            mArrGameBoard[mReDoList[mReDoList.size - 1]] = 1
            mUnDoList.add(mReDoList.get(mReDoList.size - 1))
            mReDoList.removeAt(mReDoList.size - 1)
        }
    }

    fun getReDoListSize(): Int {
        return mReDoList.size
    }

    fun getUnDoListSize(): Int {
        return mUnDoList.size
    }

    fun getCurrentUser(isPlayer1GoFist: Boolean): Int {
        return if (isPlayer1GoFist) {
            if (mUnDoList.size % 2 == 0) {
                Const.PLAYER_1
            } else {
                Const.PLAYER_2
            }
        } else {
            if (mUnDoList.size % 2 == 0) {
                Const.PLAYER_2
            } else {
                Const.PLAYER_1
            }
        }
    }

    fun moveBetweenPlayerVsPlayer(position: Int, player1GoFirst: Boolean) {
        Log.d("moveBetween", "mUnDoList.size= " + mUnDoList.size)
        if (player1GoFirst) {
            if (mUnDoList.size % 2 == 0) {
                Log.d("moveBetween", "oMove")
                oMove(position)

            } else {
                Log.d("moveBetween", "xMove")
                xMove(position)
            }
        } else {
            if (mUnDoList.size % 2 == 0) {
                Log.d("moveBetween", "xMove")
                xMove(position)


            } else {
                Log.d("moveBetween", "oMove")
                oMove(position)
            }
        }

    }

    // for function between player vs player
    private fun oMove(position: Int) {
        mArrGameBoard[position] = -1
        mUnDoList.add(position)
        mCheck = checkRow(position, -1) + checkColumn(position, -1) + checkDiagonalLeftToRight(
            position,
            -1
        ) + checkDiagonalRightToLeft(position, -1)
        if (mCheck < 0) oWin()
    }

    // for function between player vs player
    private fun xMove(position: Int) {
        mArrGameBoard[position] = 1
        mUnDoList.add(position)
        mCheck = checkRow(position, 1) + checkColumn(position, 1) + checkDiagonalLeftToRight(
            position,
            1
        ) + checkDiagonalRightToLeft(position, 1)
        if (mCheck > 0) xWin()
    }

    fun playerMove(position: Int) {
        if (mArrGameBoard[position] == 0) {
            mReDoList.clear()
            mCheck = 0
            mArrGameBoard.set(position, -1)
            mUnDoList.add(position)
            isPlayerMoved = true
        }

    }

    fun aiMove(position: Int) {
//        if (mArrGameBoard[position] == 0 && mUnDoList.size % 2 == 1) {
        if (isPlayerMoved) {
            mCheck =
                checkRow(position, -1) + checkColumn(position, -1) + checkDiagonalLeftToRight(
                    position,
                    -1
                ) + checkDiagonalRightToLeft(position, -1)
            if (mCheck < 0) {
                oWin()
            } else {
                move()
            }
            isPlayerMoved = false
        }

//        }
    }

    private fun move() {
        mCheck = 0
        val position = findPositionToBeatForAI()
        mArrGameBoard[position] = 1
        mUnDoList.add(position)
        mCheck = checkRow(position, 1) + checkColumn(position, 1) + checkDiagonalLeftToRight(
            position,
            1
        ) + checkDiagonalRightToLeft(position, 1)
        if (mCheck > 0) {
            xWin()
        }
    }

    private fun checkNearField(position: Int): Boolean {
        val row = position / Const.NET_SIZE
        val column = position % Const.NET_SIZE
        for (x in row - 1..row + 1) for (y in column - 1..column + 1) if (x in 0..(Const.NET_SIZE - 1) && y >= 0 && y <= (Const.NET_SIZE - 1) && mArrGameBoard[x * Const.NET_SIZE + y] != 0 && x * Const.NET_SIZE + y != position
        ) return true
        return false
    }

    private fun minMax(mini: Int, maxi: Int, depth: Int, isCom: Boolean): Int {
        var mini = mini
        var maxi = maxi
        if (depth == 4) {
            return 0
        }
        return if (isCom) {
            var v = -1000000000
            for (position in mUnDoList[mUnDoList.size - 1] until Const.NET_SIZE * Const.NET_SIZE) {
                if (mArrGameBoard[position] == 0 && checkNearField(position)) {
                    mArrGameBoard[position] = 1
                    mUnDoList.add(position)
                    val check =
                        checkRow(position, 1) + checkColumn(position, 1) + checkDiagonalLeftToRight(
                            position,
                            1
                        ) + checkDiagonalRightToLeft(position, 1)
                    if (check > 0) {
                        mUnDoList.removeAt(mUnDoList.size - 1)
                        mArrGameBoard[position] = 0
                        return 100000000 - depth
                    }
                    v = Math.max(v, minMax(mini, maxi, depth + 1, false))
                    mUnDoList.removeAt(mUnDoList.size - 1)
                    mArrGameBoard[position] = 0
                    if (v >= maxi) {
                        return v
                    }
                    mini = Math.max(mini, v)
                }
            }
            for (position in mUnDoList[mUnDoList.size - 1] downTo 0) {
                if (mArrGameBoard[position] == 0 && checkNearField(position)) {
                    mArrGameBoard[position] = 1
                    mUnDoList.add(position)
                    val check =
                        checkRow(position, 1) + checkColumn(position, 1) + checkDiagonalLeftToRight(
                            position,
                            1
                        ) + checkDiagonalRightToLeft(position, 1)
                    if (check > 0) {
                        mUnDoList.removeAt(mUnDoList.size - 1)
                        mArrGameBoard[position] = 0
                        return 100000000 - depth
                    }
                    v = Math.max(v, minMax(mini, maxi, depth + 1, false))
                    mUnDoList.removeAt(mUnDoList.size - 1)
                    mArrGameBoard[position] = 0
                    if (v >= maxi) {
                        return v
                    }
                    mini = Math.max(mini, v)
                }
            }
            v
        } else {
            var v = 1000000000
            for (position in mUnDoList[mUnDoList.size - 1] until Const.NET_SIZE * Const.NET_SIZE) {
                if (mArrGameBoard[position] == 0 && checkNearField(position)) {
                    mArrGameBoard[position] = -1
                    mUnDoList.add(position)
                    val check = checkRow(position, -1) + checkColumn(
                        position,
                        -1
                    ) + checkDiagonalLeftToRight(position, -1) + checkDiagonalRightToLeft(
                        position,
                        -1
                    )
                    if (check < 0) {
                        mUnDoList.removeAt(mUnDoList.size - 1)
                        mArrGameBoard[position] = 0
                        return -100000000 + depth
                    }
                    v = Math.min(v, minMax(mini, maxi, depth + 1, true))
                    mUnDoList.removeAt(mUnDoList.size - 1)
                    mArrGameBoard[position] = 0
                    if (v <= mini) {
                        return v
                    }
                    maxi = Math.min(maxi, v)
                }
            }
            for (position in mUnDoList[mUnDoList.size - 1] downTo 0) {
                if (mArrGameBoard[position] == 0 && checkNearField(position)) {
                    mArrGameBoard[position] = -1
                    mUnDoList.add(position)
                    val check = checkRow(position, -1) + checkColumn(
                        position,
                        -1
                    ) + checkDiagonalLeftToRight(position, -1) + checkDiagonalRightToLeft(
                        position,
                        -1
                    )
                    if (check < 0) {
                        mUnDoList.removeAt(mUnDoList.size - 1)
                        mArrGameBoard[position] = 0
                        return -100000000 + depth
                    }
                    v = Math.min(v, minMax(mini, maxi, depth + 1, true))
                    mUnDoList.removeAt(mUnDoList.size - 1)
                    mArrGameBoard[position] = 0
                    if (v <= mini) {
                        return v
                    }
                    maxi = Math.min(maxi, v)
                }
            }
            v
        }
    }

    private fun findPositionToBeatForAI(): Int {
        heuristic.clear()
        random.clear()
        for (position in 0 until Const.NET_SIZE * Const.NET_SIZE) if (mArrGameBoard[position] != 0) {
            heuristic.add(0)
        } else {
            heuristic.add(
                Math.max(
                    attachFromLeftToRight(position) + attachFromRightToLeft(position) + attachOnColumn(
                        position
                    ) + attachOnRow(position),
                    defenseFromLeftToRight(position) + defenseFromRightToLeft(position) + defenseOnColumn(
                        position
                    ) + defenseOnRow(position)
                )
            )
        }
        for (position in mUnDoList[mUnDoList.size - 1] until Const.NET_SIZE * Const.NET_SIZE) if (mArrGameBoard[position] == 0 && checkNearField(
                position
            )
        ) {
            mArrGameBoard[position] = 1
            mUnDoList.add(position)
            val check = checkRow(position, 1) + checkColumn(
                position,
                1
            ) + checkDiagonalLeftToRight(position, 1) + checkDiagonalRightToLeft(position, 1)
            if (check > 0) {
                mArrGameBoard[position] = 0
                heuristic[position] = 100000000
                break
            } //neu x win luon return heuristic=100cu;
            val value: Int = minMax(-1010101010, 1010101010, 2, false)
            if (heuristic[position] < value) {
                heuristic[position] = value
            }
            mUnDoList.removeAt(mUnDoList.size - 1)
            mArrGameBoard[position] = 0
        }
        for (position in mUnDoList[mUnDoList.size - 1] downTo 0) if (mArrGameBoard[position] == 0 && checkNearField(
                position
            )
        ) {
            mArrGameBoard[position] = 1
            mUnDoList.add(position)
            val check = checkRow(position, 1) + checkColumn(
                position,
                1
            ) + checkDiagonalLeftToRight(position, 1) + checkDiagonalRightToLeft(position, 1)
            if (check > 0) {
                mArrGameBoard[position] = 0
                heuristic[position] = 100000000
                break
            } //neu x win luon return heuristic=100cu;
            val value: Int = minMax(-1010101010, 1010101010, 2, false)
            if (heuristic[position] < value) {
                heuristic[position] = value
            }
            mUnDoList.removeAt(mUnDoList.size - 1)
            mArrGameBoard[position] = 0
        }
        var max = 0
        for (position in 0 until Const.NET_SIZE * Const.NET_SIZE) {
            if (heuristic[position] > heuristic[max]) max = position
        }
        for (position in 0 until Const.NET_SIZE * Const.NET_SIZE) {
            if (heuristic[position] == heuristic[max]) random.add(position)
        }
        val rd = Random()
        return random[rd.nextInt(random.size)]
    }


    //xTC_cheo2
    private fun attachFromRightToLeft(position: Int): Int {
        var countX = 0
        var countO = 0
        var countAlreadyO = 0
        var countAlreadyX = 0
        run {
            var count = position
            while (count < position + 6 * (Const.NET_SIZE - 1)) {
                if (position / Const.NET_SIZE == (Const.NET_SIZE - 1) || position % Const.NET_SIZE == 0) {
                    break
                }
                if (count == position) {
                    count += (Const.NET_SIZE - 1)
                    continue
                }
                if (mArrGameBoard[count] == 1) {
                    countX++
                } else if (mArrGameBoard[count] == -1) {
                    countO++
                    break
                } else {
                    if (count / Const.NET_SIZE != (Const.NET_SIZE - 1) && count % Const.NET_SIZE != 0 && mArrGameBoard[count + (Const.NET_SIZE - 1)] == -1) {
                        countAlreadyO++
                    }
                    if (count / Const.NET_SIZE != (Const.NET_SIZE - 1) && count % Const.NET_SIZE != 0 && mArrGameBoard[count + (Const.NET_SIZE - 1)] == 1) {
                        countAlreadyX++
                    }
                    break
                }
                if (count / Const.NET_SIZE == (Const.NET_SIZE - 1) || count % Const.NET_SIZE == 0) {
                    break
                }
                count += (Const.NET_SIZE - 1)
            }
        }
        var count = position
        while (count > position - 6 * (Const.NET_SIZE - 1)) {
            if (position / Const.NET_SIZE == 0 || position % Const.NET_SIZE == (Const.NET_SIZE - 1)) {
                break
            }
            if (count == position) {
                count -= (Const.NET_SIZE - 1)
                continue
            }
            if (mArrGameBoard[count] == 1) {
                countX++
            } else if (mArrGameBoard[count] == -1) {
                countO++
                break
            } else {
                if (count / Const.NET_SIZE != 0 && count / Const.NET_SIZE != (Const.NET_SIZE - 1) && mArrGameBoard[count - (Const.NET_SIZE - 1)] == -1) {
                    countAlreadyO++
                }
                if (count / Const.NET_SIZE != 0 && count / Const.NET_SIZE != (Const.NET_SIZE - 1) && mArrGameBoard[count - (Const.NET_SIZE - 1)] == 1) {
                    countAlreadyX++
                }
                break
            }
            if (count / Const.NET_SIZE == 0 || count % Const.NET_SIZE == (Const.NET_SIZE - 1)) {
                break
            }
            count -= (Const.NET_SIZE - 1)
        }
        if (countO == 2 && countX < 5) {
            return 0
        }
        return if (countAlreadyO == 1 && countO == 1 && countX < 4) {
            0
        } else mAttachList[countX] + mAttachList[countAlreadyX] - mDefenseList[countO] - mDefenseList[countAlreadyO]
    }

    //xTC_cheo1
    private fun attachFromLeftToRight(position: Int): Int {
        var countX = 0
        var countO = 0
        var countAlreadyO = 0
        var countAlreadyX = 0
        run {
            var count = position
            while (count < position + 6 * (Const.NET_SIZE + 1)) {
                if (position / Const.NET_SIZE == (Const.NET_SIZE - 1) || position % Const.NET_SIZE == (Const.NET_SIZE - 1)) {
                    break
                }
                if (count == position) {
                    count += (Const.NET_SIZE + 1)
                    continue
                }
                if (mArrGameBoard[count] == 1) {
                    countX++
                } else if (mArrGameBoard[count] == -1) {
                    countO++
                    break
                } else {
                    if (count / Const.NET_SIZE != (Const.NET_SIZE - 1) && count % Const.NET_SIZE != (Const.NET_SIZE - 1) && mArrGameBoard[count + (Const.NET_SIZE + 1)] == -1) {
                        countAlreadyO++
                    }
                    if (count / Const.NET_SIZE != (Const.NET_SIZE - 1) && count % Const.NET_SIZE != (Const.NET_SIZE - 1) && mArrGameBoard[count + (Const.NET_SIZE + 1)] == 1) {
                        countAlreadyX++
                    }
                    break
                }
                if (count / Const.NET_SIZE == (Const.NET_SIZE - 1) || count % Const.NET_SIZE == (Const.NET_SIZE - 1)) {
                    break
                }
                count += (Const.NET_SIZE + 1)
            }
        }
        var count = position
        while (count > position - 6 * (Const.NET_SIZE + 1)) {
            if (position / Const.NET_SIZE == 0 || position % Const.NET_SIZE == 0) {
                break
            }
            if (count == position) {
                count -= (Const.NET_SIZE + 1)
                continue
            }
            if (mArrGameBoard[count] == 1) {
                countX++
            } else if (mArrGameBoard[count] == -1) {
                countO++
                break
            } else {
                if (count / Const.NET_SIZE != 0 && count % Const.NET_SIZE != 0 && mArrGameBoard[count - (Const.NET_SIZE + 1)] == -1) {
                    countAlreadyO++
                }
                if (count / Const.NET_SIZE != 0 && count % Const.NET_SIZE != 0 && mArrGameBoard[count - (Const.NET_SIZE + 1)] == 1) {
                    countAlreadyX++
                }
                break
            }
            if (count / Const.NET_SIZE == 0 || count % Const.NET_SIZE == 0) {
                break
            }
            count -= (Const.NET_SIZE + 1)
        }
        if (countO == 2 && countX < 5) {
            return 0
        }
        return if (countAlreadyO == 1 && countO == 1 && countX < 4) {
            0
        } else mAttachList[countX] + mAttachList[countAlreadyX] - mDefenseList[countO] - mDefenseList[countAlreadyO]
    }

    private fun attachOnColumn(position: Int): Int {
        var countX = 0
        var countO = 0
        var countAlreadyO = 0
        var countAlreadyX = 0
        run {
            var count = position
            while (count < position + 6 * Const.NET_SIZE) {
                if (position / Const.NET_SIZE == (Const.NET_SIZE - 1)) {
                    break
                }
                if (count == position) {
                    count += Const.NET_SIZE
                    continue
                }
                if (mArrGameBoard[count] == 1) {
                    countX++
                } else if (mArrGameBoard[count] == -1) {
                    countO++
                    break
                } else {
                    if (count / Const.NET_SIZE != (Const.NET_SIZE - 1) && mArrGameBoard[count + Const.NET_SIZE] == -1) {
                        countAlreadyO++
                    }
                    if (count / Const.NET_SIZE != (Const.NET_SIZE - 1) && mArrGameBoard[count + Const.NET_SIZE] == 1) {
                        countAlreadyX++
                    }
                    break
                }
                if (count / Const.NET_SIZE == (Const.NET_SIZE - 1)) {
                    break
                }
                count += Const.NET_SIZE
            }
        }
        var count = position
        while (count > position - 6 * Const.NET_SIZE) {
            if (position / Const.NET_SIZE == 0) {
                break
            }
            if (count == position) {
                count -= Const.NET_SIZE
                continue
            }
            if (mArrGameBoard[count] == 1) {
                countX++
            } else if (mArrGameBoard[count] == -1) {
                countO++
                break
            } else {
                if (count / Const.NET_SIZE != 0 && mArrGameBoard[count - Const.NET_SIZE] == -1) {
                    countAlreadyO++
                }
                if (count / Const.NET_SIZE != 0 && mArrGameBoard[count - Const.NET_SIZE] == 1) {
                    countAlreadyX++
                }
                break
            }
            if (count / Const.NET_SIZE == 0) {
                break
            }
            count -= Const.NET_SIZE
        }
        if (countO == 2 && countX < 5) {
            return 0
        }
        return if (countAlreadyO == 1 && countO == 1 && countX < 4) {
            0
        } else mAttachList[countX] + mAttachList[countAlreadyX] - mDefenseList[countO] - mDefenseList[countAlreadyO]
    }

    private fun attachOnRow(position: Int): Int {
        var countX = 0
        var countO = 0
        var countAlreadyO = 0
        var countAlreadyX = 0
        for (count in position until position + 6) {
            if (position % Const.NET_SIZE == (Const.NET_SIZE - 1)) {
                break
            }
            if (count == position) {
                continue
            }
            if (mArrGameBoard[count] == 1) {
                countX++
            } else if (mArrGameBoard[count] == -1) {
                countO++
                break
            } else {
                if (count % Const.NET_SIZE != (Const.NET_SIZE - 1) && mArrGameBoard[count + 1] == -1) {
                    countAlreadyO++
                }
                if (count % Const.NET_SIZE != (Const.NET_SIZE - 1) && mArrGameBoard[count + 1] == 1) {
                    countAlreadyX++
                }
                break
            }
            if (count % Const.NET_SIZE == (Const.NET_SIZE - 1)) {
                break
            }
        }
        for (count in position downTo position - 6 + 1) {
            if (position % Const.NET_SIZE == 0) {
                break
            }
            if (count == position) {
                continue
            }
            if (mArrGameBoard[count] == 1) {
                countX++
            } else if (mArrGameBoard[count] == -1) {
                countO++
                break
            } else {
                if (count % Const.NET_SIZE != 0 && mArrGameBoard[count - 1] == -1) {
                    countAlreadyO++
                }
                if (count % Const.NET_SIZE != 0 && mArrGameBoard[count - 1] == 1) {
                    countAlreadyX++
                }
                break
            }
            if (count % Const.NET_SIZE == 0) {
                break
            }
        }
        if (countO == 2 && countX < 5) {
            return 0
        }
        return if (countAlreadyO == 1 && countO == 1 && countX < 4) {
            0
        } else mAttachList[countX] + mAttachList[countAlreadyX] - mDefenseList[countO] - mDefenseList[countAlreadyO]
    }

    //cheo 2
    private fun defenseFromRightToLeft(position: Int): Int {
        var countX = 0
        var countO = 0
        var countAlreadyX = 0
        var countAlreadyO = 0
        run {
            var count = position
            while (count < position + 6 * (Const.NET_SIZE - 1)) {
                if (position / Const.NET_SIZE == (Const.NET_SIZE - 1) || position % Const.NET_SIZE == 0) {
                    break
                }
                if (count == position) {
                    count += (Const.NET_SIZE - 1)
                    continue
                }
                if (mArrGameBoard[count] == -1) {
                    countO++
                } else if (mArrGameBoard[count] == 1) {
                    countX++
                    break
                } else {
                    if (count / Const.NET_SIZE != (Const.NET_SIZE - 1) && count % Const.NET_SIZE != 0 && mArrGameBoard[count + (Const.NET_SIZE - 1)] == 1) {
                        countAlreadyX++
                    }
                    if (count / Const.NET_SIZE != (Const.NET_SIZE - 1) && count % Const.NET_SIZE != 0 && mArrGameBoard[count + (Const.NET_SIZE - 1)] == -1) {
                        countAlreadyO++
                    }
                    break
                }
                if (count / Const.NET_SIZE == (Const.NET_SIZE - 1) || count % Const.NET_SIZE == 0) {
                    break
                }
                count += (Const.NET_SIZE - 1)
            }
        }
        var count = position
        while (count > position - 6 * (Const.NET_SIZE - 1)) {
            if (position / Const.NET_SIZE == 0 || position % Const.NET_SIZE == (Const.NET_SIZE - 1)) {
                break
            }
            if (count == position) {
                count -= (Const.NET_SIZE - 1)
                continue
            }
            if (mArrGameBoard[count] == -1) {
                countO++
            } else if (mArrGameBoard[count] == 1) {
                countX++
                break
            } else {
                if (count / Const.NET_SIZE != 0 && count / Const.NET_SIZE != (Const.NET_SIZE - 1) && mArrGameBoard[count - (Const.NET_SIZE - 1)] == 1) {
                    countAlreadyX++
                }
                if (count / Const.NET_SIZE != 0 && count / Const.NET_SIZE != (Const.NET_SIZE - 1) && mArrGameBoard[count - (Const.NET_SIZE - 1)] == -1) {
                    countAlreadyO++
                }
                break
            }
            if (count / Const.NET_SIZE == 0 || count % Const.NET_SIZE == (Const.NET_SIZE - 1)) {
                break
            }
            count -= (Const.NET_SIZE - 1)
        }
        if (countX == 2 && countO < 5) {
            return 0
        }
        return if (countAlreadyX == 1 && countX == 1 && countO < 4) {
            0
        } else mDefenseList[countO] + mDefenseList[countAlreadyO] - mAttachList[countX] - mAttachList[countAlreadyX]
    }

    //cheo 1
    private fun defenseFromLeftToRight(position: Int): Int {
        var countX = 0
        var countO = 0
        var countAlreadyX = 0
        var countAlreadyO = 0
        run {
            var count = position
            while (count < position + 6 * (Const.NET_SIZE + 1)) {
                if (position / Const.NET_SIZE == (Const.NET_SIZE - 1) || position % Const.NET_SIZE == (Const.NET_SIZE - 1)) {
                    break
                }
                if (count == position) {
                    count += (Const.NET_SIZE + 1)
                    continue
                }
                if (mArrGameBoard[count] == -1) {
                    countO++
                } else if (mArrGameBoard[count] == 1) {
                    countX++
                    break
                } else {
                    if (count / Const.NET_SIZE != (Const.NET_SIZE - 1) && count % Const.NET_SIZE != (Const.NET_SIZE - 1) && mArrGameBoard[count + (Const.NET_SIZE + 1)] == 1) {
                        countAlreadyX++
                    }
                    if (count / Const.NET_SIZE != (Const.NET_SIZE - 1) && count % Const.NET_SIZE != (Const.NET_SIZE - 1) && mArrGameBoard[count + (Const.NET_SIZE + 1)] == -1) {
                        countAlreadyO++
                    }
                    break
                }
                if (count / Const.NET_SIZE == (Const.NET_SIZE - 1) || count % Const.NET_SIZE == (Const.NET_SIZE - 1)) {
                    break
                }
                count += (Const.NET_SIZE + 1)
            }
        }
        var count = position
        while (count > position - 6 * (Const.NET_SIZE + 1)) {
            if (position / Const.NET_SIZE == 0 || position % Const.NET_SIZE == 0) {
                break
            }
            if (count == position) {
                count -= (Const.NET_SIZE + 1)
                continue
            }
            if (mArrGameBoard[count] == -1) {
                countO++
            } else if (mArrGameBoard[count] == 1) {
                countX++
                break
            } else {
                if (count / Const.NET_SIZE != 0 && count % Const.NET_SIZE != 0 && mArrGameBoard[count - (Const.NET_SIZE + 1)] == 1) {
                    countAlreadyX++
                }
                if (count / Const.NET_SIZE != 0 && count % Const.NET_SIZE != 0 && mArrGameBoard[count - (Const.NET_SIZE + 1)] == -1) {
                    countAlreadyO++
                }
                break
            }
            if (count / Const.NET_SIZE == 0 || count % Const.NET_SIZE == 0) {
                break
            }
            count -= (Const.NET_SIZE + 1)
        }
        if (countX == 2 && countO < 5) {
            return 0
        }
        return if (countAlreadyX == 1 && countX == 1 && countO < 4) {
            0
        } else mDefenseList[countO] + mDefenseList[countAlreadyO] - mAttachList[countX] - mAttachList[countAlreadyX]
    }

    private fun defenseOnColumn(position: Int): Int {
        var countX = 0
        var countO = 0
        var countAlreadyX = 0
        var countAlreadyO = 0
        run {
            var count = position
            while (count < position + 6 * Const.NET_SIZE) {
                if (position / Const.NET_SIZE == (Const.NET_SIZE - 1)) {
                    break
                }
                if (count == position) {
                    count += Const.NET_SIZE
                    continue
                }
                if (mArrGameBoard[count] == -1) {
                    countO++
                } else if (mArrGameBoard[count] == 1) {
                    countX++
                    break
                } else {
                    if (count / Const.NET_SIZE != (Const.NET_SIZE - 1) && mArrGameBoard[count + Const.NET_SIZE] == 1) {
                        countAlreadyX++
                    }
                    if (count / Const.NET_SIZE != (Const.NET_SIZE - 1) && mArrGameBoard[count + Const.NET_SIZE] == -1) {
                        countAlreadyO++
                    }
                    break
                }
                if (count / Const.NET_SIZE == (Const.NET_SIZE - 1)) break
                count += Const.NET_SIZE
            }
        }
        var count = position
        while (count > position - 6 * Const.NET_SIZE) {
            if (position / Const.NET_SIZE == 0) {
                break
            }
            if (count == position) {
                count -= Const.NET_SIZE
                continue
            }
            if (mArrGameBoard[count] == -1) {
                countO++
            } else if (mArrGameBoard[count] == 1) {
                countX++
                break
            } else {
                if (count / Const.NET_SIZE != 0 && mArrGameBoard[count - Const.NET_SIZE] == 1) {
                    countAlreadyX++
                }
                if (count / Const.NET_SIZE != 0 && mArrGameBoard[count - Const.NET_SIZE] == -1) {
                    countAlreadyO++
                }
                break
            }
            if (count / Const.NET_SIZE == 0) {
                break
            }
            count -= Const.NET_SIZE
        }
        if (countX == 2 && countO < 5) {
            return 0
        }
        return if (countAlreadyX == 1 && countX == 1 && countO < 4) {
            0
        } else mDefenseList[countO] + mDefenseList[countAlreadyO] - mAttachList[countX] - mAttachList[countAlreadyX]
    }

    private fun defenseOnRow(position: Int): Int {
        var countX = 0
        var countO = 0
        var countAlreadyX = 0
        var countAlreadyO = 0
        for (count in position until position + 6) {
            if (position % Const.NET_SIZE == (Const.NET_SIZE - 1)) {
                break
            }
            if (count == position) {
                continue
            }
            if (mArrGameBoard[count] == -1) {
                countO++
            } else if (mArrGameBoard[count] == 1) {
                countX++
                break
            } else {
                if (count % Const.NET_SIZE != (Const.NET_SIZE - 1) && mArrGameBoard[count + 1] == 1) {
                    countAlreadyX++
                }
                if (count % Const.NET_SIZE != (Const.NET_SIZE - 1) && mArrGameBoard[count + 1] == -1) {
                    countAlreadyO++
                }
                break
            }
            if (count % Const.NET_SIZE == (Const.NET_SIZE - 1)) {
                break
            }
        }
        for (count in position downTo position - 6 + 1) {
            if (position % Const.NET_SIZE == 0) {
                break
            }
            if (count == position) {
                continue
            }
            if (mArrGameBoard[count] == -1) {
                countO++
            } else if (mArrGameBoard[count] == 1) {
                countX++
                break
            } else {
                if (count % Const.NET_SIZE != 0 && mArrGameBoard[count - 1] == 1) {
                    countAlreadyX++
                }
                if (count % Const.NET_SIZE != 0 && mArrGameBoard[count - 1] == -1) {
                    countAlreadyO++
                }
                break
            }
            if (count % Const.NET_SIZE == 0) {
                break
            }
        }
        if (countX == 2 && countO < 5) {
            return 0
        }
        return if (countAlreadyX == 1 && countX == 1 && countO < 4) {
            0
        } else mDefenseList[countO] + mDefenseList[countAlreadyO] - mAttachList[countX] - mAttachList[countAlreadyX]
    }

    private fun xWin() {
        mUnDoList.add(-2)
        playStatusListener.xWin()
        for (j in 0 until Const.NET_SIZE * Const.NET_SIZE) {
            if (mArrGameBoard[j] == 0) mArrGameBoard[j] = 7
        }
    }

    private fun oWin() {
        mUnDoList.add(-1)
        playStatusListener.oWin()
        for (j in 0 until Const.NET_SIZE * Const.NET_SIZE) {
            if (mArrGameBoard[j] == 0) mArrGameBoard[j] = 7
        }
    }

    // check the row
    private fun checkRow(i: Int, i1: Int): Int {
        var positionOnRow =
            i / Const.NET_SIZE * Const.NET_SIZE
        var score = 0
        while (positionOnRow < i / Const.NET_SIZE * Const.NET_SIZE + Const.NET_SIZE) {
            score = if (mArrGameBoard[positionOnRow] == i1) {
                score + i1
            } else {
                0
            }
            if (score == 5 * i1) {
                if (positionOnRow % Const.NET_SIZE == (Const.NET_SIZE - 1) || positionOnRow % Const.NET_SIZE == 4) {
                    return i1
                }
                if (mArrGameBoard[positionOnRow + 1] != -i1 || mArrGameBoard[positionOnRow - 5] != -i1) {
                    return i1
                }
            }
            positionOnRow++
        }
        return 0
    }

    // check the column
    private fun checkColumn(i: Int, i1: Int): Int {
        var positionOnColumn = i % Const.NET_SIZE
        var score = 0
        while (positionOnColumn < Const.NET_SIZE * Const.NET_SIZE) {
            score = if (mArrGameBoard[positionOnColumn] == i1) {
                score + i1
            } else {
                0
            }
            if (score == 5 * i1) {
                if (positionOnColumn / Const.NET_SIZE == (Const.NET_SIZE - 1) || positionOnColumn / Const.NET_SIZE == 4) {
                    return i1
                }
                if (mArrGameBoard[positionOnColumn + Const.NET_SIZE] != -i1 || mArrGameBoard[positionOnColumn - Const.NET_SIZE * 5] != -i1
                ) {
                    return i1
                }
            }
            positionOnColumn += Const.NET_SIZE
        }
        return 0
    }

    // check the diagonal from the left to the right
    private fun checkDiagonalLeftToRight(i: Int, i1: Int): Int {
        var positionOnDiagonal = i
        var score = 0
        while (positionOnDiagonal % Const.NET_SIZE != 0 && positionOnDiagonal / Const.NET_SIZE != 0) {
            positionOnDiagonal -= (Const.NET_SIZE + 1)
        }
        do {
            score = if (mArrGameBoard[positionOnDiagonal] == i1) {
                score + i1
            } else {
                0
            }
            if (score == 5 * i1) {
                if (positionOnDiagonal % Const.NET_SIZE == (Const.NET_SIZE - 1) || positionOnDiagonal / Const.NET_SIZE == (Const.NET_SIZE - 1) || positionOnDiagonal / Const.NET_SIZE == 4 || positionOnDiagonal % Const.NET_SIZE == 4) {
                    return i1
                }
                if (mArrGameBoard[positionOnDiagonal + (Const.NET_SIZE + 1)] != -i1 || mArrGameBoard[positionOnDiagonal - (Const.NET_SIZE + 1) * 5] != -i1
                ) {
                    return i1
                }
            }
            positionOnDiagonal += (Const.NET_SIZE + 1)
        } while (positionOnDiagonal % Const.NET_SIZE != 0 && positionOnDiagonal / Const.NET_SIZE != Const.NET_SIZE)
        return 0
    }

    // check the diagonal from the right to the left
    private fun checkDiagonalRightToLeft(i: Int, i1: Int): Int {
        var positionOnDiagonal = i
        var score = 0
        while (positionOnDiagonal % Const.NET_SIZE != (Const.NET_SIZE - 1) && positionOnDiagonal / Const.NET_SIZE != 0) {
            positionOnDiagonal -= (Const.NET_SIZE - 1)
        }
        do {
            score = if (mArrGameBoard[positionOnDiagonal] == i1) {
                score + i1
            } else {
                0
            }
            if (score == 5 * i1) {
                if (positionOnDiagonal % Const.NET_SIZE == 0 || positionOnDiagonal / Const.NET_SIZE == (Const.NET_SIZE - 1) || positionOnDiagonal / Const.NET_SIZE == 3 || positionOnDiagonal % Const.NET_SIZE == 14) {
                    return i1
                }
                if (mArrGameBoard[positionOnDiagonal + (Const.NET_SIZE - 1)] != -i1 || mArrGameBoard[positionOnDiagonal - (Const.NET_SIZE - 1) * 5] != -i1
                ) {
                    return i1
                }
            }
            positionOnDiagonal += (Const.NET_SIZE - 1)
        } while (positionOnDiagonal % Const.NET_SIZE != (Const.NET_SIZE - 1) && positionOnDiagonal / Const.NET_SIZE != Const.NET_SIZE)
        return 0
    }

    interface PlayStatusListener {
        fun oWin()
        fun xWin()
    }
}
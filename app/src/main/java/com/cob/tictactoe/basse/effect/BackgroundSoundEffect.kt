package com.cob.tictactoe.basse.effect

import android.content.Context
import android.media.MediaPlayer
import android.util.Log
import com.cob.tictactoe.R

class BackgroundSoundEffect private constructor() {
    private lateinit var mMediaPlayer: MediaPlayer

    private object Holder {
        val INSTANCE = BackgroundSoundEffect()
    }


    companion object {
        fun getInstance(): BackgroundSoundEffect {
            return Holder.INSTANCE
        }
    }

    fun initPlayer(context: Context) {
        mMediaPlayer = MediaPlayer.create(context, R.raw.background_sound)
        mMediaPlayer.isLooping = true
        mMediaPlayer.setVolume(0.5F, 0.5F)
    }

    fun playSoundEffect() {
        Log.d("TruongVM","playSoundEffect")
        if (mMediaPlayer.isPlaying) {
            mMediaPlayer.stop()
        }
        mMediaPlayer.start()
    }

    fun pauseSoundEffect() {
        if (mMediaPlayer.isPlaying) {
            mMediaPlayer.stop()
        }
    }

    fun releaseSoundEffect() {
        if (mMediaPlayer.isPlaying) {
            mMediaPlayer.release()
        }
    }

}
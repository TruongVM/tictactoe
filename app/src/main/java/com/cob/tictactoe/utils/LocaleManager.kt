@file:Suppress("DEPRECATION")

package com.cob.tictactoe.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import android.util.DisplayMetrics
import java.util.*


object LocaleManager {
    @SuppressLint("DefaultLocale")
    fun setUpLocale(context: Context, localeCode: String): Context {
        LogUtil.d("setUpLocale context= $context")
        val resources: Resources = context.resources
        val dm: DisplayMetrics = resources.displayMetrics
        val config: Configuration = resources.configuration
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLocale(Locale(localeCode.toLowerCase()))
        } else {
            config.locale = Locale(localeCode.toLowerCase())
        }
        resources.updateConfiguration(config, dm)
        return context
    }

//    @SuppressLint("DefaultLocale")
//    public fun setUpLocale(context: Context, localeCode: String) {
//        val resources = context.resources
//        val displayMetrics = resources.displayMetrics
//        val config = resources.configuration
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
//            config.setLocale(Locale(localeCode.toLowerCase()))
//        } else {
//            config.locale = Locale(localeCode.toLowerCase())
//        }
//
//        resources.updateConfiguration(config, displayMetrics)
//    }
}
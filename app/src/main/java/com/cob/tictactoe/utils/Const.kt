package com.cob.tictactoe.utils

object Const {
    const val KEY_PIN_CODE = "key_pin_code"

    const val KEY_SETTING_PIN_CODE = 4

    const val KEY_CHANGE_PIN_CODE = 5

    const val KEY_REMOVE_PIN_CODE = 6

    const val NET_SIZE = 19

    const val KEY_GAME_STEPS = "key_game_steps"

    const val KEY_GAME_TYPE = "key_game_type"

    const val KEY_COLOR_TYPE = "key_color_type"

    const val KEY_GAME_LEVEL = "key_game_level"

    const val KEY_IS_PLAYER_1_GO_FIRST = "key_is_player_1_go_first"

    const val KEY_MUSIC_ONOFF = "key_music_onoff"

    const val KEY_SOND_ONOFF = "key_sound_onoff"

    const val PLAYER_1 = -1

    const val PLAYER_2 = 1

    const val SCORE_PER_STEP_USER_1 = 1

    const val SCORE_PER_STEP_USER_2 = 5

    const val KEY_PLAY_TYPE = "key_play_type"

    const val PLAYER_VS_COM = 1

    const val PLAYER_VS_PLAYER = 2


    const val mLevelHard = 2
    const val mLevelMedium = 1
    const val mLevelEasy = 0

    const val mPlayOnThisDevice = 3
    const val mPlayOnline = 4

    const val TIME_FOR_HARD_LEVEL = 8000
    const val TIME_FOR_MEDIUM_LEVEL = 15000
    const val SHOW_ADS_FROM_TIMES_PLAY = 1
    const val SHOW_ADS_FROM_NEW_GAME = 2


}
package com.cob.tictactoe.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Set;

public class SpManager {
    // For Singleton instantiation
    private static final Object LOCK = new Object();
    private static SpManager sInstance;
    private SharedPreferences.Editor mEditor;
    private SharedPreferences mSettings;
    private Context mContext;

    public static synchronized SpManager getInstance() {
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = new SpManager();
            }
        }
        return sInstance;
    }

    private SpManager() {
        // private constructor to prevent create instance multiple time
    }

    public void init(Context context, String name) {
        mContext = context;
        mSettings = context.getSharedPreferences(name, Context.MODE_PRIVATE);
        mEditor = mSettings.edit();
    }

    public String getString(String key, String defaultValue) {
        return mSettings.getString(key, defaultValue);
    }

    public void putString(String key, String value) {
        mEditor.putString(key, value).apply();
    }


    public int getInt(String key, int defaultValue) {
        return mSettings.getInt(key, defaultValue);
    }


    public void putBoolean(String key, boolean value) {
        mEditor.putBoolean(key, value).apply();
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        return mSettings.getBoolean(key, defaultValue);
    }

    public void putBooleanForExtra(String key, boolean value) {
        mEditor.putBoolean(key, value).apply();
    }

    public boolean getBooleanForExtra(String key, boolean defaultValue) {
        return mSettings.getBoolean(key, defaultValue);
    }

    public Set<String> getStringSet(String key, Set<String> defaultValue) {
        return mSettings.getStringSet(key, defaultValue);
    }

    public void putInt(String key, int value) {
        mEditor.putInt(key, value).apply();
    }

    public void putStringSet(String key, Set<String> value) {
        mEditor.putStringSet(key, value).apply();
    }

    public void removeKey(String key) {
        mEditor.remove(key).apply();
    }

    public boolean contains(String key) {
        return mSettings.contains(key);
    }

    public void registerOnSharedPreferenceChangeListener(
            SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        mSettings.registerOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);
    }

    public void unregisterOnSharedPreferenceChangeListener(
            SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        mSettings.unregisterOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);
    }
}

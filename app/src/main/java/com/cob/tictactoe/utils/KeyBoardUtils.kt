package com.cob.tictactoe.utils

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.inputmethod.InputMethodManager


object KeyBoardUtils {
    fun hideKeyboard(activity: Activity?): Boolean {
        Log.d("TruongVM","hideKeyboard activity= "+activity)
        if (activity != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            try {
                Log.d("TruongVM","hideKeyboard isAcceptingText= "+imm.isAcceptingText)
//                if (imm.isAcceptingText) {
                    return imm.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
//                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
        return false
    }
}
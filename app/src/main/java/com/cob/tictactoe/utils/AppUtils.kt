package com.cob.tictactoe.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.database.Cursor
import android.graphics.Bitmap
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.widget.Toast
import androidx.core.content.FileProvider
import com.cob.tictactoe.BuildConfig
import com.cob.tictactoe.MainActivity
import java.io.*
import java.text.SimpleDateFormat

object AppUtils {

    const val REQ_CODE_RECORD_AUDIO = 303;

    const val KEY_LANGUAGE_VIE = "vi"

    const val KEY_LANGUAGE_ENG = "en"

    const val KEY_SETTING_LANGUAGE = "key_setting_language"

    const val KEY_PREF = "key_pref"

    var mUnDoTimes = 4

    val APP_FOLDER_AUDIO =
        Environment.getExternalStorageDirectory().absolutePath + "/soundrecorder/"

    val APP_FOLDER_IMAGE =
        Environment.getExternalStorageDirectory().absolutePath + "/image_capture/"

    private lateinit var currentPhotoPath: String

    private lateinit var currentVideoPath: String

    private const val REQUEST_TAKE_PHOTO = 1005

    const val REQUEST_IMAGE_FROM_GALLERY = 1006

    const val REQUEST_VIDEO_CAPTURE = 1007

    const val REQUEST_VIDEO_FROM_GALLERY = 1008


    fun getAudioDuration(url: String): String {
        val mediaMetadataRetriever = MediaMetadataRetriever()
        mediaMetadataRetriever.setDataSource(url)
        val duration =
            mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
                .toInt()
        LogUtil.d(" duration= $duration  ----")

        return timeToString(duration)

    }

    @SuppressLint("SimpleDateFormat")
    fun timeToString(milliSecond: Int): String =
        SimpleDateFormat("mm:ss").format(milliSecond)

    fun deleteFileFromStorage(url: String) {
        val file = File(url)
        if (file.exists()) {
            file.delete()
        }
    }

    fun checkFileExist(url: String): Boolean {
        val file = File(url)
        return (file.exists())
    }

    fun sendMail(
        recipient: String,
        subject: String,
        message: String,
        context: Context,
        uri: ArrayList<String>?
    ) {
        val inTent = Intent(Intent.ACTION_SEND_MULTIPLE)
        inTent.data = Uri.parse("mailto:")
        inTent.type = "plain/text"
        inTent.putExtra(Intent.EXTRA_EMAIL, arrayOf(recipient))
        inTent.putExtra(Intent.EXTRA_SUBJECT, subject)
        inTent.putExtra(Intent.EXTRA_TEXT, message)
        if (uri?.size!! > 0) {
            val arrUri = ArrayList<Uri>()
            for (filePath in uri) {
                val file = File(filePath)
                val tempUri =
                    FileProvider.getUriForFile(context, context.packageName + ".provider", file)
                arrUri.add(tempUri)
            }
            LogUtil.d("arrUri.size= " + arrUri.size)
            inTent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            inTent.putExtra(Intent.EXTRA_STREAM, arrUri)
        }

        try {
            context.startActivity(Intent.createChooser(inTent, "Choose Email Client..."))
        } catch (e: Exception) {
        }
    }

    fun email(
        context: Context,
        emailTo: String,
        subject: String?,
        emailText: String?,
        filePaths: List<String?>
    ) {
        //need to "send multiple" to get more than one attachment
        val emailIntent = Intent(Intent.ACTION_SEND_MULTIPLE)
        emailIntent.type = "plain/text"
        emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(emailTo))
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        emailIntent.putExtra(Intent.EXTRA_TEXT, emailText)
        //has to be an ArrayList
        val uris = ArrayList<Uri>()
        //convert from paths to Android friendly Parcelable Uri's
        for (file in filePaths) {
            val fileIn = File(file)
            val u = FileProvider.getUriForFile(context, context.packageName + ".provider", fileIn)
            uris.add(u)
        }
        emailIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris)
        context.startActivity(Intent.createChooser(emailIntent, "Send mail..."))
    }

    public fun restartApp(context: Context) {
        val mStartActivity = Intent(context, MainActivity::class.java)
        val mPendingIntentId = 123456
        val mPendingIntent = PendingIntent.getActivity(
            context,
            mPendingIntentId,
            mStartActivity,
            PendingIntent.FLAG_CANCEL_CURRENT
        )
        val mgr =
            context!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        mgr[AlarmManager.RTC, System.currentTimeMillis() + 100] = mPendingIntent
        System.exit(0)
    }

    fun getCurrentLanguage(): String {
        return if (SpManager.getInstance().getString(
                KEY_SETTING_LANGUAGE,
                KEY_LANGUAGE_ENG
            ) == KEY_LANGUAGE_ENG
        ) {
            KEY_LANGUAGE_ENG
        } else {
            KEY_LANGUAGE_VIE
        }
    }


    @Throws(IOException::class)
    private fun createImageFile(activity: Activity): File {
        val timeStamp: String = DateTimeUtil.timeToString()
        val storageDir: File = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        return File.createTempFile(
            "Image_${timeStamp}",
            ".jpg",
            storageDir
        ).apply {
            currentPhotoPath = absolutePath
        }
    }


    fun dispatchTakePictureIntent(activity: Activity) {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(activity.packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile(activity)
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        activity,
                        "${BuildConfig.APPLICATION_ID}.provider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    activity.startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
                }
            }
        }
    }

    fun galleryAddPic(activity: Activity): String {
        Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE).also { mediaScanIntent ->
            val f = File(currentPhotoPath)
            mediaScanIntent.data = Uri.fromFile(f)
            activity.sendBroadcast(mediaScanIntent)
            return currentPhotoPath
        }
    }

    fun openGalleryToPickImage(activity: Activity) {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        activity.startActivityForResult(intent, REQUEST_IMAGE_FROM_GALLERY)
    }

    fun openGalleryToPickVideo(activity: Activity) {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "video/*"
        activity.startActivityForResult(intent, REQUEST_VIDEO_FROM_GALLERY)
    }

    fun saveImageToInternalStorage(imageUri: Uri, activity: Activity): Uri {
        val bitmap = MediaStore.Images.Media.getBitmap(
            activity.contentResolver, imageUri
        );
        val file = createImageFile(activity)

        try {
            val stream: OutputStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
            stream.flush()
            stream.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return Uri.parse(currentPhotoPath)
    }


    fun dispatchTakeVideoIntent(activity: Activity) {
        Intent(MediaStore.ACTION_VIDEO_CAPTURE).also { takeVideoIntent ->
            takeVideoIntent.resolveActivity(activity.packageManager)?.also {
                activity.startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE)
            }
        }
    }

    private fun createVideoFile(activity: Activity): File {
        val timeStamp: String = DateTimeUtil.timeToString()
        val storageDir: File = activity.getExternalFilesDir(Environment.DIRECTORY_MOVIES)!!
        return File.createTempFile(
            "Video_${timeStamp}",
            ".mp4",
            storageDir
        ).apply {
            currentVideoPath = absolutePath
        }
    }


    fun saveVideoToInternalStorage(uri: String, activity: Activity): Uri {
        val newFile: File
        try {
            val currentFile = File(uri.toString())
            newFile = createVideoFile(activity)
            LogUtil.d("currentFile.exists()= " + currentFile.exists())
            val inputStream: InputStream = FileInputStream(currentFile)
            val out: OutputStream = FileOutputStream(newFile)

            val buf = ByteArray(1024)
            var len: Int = -1
            while (inputStream.read(buf).also { len = it } > 0) {
                out.write(buf, 0, len)
            }
            inputStream.close()
            out.close()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return Uri.parse(currentVideoPath)
    }

    @SuppressLint("Recycle")
    fun getVideoPath(uri: Uri?, activity: Activity): String? {
        val projection =
            arrayOf(MediaStore.Video.Media.DATA)
        val cursor: Cursor =
            activity.getContentResolver().query(uri!!, projection, null, null, null)!!
        return run {
            val column_index: Int = cursor
                .getColumnIndexOrThrow(MediaStore.Video.Media.DATA)
            cursor.moveToFirst()
            cursor.getString(column_index)
        }
    }

    fun getScreenHeight(): Int {
        return Resources.getSystem().displayMetrics.heightPixels
    }

    fun getScreenWidth(): Int {
        return Resources.getSystem().displayMetrics.widthPixels
    }

    fun setMusicOffOn(isMusicOn: Boolean) {
        SpManager.getInstance().putBoolean(Const.KEY_MUSIC_ONOFF, isMusicOn)
    }

    fun getMusicStatus(): Boolean {
        return SpManager.getInstance().getBoolean(Const.KEY_MUSIC_ONOFF, true)
    }

    fun setSoundOffOn(isSoundOn: Boolean) {
        SpManager.getInstance().putBoolean(Const.KEY_SOND_ONOFF, isSoundOn)
    }

    fun getSondStatus(): Boolean {
        return SpManager.getInstance().getBoolean(Const.KEY_SOND_ONOFF, true)
    }

    fun showToast(message: String, context: Context) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }


}
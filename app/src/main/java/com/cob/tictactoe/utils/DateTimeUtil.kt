package com.cob.tictactoe.utils

import android.annotation.SuppressLint
import java.text.DateFormatSymbols
import java.text.SimpleDateFormat
import java.util.*

object DateTimeUtil {
    @SuppressLint("SimpleDateFormat")
    fun dateTimeToString(): String =
        SimpleDateFormat("HH:mm dd/MM/yyyy").format(System.currentTimeMillis())

    @SuppressLint("SimpleDateFormat")
    fun timeToString(): String =
        SimpleDateFormat("_HH_mm_ss_ddMMyy").format(System.currentTimeMillis())

    @SuppressLint("SimpleDateFormat")
    fun getMonthString(month: Int): String {
        val mons: Array<String> = DateFormatSymbols().shortMonths
        LogUtil.d("getMonthString" + mons[month])
        return mons[month]
    }

    fun getMillisecondFromDateTime(year: Int, month: Int, day: Int, hour: Int, min: Int): Long {
        val calendar: Calendar = Calendar.getInstance()
        calendar.set(
            year, month, day, hour, min, 0
        )
        return calendar.getTimeInMillis()
    }
}
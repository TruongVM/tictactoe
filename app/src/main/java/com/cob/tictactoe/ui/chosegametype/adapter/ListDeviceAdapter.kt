package com.cob.tictactoe.ui.chosegametype.adapter

import android.content.Context
import android.net.wifi.p2p.WifiP2pDevice
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cob.tictactoe.R
import com.cob.tictactoe.ui.chosegametype.DeviceModel
import kotlinx.android.synthetic.main.item_list_available_devices.view.*

class ListDeviceAdapter(
    private var mContext: Context,
    private var onItemClick: (model: WifiP2pDevice, connectStatus: Int) -> Unit
) : RecyclerView.Adapter<ListDeviceAdapter.ViewHolder>() {

    private var mListDevices: ArrayList<WifiP2pDevice> = ArrayList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(mContext).inflate(
                R.layout.item_list_available_devices,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return mListDevices.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            onItemClick.invoke(mListDevices[position], mListDevices[position].status)
        }
        holder.fillData(mListDevices[position])
    }

    fun clearList() {
        mListDevices.clear()
        notifyDataSetChanged()
    }

    fun updateList(wifiP2pDeviceList: Collection<WifiP2pDevice?>?) {
        mListDevices.clear()
        for (device in wifiP2pDeviceList!!) {
            if (!mListDevices.contains(device)) {
                mListDevices.add(device!!)
            }
        }
        notifyDataSetChanged()

    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun fillData(deviceModel: WifiP2pDevice) {
            itemView.txtDeviceName.text =
                String.format(mContext.getString(R.string.device_name, deviceModel.deviceName))
            itemView.txtDeviceAddress.text =
                String.format(
                    mContext.getString(
                        R.string.device_address,
                        deviceModel.deviceAddress
                    )
                )
            itemView.txtDeviceDetails.text = String.format(
                mContext.getString(
                    R.string.device_status,
                    getDeviceStatus(deviceModel.status)
                )
            )
        }

        private fun getDeviceStatus(deviceStatus: Int): String? {
            return when (deviceStatus) {
                WifiP2pDevice.AVAILABLE -> mContext.resources.getString(R.string.device_status_usable)
                WifiP2pDevice.INVITED -> mContext.resources.getString(R.string.device_status_inviting)
                WifiP2pDevice.CONNECTED -> mContext.resources.getString(R.string.device_status_connected)
                WifiP2pDevice.FAILED -> mContext.resources.getString(R.string.device_status_lose)
                WifiP2pDevice.UNAVAILABLE -> mContext.resources.getString(R.string.device_status_unavailable)
                else -> mContext.resources.getString(R.string.device_status_unknown)
            }
        }

    }
}
package com.cob.tictactoe.ui.playgame.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Toast;

import com.cob.tictactoe.R;
import com.cob.tictactoe.customview.CustomTextView;
import com.cob.tictactoe.utils.Const;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Administrator on 3/12/2018.
 */

public class AdapterGridViewPvsC extends BaseAdapter {
    private Context myContext;
    private ArrayList<Integer> mArrGameBoard, mReplayList, mRedoList, mAttachList, mDefenseList, heuristic, random;
    private int mCheck = 0;

    public AdapterGridViewPvsC(Context myContext, ArrayList<Integer> arr, ArrayList<Integer> rep,
                               ArrayList<Integer> red) {
        this.myContext = myContext;
        this.mArrGameBoard = arr;
        this.mReplayList = rep;
        this.mRedoList = red;
    }

    @Override
    public int getCount() {
        return mArrGameBoard.size();
    }

    @Override
    public Object getItem(int i) {
        return mArrGameBoard.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.item_game_board, null);

        mAttachList = new ArrayList<>();
        mAttachList.add(0);
        mAttachList.add(3);
        mAttachList.add(24);
        mAttachList.add(192);
        mAttachList.add(1536);
        mAttachList.add(12288);
        mAttachList.add(98304);
        mAttachList.add(786432);
        mDefenseList = new ArrayList<>();
        mDefenseList.add(0);
        mDefenseList.add(1);
        mDefenseList.add(9);
        mDefenseList.add(81);
        mDefenseList.add(729);
        mDefenseList.add(6561);
        mDefenseList.add(59849);
        mDefenseList.add(538641);
        heuristic = new ArrayList<>();
        random = new ArrayList<>();
        final CustomTextView customTextView = view.findViewById(R.id.txtItemGameBoard);
//        customTextView.setBackgroundResource(R.drawable.oco);

//        if (mArrGameBoard.get(i) == 1) customTextView.setText("X");
//        if (mArrGameBoard.get(i) == -1) customTextView.setText("O");
//        if (mReplayList.size() > 0)
//            if (mReplayList.get(mReplayList.size() - 1) == i)
//                customTextView.setText("X");

        customTextView.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onClick(View view) {
                Log.d("TruongVM", "onClick customTextView");
                if (mArrGameBoard.get(i) == 0 && mReplayList.size() % 2 == 1) {
                    Log.d("TruongVM", "onClick customTextView in condition");
                    mRedoList.clear();
                    mCheck = 0;
                    mArrGameBoard.set(i, -1);
                    notifyDataSetChanged();
                    mReplayList.add(i);
                    mCheck = checkRow(i, -1) + checkColumn(i, -1) + checkDiagonalLeftToRight(i, -1) + checkDiagonalRightToLeft(i, -1);
                    if (mCheck < 0) {
                        oWin();
                    } else {
                        move();
                    }
                }
            }

            private void move() {
                mCheck = 0;
                int position = findPositionToBeatForAI();
                mArrGameBoard.set(position, 1);
                mReplayList.add(position);
                notifyDataSetChanged();
                mCheck = checkRow(position, 1) + checkColumn(position, 1) + checkDiagonalLeftToRight(position, 1) + checkDiagonalRightToLeft(position, 1);
                if (mCheck > 0) {
                    xWin();
                }
            }

            private boolean checkNearField(int position) {
                int row = position / 19;
                int column = position % 19;
                for (int x = row - 1; x <= row + 1; x++)
                    for (int y = column - 1; y <= column + 1; y++)
                        if (x >= 0 && x <= 18 && y >= 0 && y <= 18 && mArrGameBoard.get(x * 19 + y) != 0 && x * 19 + y != position)
                            return true;
                return false;
            }

            private int minMax(int mini, int maxi, int depth, boolean isCom) {
                if (depth == 4) {
                    return 0;
                }
                if (isCom) {
                    int v = -1000000000;
                    for (int position = mReplayList.get(mReplayList.size() - 1); position < 19 * 19; position++) {
                        if (mArrGameBoard.get(position) == 0 && checkNearField(position)) {
                            mArrGameBoard.set(position, 1);
                            mReplayList.add(position);
                            int check = checkRow(position, 1) + checkColumn(position, 1) + checkDiagonalLeftToRight(position, 1) + checkDiagonalRightToLeft(position, 1);
                            if (check > 0) {
                                mReplayList.remove(mReplayList.size() - 1);
                                mArrGameBoard.set(position, 0);
                                return 100000000 - depth;
                            }
                            v = Math.max(v, minMax(mini, maxi, depth + 1, false));
                            mReplayList.remove(mReplayList.size() - 1);
                            mArrGameBoard.set(position, 0);
                            if (v >= maxi) {
                                return v;
                            }
                            mini = Math.max(mini, v);
                        }
                    }
                    for (int position = mReplayList.get(mReplayList.size() - 1); position >= 0; position--) {
                        if (mArrGameBoard.get(position) == 0 && checkNearField(position)) {
                            mArrGameBoard.set(position, 1);
                            mReplayList.add(position);
                            int check = checkRow(position, 1) + checkColumn(position, 1) + checkDiagonalLeftToRight(position, 1) + checkDiagonalRightToLeft(position, 1);
                            if (check > 0) {
                                mReplayList.remove(mReplayList.size() - 1);
                                mArrGameBoard.set(position, 0);
                                return 100000000 - depth;
                            }
                            v = Math.max(v, minMax(mini, maxi, depth + 1, false));
                            mReplayList.remove(mReplayList.size() - 1);
                            mArrGameBoard.set(position, 0);
                            if (v >= maxi) {
                                return v;
                            }
                            mini = Math.max(mini, v);
                        }
                    }

                    return v;
                } else {
                    int v = 1000000000;
                    for (int position = mReplayList.get(mReplayList.size() - 1); position < 19 * 19; position++) {
                        if (mArrGameBoard.get(position) == 0 && checkNearField(position)) {
                            mArrGameBoard.set(position, -1);
                            mReplayList.add(position);
                            int check = checkRow(position, -1) + checkColumn(position, -1) + checkDiagonalLeftToRight(position, -1) + checkDiagonalRightToLeft(position, -1);
                            if (check < 0) {
                                mReplayList.remove(mReplayList.size() - 1);
                                mArrGameBoard.set(position, 0);
                                return -100000000 + depth;
                            }
                            v = Math.min(v, minMax(mini, maxi, depth + 1, true));
                            mReplayList.remove(mReplayList.size() - 1);
                            mArrGameBoard.set(position, 0);
                            if (v <= mini) {
                                return v;
                            }
                            maxi = Math.min(maxi, v);
                        }
                    }
                    for (int position = mReplayList.get(mReplayList.size() - 1); position >= 0; position--) {
                        if (mArrGameBoard.get(position) == 0 && checkNearField(position)) {
                            mArrGameBoard.set(position, -1);
                            mReplayList.add(position);
                            int check = checkRow(position, -1) + checkColumn(position, -1) + checkDiagonalLeftToRight(position, -1) + checkDiagonalRightToLeft(position, -1);
                            if (check < 0) {
                                mReplayList.remove(mReplayList.size() - 1);
                                mArrGameBoard.set(position, 0);
                                return -100000000 + depth;
                            }
                            v = Math.min(v, minMax(mini, maxi, depth + 1, true));
                            mReplayList.remove(mReplayList.size() - 1);
                            mArrGameBoard.set(position, 0);
                            if (v <= mini) {
                                return v;
                            }
                            maxi = Math.min(maxi, v);
                        }
                    }
                    return v;
                }
            }

            private int findPositionToBeatForAI() {
                heuristic.clear();
                random.clear();
                for (int position = 0; position < 19 * 19; position++)
                    if (mArrGameBoard.get(position) != 0) {
                        heuristic.add(0);
                    } else {
                        heuristic.add(
                                Math.max(attachFromLeftToRight(position) + attachFromRightToLeft(position) + attachOnColumn(position) + attachOnRow(position),
                                        defenseFromLeftToRight(position) + defenseFromRightToLeft(position) + defenseOnColumn(position) + defenseOnRow(position)));
                    }
                for (int position = mReplayList.get(mReplayList.size() - 1); position < 19 * 19; position++)
                    if (mArrGameBoard.get(position) == 0 && checkNearField(position)) {
                        mArrGameBoard.set(position, 1);
                        mReplayList.add(position);
                        int check = checkRow(position, 1) + checkColumn(position, 1) + checkDiagonalLeftToRight(position, 1) + checkDiagonalRightToLeft(position, 1);
                        if (check > 0) {
                            mArrGameBoard.set(position, 0);
                            heuristic.set(position, 100000000);
                            break;
                        }   //neu x win luon return heuristic=100cu;
                        int value = minMax(-1010101010, 1010101010, 2, false);
                        if (heuristic.get(position) < value) {
                            heuristic.set(position, value);
                        }
                        mReplayList.remove(mReplayList.size() - 1);
                        mArrGameBoard.set(position, 0);
                    }

                for (int position = mReplayList.get(mReplayList.size() - 1); position >= 0; position--)
                    if (mArrGameBoard.get(position) == 0 && checkNearField(position)) {
                        mArrGameBoard.set(position, 1);
                        mReplayList.add(position);
                        int check = checkRow(position, 1) + checkColumn(position, 1) + checkDiagonalLeftToRight(position, 1) + checkDiagonalRightToLeft(position, 1);
                        if (check > 0) {
                            mArrGameBoard.set(position, 0);
                            heuristic.set(position, 100000000);
                            break;
                        }   //neu x win luon return heuristic=100cu;
                        int value = minMax(-1010101010, 1010101010, 2, false);
                        if (heuristic.get(position) < value) {
                            heuristic.set(position, value);
                        }
                        mReplayList.remove(mReplayList.size() - 1);
                        mArrGameBoard.set(position, 0);
                    }

                int max = 0;
                for (int position = 0; position < 19 * 19; position++) {
                    if (heuristic.get(position) > heuristic.get(max)) max = position;
                }
                for (int position = 0; position < 19 * 19; position++) {
                    if (heuristic.get(position).equals(heuristic.get(max))) random.add(position);
                }
                Random rd = new Random();
                return random.get(rd.nextInt(random.size()));
            }

            private int attachFromRightToLeft(int position) {
                int countX = 0;
                int countO = 0;
                int countAlreadyO = 0;
                int countAlreadyX = 0;
                for (int count = position; count < position + 6 * 18; count = count + 18) {
                    if (position / 19 == 18 || position % 19 == 0) {
                        break;
                    }
                    if (count == position) {
                        continue;
                    }
                    if (mArrGameBoard.get(count) == 1) {
                        countX++;
                    } else if (mArrGameBoard.get(count) == -1) {
                        countO++;
                        break;
                    } else {
                        if (count / 19 != 18 && count % 19 != 0 && mArrGameBoard.get(count + 18) == -1) {
                            countAlreadyO++;
                        }
                        if (count / 19 != 18 && count % 19 != 0 && mArrGameBoard.get(count + 18) == 1) {
                            countAlreadyX++;
                        }
                        break;
                    }
                    if (count / 19 == 18 || count % 19 == 0) {
                        break;
                    }
                }
                for (int count = position; count > position - 6 * 18; count = count - 18) {
                    if (position / 19 == 0 || position % 19 == 18) {
                        break;
                    }
                    if (count == position) {
                        continue;
                    }
                    if (mArrGameBoard.get(count) == 1) {
                        countX++;
                    } else if (mArrGameBoard.get(count) == -1) {
                        countO++;
                        break;
                    } else {
                        if (count / 19 != 0 && count / 19 != 18 && mArrGameBoard.get(count - 18) == -1) {
                            countAlreadyO++;
                        }
                        if (count / 19 != 0 && count / 19 != 18 && mArrGameBoard.get(count - 18) == 1) {
                            countAlreadyX++;
                        }
                        break;
                    }
                    if (count / 19 == 0 || count % 19 == 18) {
                        break;
                    }
                }
                if (countO == 2 && countX < 5) {
                    return 0;
                }
                if (countAlreadyO == 1 && countO == 1 && countX < 4) {
                    return 0;
                }
                return mAttachList.get(countX) + mAttachList.get(countAlreadyX) - mDefenseList.get(countO) - mDefenseList.get(countAlreadyO);

            }

            private int attachFromLeftToRight(int position) {
                int countX = 0;
                int countO = 0;
                int countAlreadyO = 0;
                int countAlreadyX = 0;
                for (int count = position; count < position + 6 * 20; count = count + 20) {
                    if (position / 19 == 18 || position % 19 == 18) {
                        break;
                    }
                    if (count == position) {
                        continue;
                    }
                    if (mArrGameBoard.get(count) == 1) {
                        countX++;
                    } else if (mArrGameBoard.get(count) == -1) {
                        countO++;
                        break;
                    } else {
                        if (count / 19 != 18 && count % 19 != 18 && mArrGameBoard.get(count + 20) == -1) {
                            countAlreadyO++;
                        }
                        if (count / 19 != 18 && count % 19 != 18 && mArrGameBoard.get(count + 20) == 1) {
                            countAlreadyX++;
                        }
                        break;
                    }
                    if (count / 19 == 18 || count % 19 == 18) {
                        break;
                    }
                }
                for (int count = position; count > position - 6 * 20; count = count - 20) {
                    if (position / 19 == 0 || position % 19 == 0) {
                        break;
                    }
                    if (count == position) {
                        continue;
                    }
                    if (mArrGameBoard.get(count) == 1) {
                        countX++;
                    } else if (mArrGameBoard.get(count) == -1) {
                        countO++;
                        break;
                    } else {
                        if (count / 19 != 0 && count % 19 != 0 && mArrGameBoard.get(count - 20) == -1) {
                            countAlreadyO++;
                        }
                        if (count / 19 != 0 && count % 19 != 0 && mArrGameBoard.get(count - 20) == 1) {
                            countAlreadyX++;
                        }
                        break;
                    }
                    if (count / 19 == 0 || count % 19 == 0) {
                        break;
                    }
                }
                if (countO == 2 && countX < 5) {
                    return 0;
                }
                if (countAlreadyO == 1 && countO == 1 && countX < 4) {
                    return 0;
                }
                return mAttachList.get(countX) + mAttachList.get(countAlreadyX) - mDefenseList.get(countO) - mDefenseList.get(countAlreadyO);

            }

            private int attachOnColumn(int position) {
                int countX = 0;
                int countO = 0;
                int countAlreadyO = 0;
                int countAlreadyX = 0;
                for (int count = position; count < position + 6 * 19; count = count + 19) {
                    if (position / 19 == 18) {
                        break;
                    }
                    if (count == position) {
                        continue;
                    }
                    if (mArrGameBoard.get(count) == 1) {
                        countX++;
                    } else if (mArrGameBoard.get(count) == -1) {
                        countO++;
                        break;
                    } else {
                        if (count / 19 != 18 && mArrGameBoard.get(count + 19) == -1) {
                            countAlreadyO++;
                        }
                        if (count / 19 != 18 && mArrGameBoard.get(count + 19) == 1) {
                            countAlreadyX++;
                        }
                        break;
                    }
                    if (count / 19 == 18) {
                        break;
                    }
                }
                for (int count = position; count > position - 6 * 19; count = count - 19) {
                    if (position / 19 == 0) {
                        break;
                    }
                    if (count == position) {
                        continue;
                    }
                    if (mArrGameBoard.get(count) == 1) {
                        countX++;
                    } else if (mArrGameBoard.get(count) == -1) {
                        countO++;
                        break;
                    } else {
                        if (count / 19 != 0 && mArrGameBoard.get(count - 19) == -1) {
                            countAlreadyO++;
                        }
                        if (count / 19 != 0 && mArrGameBoard.get(count - 19) == 1) {
                            countAlreadyX++;
                        }
                        break;
                    }
                    if (count / 19 == 0) {
                        break;
                    }
                }
                if (countO == 2 && countX < 5) {
                    return 0;
                }
                if (countAlreadyO == 1 && countO == 1 && countX < 4) {
                    return 0;
                }
                return mAttachList.get(countX) + mAttachList.get(countAlreadyX) - mDefenseList.get(countO) - mDefenseList.get(countAlreadyO);

            }

            private int attachOnRow(int position) {
                int countX = 0;
                int countO = 0;
                int countAlreadyO = 0;
                int countAlreadyX = 0;
                for (int count = position; count < position + 6; count++) {
                    if (position % 19 == 18) {
                        break;
                    }
                    if (count == position) {
                        continue;
                    }
                    if (mArrGameBoard.get(count) == 1) {
                        countX++;
                    } else if (mArrGameBoard.get(count) == -1) {
                        countO++;
                        break;
                    } else {
                        if (count % 19 != 18 && mArrGameBoard.get(count + 1) == -1) {
                            countAlreadyO++;
                        }
                        if (count % 19 != 18 && mArrGameBoard.get(count + 1) == 1) {
                            countAlreadyX++;
                        }
                        break;
                    }
                    if (count % 19 == 18) {
                        break;
                    }
                }
                for (int count = position; count > position - 6; count--) {
                    if (position % 19 == 0) {
                        break;
                    }
                    if (count == position) {
                        continue;
                    }
                    if (mArrGameBoard.get(count) == 1) {
                        countX++;
                    } else if (mArrGameBoard.get(count) == -1) {
                        countO++;
                        break;
                    } else {
                        if (count % 19 != 0 && mArrGameBoard.get(count - 1) == -1) {
                            countAlreadyO++;
                        }
                        if (count % 19 != 0 && mArrGameBoard.get(count - 1) == 1) {
                            countAlreadyX++;
                        }
                        break;
                    }
                    if (count % 19 == 0) {
                        break;
                    }
                }
                if (countO == 2 && countX < 5) {
                    return 0;
                }
                if (countAlreadyO == 1 && countO == 1 && countX < 4) {
                    return 0;
                }
                return mAttachList.get(countX) + mAttachList.get(countAlreadyX) - mDefenseList.get(countO) - mDefenseList.get(countAlreadyO);
            }

            private int defenseFromRightToLeft(int position) {
                int countX = 0;
                int countO = 0;
                int countAlreadyX = 0;
                int countAlreadyO = 0;
                for (int count = position; count < position + 6 * 18; count = count + 18) {
                    if (position / 19 == 18 || position % 19 == 0) {
                        break;
                    }
                    if (count == position) {
                        continue;
                    }
                    if (mArrGameBoard.get(count) == -1) {
                        countO++;
                    } else if (mArrGameBoard.get(count) == 1) {
                        countX++;
                        break;
                    } else {
                        if (count / 19 != 18 && count % 19 != 0 && mArrGameBoard.get(count + 18) == 1) {
                            countAlreadyX++;
                        }
                        if (count / 19 != 18 && count % 19 != 0 && mArrGameBoard.get(count + 18) == -1) {
                            countAlreadyO++;
                        }
                        break;
                    }
                    if (count / 19 == 18 || count % 19 == 0) {
                        break;
                    }
                }
                for (int count = position; count > position - 6 * 18; count = count - 18) {
                    if (position / 19 == 0 || position % 19 == 18) {
                        break;
                    }
                    if (count == position) {
                        continue;
                    }
                    if (mArrGameBoard.get(count) == -1) {
                        countO++;
                    } else if (mArrGameBoard.get(count) == 1) {
                        countX++;
                        break;
                    } else {
                        if (count / 19 != 0 && count / 19 != 18 && mArrGameBoard.get(count - 18) == 1) {
                            countAlreadyX++;
                        }
                        if (count / 19 != 0 && count / 19 != 18 && mArrGameBoard.get(count - 18) == -1) {
                            countAlreadyO++;
                        }
                        break;
                    }
                    if (count / 19 == 0 || count % 19 == 18) {
                        break;
                    }
                }
                if (countX == 2 && countO < 5) {
                    return 0;
                }
                if (countAlreadyX == 1 && countX == 1 && countO < 4) {
                    return 0;
                }
                return mDefenseList.get(countO) + mDefenseList.get(countAlreadyO) - mAttachList.get(countX) - mAttachList.get(countAlreadyX);

            }

            private int defenseFromLeftToRight(int position) {
                int countX = 0;
                int countO = 0;
                int countAlreadyX = 0;
                int countAlreadyO = 0;
                for (int count = position; count < position + 6 * 20; count = count + 20) {
                    if (position / 19 == 18 || position % 19 == 18) {
                        break;
                    }
                    if (count == position) {
                        continue;
                    }
                    if (mArrGameBoard.get(count) == -1) {
                        countO++;
                    } else if (mArrGameBoard.get(count) == 1) {
                        countX++;
                        break;
                    } else {
                        if (count / 19 != 18 && count % 19 != 18 && mArrGameBoard.get(count + 20) == 1) {
                            countAlreadyX++;
                        }
                        if (count / 19 != 18 && count % 19 != 18 && mArrGameBoard.get(count + 20) == -1) {
                            countAlreadyO++;
                        }
                        break;
                    }
                    if (count / 19 == 18 || count % 19 == 18) {
                        break;
                    }
                }
                for (int count = position; count > position - 6 * 20; count = count - 20) {
                    if (position / 19 == 0 || position % 19 == 0) {
                        break;
                    }
                    if (count == position) {
                        continue;
                    }
                    if (mArrGameBoard.get(count) == -1) {
                        countO++;
                    } else if (mArrGameBoard.get(count) == 1) {
                        countX++;
                        break;
                    } else {
                        if (count / 19 != 0 && count % 19 != 0 && mArrGameBoard.get(count - 20) == 1) {
                            countAlreadyX++;
                        }
                        if (count / 19 != 0 && count % 19 != 0 && mArrGameBoard.get(count - 20) == -1) {
                            countAlreadyO++;
                        }
                        break;
                    }
                    if (count / 19 == 0 || count % 19 == 0) {
                        break;
                    }
                }
                if (countX == 2 && countO < 5) {
                    return 0;
                }
                if (countAlreadyX == 1 && countX == 1 && countO < 4) {
                    return 0;
                }
                return mDefenseList.get(countO) + mDefenseList.get(countAlreadyO) - mAttachList.get(countX) - mAttachList.get(countAlreadyX);

            }

            private int defenseOnColumn(int position) {
                int countX = 0;
                int countO = 0;
                int countAlreadyX = 0;
                int countAlreadyO = 0;
                for (int count = position; count < position + 6 * 19; count = count + 19) {
                    if (position / 19 == 18) {
                        break;
                    }
                    if (count == position) {
                        continue;
                    }
                    if (mArrGameBoard.get(count) == -1) {
                        countO++;
                    } else if (mArrGameBoard.get(count) == 1) {
                        countX++;
                        break;
                    } else {
                        if (count / 19 != 18 && mArrGameBoard.get(count + 19) == 1) {
                            countAlreadyX++;
                        }
                        if (count / 19 != 18 && mArrGameBoard.get(count + 19) == -1) {
                            countAlreadyO++;
                        }
                        break;
                    }
                    if (count / 19 == 18) break;
                }
                for (int count = position; count > position - 6 * 19; count = count - 19) {
                    if (position / 19 == 0) {
                        break;
                    }
                    if (count == position) {
                        continue;
                    }
                    if (mArrGameBoard.get(count) == -1) {
                        countO++;
                    } else if (mArrGameBoard.get(count) == 1) {
                        countX++;
                        break;
                    } else {
                        if (count / 19 != 0 && mArrGameBoard.get(count - 19) == 1) {
                            countAlreadyX++;
                        }
                        if (count / 19 != 0 && mArrGameBoard.get(count - 19) == -1) {
                            countAlreadyO++;
                        }
                        break;
                    }
                    if (count / 19 == 0) {
                        break;
                    }
                }
                if (countX == 2 && countO < 5) {
                    return 0;
                }
                if (countAlreadyX == 1 && countX == 1 && countO < 4) {
                    return 0;
                }
                return mDefenseList.get(countO) + mDefenseList.get(countAlreadyO) - mAttachList.get(countX) - mAttachList.get(countAlreadyX);

            }

            private int defenseOnRow(int position) {
                int countX = 0;
                int countO = 0;
                int countAlreadyX = 0;
                int countAlreadyO = 0;
                for (int count = position; count < position + 6; count++) {
                    if (position % 19 == 18) {
                        break;
                    }
                    if (count == position) {
                        continue;
                    }
                    if (mArrGameBoard.get(count) == -1) {
                        countO++;
                    } else if (mArrGameBoard.get(count) == 1) {
                        countX++;
                        break;
                    } else {
                        if (count % 19 != 18 && mArrGameBoard.get(count + 1) == 1) {
                            countAlreadyX++;
                        }
                        if (count % 19 != 18 && mArrGameBoard.get(count + 1) == -1) {
                            countAlreadyO++;
                        }
                        break;
                    }
                    if (count % 19 == 18) {
                        break;
                    }
                }
                for (int count = position; count > position - 6; count--) {
                    if (position % 19 == 0) {
                        break;
                    }
                    if (count == position) {
                        continue;
                    }
                    if (mArrGameBoard.get(count) == -1) {
                        countO++;
                    } else if (mArrGameBoard.get(count) == 1) {
                        countX++;
                        break;
                    } else {
                        if (count % 19 != 0 && mArrGameBoard.get(count - 1) == 1) {
                            countAlreadyX++;
                        }
                        if (count % 19 != 0 && mArrGameBoard.get(count - 1) == -1) {
                            countAlreadyO++;
                        }
                        break;
                    }
                    if (count % 19 == 0) {
                        break;
                    }
                }
                if (countX == 2 && countO < 5) {
                    return 0;
                }
                if (countAlreadyX == 1 && countX == 1 && countO < 4) {
                    return 0;
                }
                return mDefenseList.get(countO) + mDefenseList.get(countAlreadyO) - mAttachList.get(countX) - mAttachList.get(countAlreadyX);
            }

            private void xWin() {
                mReplayList.add(-2);
                Toast.makeText(myContext, "X win", Toast.LENGTH_LONG).show();
                for (int j = 0; j < 19 * 19; j++) {
                    if (mArrGameBoard.get(j) == 0) mArrGameBoard.set(j, 7);
                }

            }

            private void oWin() {
                mReplayList.add(-1);
                Toast.makeText(myContext, "O win", Toast.LENGTH_LONG).show();
                for (int j = 0; j < 19 * 19; j++) {
                    if (mArrGameBoard.get(j) == 0) mArrGameBoard.set(j, 7);
                }
                notifyDataSetChanged();
            }

            private int checkRow(int i, int i1) {
                int positionOnRow = i / Const.NET_SIZE * Const.NET_SIZE;
                int score = 0;
                while (positionOnRow < i / Const.NET_SIZE * Const.NET_SIZE + Const.NET_SIZE) {
                    if (mArrGameBoard.get(positionOnRow) == i1) {
                        score = score + i1;
                    } else {
                        score = 0;
                    }
                    if (score == 5 * i1) {
                        if (positionOnRow % Const.NET_SIZE == 18 || positionOnRow % Const.NET_SIZE == 4) {
                            return i1;
                        }
                        if (mArrGameBoard.get(positionOnRow + 1) != -i1 || mArrGameBoard.get(positionOnRow - 5) != -i1) {
                            return i1;
                        }
                    }

                    positionOnRow++;
                }
                return 0;

            }

            private int checkColumn(int i, int i1) {
                int positionOnColumn = i % Const.NET_SIZE;
                int score = 0;
                while (positionOnColumn < Const.NET_SIZE * Const.NET_SIZE) {
                    if (mArrGameBoard.get(positionOnColumn) == i1) {
                        score = score + i1;
                    } else {
                        score = 0;
                    }
                    if (score == 5 * i1) {
                        if (positionOnColumn / Const.NET_SIZE == 18 || positionOnColumn / Const.NET_SIZE == 4) {
                            return i1;
                        }
                        if (mArrGameBoard.get(positionOnColumn + Const.NET_SIZE) != -i1 || mArrGameBoard.get(positionOnColumn - Const.NET_SIZE * 5) != -i1) {
                            return i1;
                        }
                    }

                    positionOnColumn = positionOnColumn + Const.NET_SIZE;
                }
                return 0;

            }

            private int checkDiagonalLeftToRight(int i, int i1) {
                int positionOnDiagonal = i;
                int score = 0;
                while (positionOnDiagonal % Const.NET_SIZE != 0 && positionOnDiagonal / 19 != 0) {
                    positionOnDiagonal = positionOnDiagonal - 20;
                }
                do {
                    if (mArrGameBoard.get(positionOnDiagonal) == i1) {
                        score = score + i1;
                    } else {
                        score = 0;
                    }
                    if (score == 5 * i1) {
                        if (positionOnDiagonal % Const.NET_SIZE == 18 || positionOnDiagonal / Const.NET_SIZE == 18 || positionOnDiagonal / Const.NET_SIZE == 4 || positionOnDiagonal % Const.NET_SIZE == 4) {
                            return i1;
                        }
                        if (mArrGameBoard.get(positionOnDiagonal + 20) != -i1 || mArrGameBoard.get(positionOnDiagonal - 20 * 5) != -i1) {
                            return i1;
                        }
                    }
                    positionOnDiagonal = positionOnDiagonal + 20;
                } while (positionOnDiagonal % 19 != 0 && positionOnDiagonal / 19 != 19);
                return 0;

            }

            private int checkDiagonalRightToLeft(int i, int i1) {
                int positionOnDiagonal = i;
                int score = 0;
                while (positionOnDiagonal % 19 != 18 && positionOnDiagonal / 19 != 0) {
                    positionOnDiagonal = positionOnDiagonal - 18;
                }
                do {
                    if (mArrGameBoard.get(positionOnDiagonal) == i1) {
                        score = score + i1;
                    } else {
                        score = 0;
                    }
                    if (score == 5 * i1) {
                        if (positionOnDiagonal % 19 == 0 || positionOnDiagonal / 19 == 18 || positionOnDiagonal / 19 == 3 || positionOnDiagonal % 19 == 14) {
                            return i1;
                        }
                        if (mArrGameBoard.get(positionOnDiagonal + 18) != -i1 || mArrGameBoard.get(positionOnDiagonal - 18 * 5) != -i1) {
                            return i1;
                        }
                    }
                    positionOnDiagonal = positionOnDiagonal + 18;

                } while (positionOnDiagonal % 19 != 18 && positionOnDiagonal / 19 != 19);
                return 0;
            }

        });

        return view;
    }
}

package com.cob.tictactoe.ui.chosegametype.presenter

import android.net.wifi.WpsInfo
import android.net.wifi.p2p.WifiP2pConfig
import android.net.wifi.p2p.WifiP2pDevice
import android.net.wifi.p2p.WifiP2pInfo
import android.net.wifi.p2p.WifiP2pManager
import com.cob.tictactoe.R
import com.cob.tictactoe.receiver.DirectActionListener
import com.cob.tictactoe.ui.chosegametype.ui.ChoseGameTypeView
import com.cob.tictactoe.utils.LogUtil

class ChoseGameTypePresenter(var choseGameTypeView: ChoseGameTypeView) :
    IChoseGameTypePresenter {

    override fun connectWifiP2p(
        wifiP2pDevice: WifiP2pDevice,
        channel: WifiP2pManager.Channel,
        wifiP2pManager: WifiP2pManager
    ) {

        val config = WifiP2pConfig()
        if (config.deviceAddress != null) {
            config.deviceAddress = wifiP2pDevice.deviceAddress
            config.wps.setup = WpsInfo.PBC
            wifiP2pManager.connect(channel, config, object : WifiP2pManager.ActionListener {
                override fun onSuccess() {
                    choseGameTypeView.showNoticeMessage(R.string.connect_success)
                    LogUtil.d("onSuccess")
                }

                override fun onFailure(reason: Int) {
                    choseGameTypeView.showNoticeMessage(R.string.connect_fail)
                    LogUtil.d("onFailure")
                }
            })
        }
    }

    override fun disConnectWifiP2p(
        channel: WifiP2pManager.Channel,
        wifiP2pManager: WifiP2pManager
    ) {
        wifiP2pManager.removeGroup(channel, object : WifiP2pManager.ActionListener {
            override fun onFailure(reason: Int) {
            }

            override fun onSuccess() {
                choseGameTypeView.showNoticeMessage(R.string.disconnect_success)
            }
        })

    }


}
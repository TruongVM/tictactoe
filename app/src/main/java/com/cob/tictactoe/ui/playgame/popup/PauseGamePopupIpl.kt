package com.cob.tictactoe.ui.playgame.popup

interface PauseGamePopupIpl {
    fun closePopupResumeGame()
    fun closePopupAndNewGame()
}
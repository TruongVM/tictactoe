package com.cob.tictactoe.ui.playgame.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.cob.tictactoe.R
import com.cob.tictactoe.utils.Const

class GameBoardAdapter(
    var mArrGameBoard: ArrayList<Int>,
    var mArrReplay: ArrayList<Int>,
    var mArrRedo: ArrayList<Int>,
    var onItemClick: (position: Int) -> Unit
) : BaseAdapter() {

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val inflater =
            parent?.context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.item_game_board, parent, false)
        val item = view.findViewById<ImageView>(R.id.txtItemGameBoard)
        if (mArrGameBoard[position] == Const.PLAYER_1) {
            item.setImageResource(R.drawable.ic_circle)
        } else if (mArrGameBoard[position] == Const.PLAYER_2) {
            item.setImageResource(R.drawable.ic_cross)
        }
        if (mArrReplay.size > 0) {
            if (mArrReplay[mArrReplay.size - 1] == position) {
                item.setBackgroundResource(R.drawable.bg_item_player_moved)
            }
        }
        item.setOnClickListener {
            onItemClick.invoke(position)
            notifyDataSetChanged()
            Log.d("TruongVM", "press")
        }
        return view
    }

    override fun getItem(position: Int): Any {
        return mArrGameBoard[position]
    }

    override fun getCount(): Int {
        return mArrGameBoard.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

}
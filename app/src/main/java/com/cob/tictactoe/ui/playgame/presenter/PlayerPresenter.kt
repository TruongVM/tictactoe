package com.cob.tictactoe.ui.playgame.presenter

import android.os.Bundle
import android.util.Log
import android.view.View
import com.cob.tictactoe.basse.timmer.CountDownTimerTask
import com.cob.tictactoe.ui.playgame.ui.IPlayerView
import com.cob.tictactoe.utils.AppUtils
import com.cob.tictactoe.utils.Const

class PlayerPresenter(var playerView: IPlayerView) : IPlayerPresenter,
    CountDownTimerTask.UpdateFromTimer {
    private var mPlayType = 0
    private var mColorForType = -1

    private var mGameLevel = 0

    private var mIsMusicOn = true

    private var mIsSoundOn = true

    private lateinit var mCountDownTimerTask: CountDownTimerTask

    override fun onCreate(bundle: Bundle) {
        mPlayType = bundle.getInt(Const.KEY_GAME_TYPE)
        isPlayVsCom(mPlayType == 1)
        displayUndoRedo(mPlayType)


        mColorForType = bundle.getInt(Const.KEY_COLOR_TYPE)
        playerView.colorType(mColorForType)
        updateActionBarColor(mColorForType)
        showJoinGamePopup(mColorForType)

        mGameLevel = bundle.getInt(Const.KEY_GAME_LEVEL)
        setTimeForEachLevel(mGameLevel)

        mIsMusicOn = AppUtils.getMusicStatus()
        updateMusicView(mIsMusicOn)

        mIsSoundOn = AppUtils.getSondStatus()
        updateSoundView(mIsSoundOn)

    }

    override fun setMusicStatus() {
        mIsMusicOn = !mIsMusicOn
        updateMusicView(mIsMusicOn)
        AppUtils.setMusicOffOn(mIsMusicOn)
    }

    override fun setSoundStatus() {
        mIsSoundOn = !mIsSoundOn
        updateSoundView(mIsSoundOn)
        AppUtils.setSoundOffOn(mIsSoundOn)
    }

    override fun showAds(showFrom: Int) {

    }

    private fun displayUndoRedo(playType: Int) {
        val visibility = if (playType == Const.PLAYER_VS_COM) View.VISIBLE else View.GONE
        playerView.displayUndoReDo(visibility)
    }

    private fun isPlayVsCom(b: Boolean) {
        playerView.isPlayVsCom(b)
    }

    private fun updateMusicView(isMusicOn: Boolean) {
        playerView.updateMusicView(isMusicOn)
    }

    private fun updateSoundView(isSoundOn: Boolean) {
        playerView.updateSoundView(isSoundOn)
    }

    private fun updateActionBarColor(color: Int) {
        playerView.updateCustomActionBarColor(color)
    }

    private fun setTimeForEachLevel(gameLevel: Int) {
        val time: Int = when (gameLevel) {
            Const.mLevelHard -> {
                Const.TIME_FOR_HARD_LEVEL
            }
            Const.mLevelMedium -> {
                Const.TIME_FOR_MEDIUM_LEVEL
            }
            else -> {
                Const.TIME_FOR_MEDIUM_LEVEL
            }
        }
        Log.d("setTimeForEachLevel", "time= $time")

        CountDownTimerTask.MAX_PROGRESS = time
    }

    override fun createCountDownTimer() {
        mCountDownTimerTask = CountDownTimerTask(this)
    }

    override fun updateUIFromTimer(progress: Long) {
        playerView.updateUIFromTimer(progress)
    }

    fun showJoinGamePopup(color: Int) {
        playerView.displayJoinGamePopup(color)
    }

    override fun timerFinish() {
        playerView.timerFinish()
    }

    override fun cancelCountDownTimer() {
        playerView.cancelCountDownTimer()
    }

    override fun stopCountDown() {
        mCountDownTimerTask.stopCountDown()
    }

    override fun startTimer() {
        mCountDownTimerTask.startTimer()
    }

    override fun stopTimer() {
        mCountDownTimerTask.stopTimer()
    }
}
package com.cob.tictactoe.ui.playgame.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.view.View
import androidx.core.content.ContextCompat
import com.cob.tictactoe.R
import com.cob.tictactoe.basse.fragments.BaseFragment
import com.cob.tictactoe.basse.game.ai.AIBoard
import com.cob.tictactoe.basse.timmer.CountDownTimerTask
import com.cob.tictactoe.ui.playgame.adapter.GameBoardAdapter
import com.cob.tictactoe.ui.playgame.popup.*
import com.cob.tictactoe.ui.playgame.presenter.IPlayerPresenter
import com.cob.tictactoe.ui.playgame.presenter.PlayerPresenter
import com.cob.tictactoe.utils.AppUtils
import com.cob.tictactoe.utils.Const
import com.cob.tictactoe.utils.LogUtil
import kotlinx.android.synthetic.main.activity_play_game.*
import kotlinx.android.synthetic.main.activity_play_game.imgPlayer
import kotlinx.android.synthetic.main.activity_play_game.imgPlayer2


class PlayGameActivity : BaseFragment(), View.OnClickListener, JoinGamePopupIpl,
    AIBoard.PlayStatusListener,
    View.OnTouchListener, IPlayerView, EndGamePopupIpl, PauseGamePopupIpl {
    private var mScorePlayer1 = 0
    private var mScorePlayer2 = 0
    private var isPlayer1GoFirst = false

    private var isPlayVsCom = false
    private var isPlayerTurn = true


    private var mScaleFactor = 1f
    private var mPosX = 0f
    private var mPosY = 0f
    private var mLastTouchX = 0f
    private var mLastTouchY = 0f
    private var mScaleDetector: ScaleGestureDetector? = null
    private var mPointerCount = 1
    private var isOnTouch = false

    private var isInGame = true
    private lateinit var mPresenter: IPlayerPresenter
    private var mColorType = 0


    override fun getContentRes(): Int = R.layout.activity_play_game

    override fun actionBack(): Boolean = false

    companion object {

        fun newInstance(data: Bundle): PlayGameActivity {
            val fragment =
                PlayGameActivity()
            val bundle = Bundle()
            bundle.putInt(Const.KEY_GAME_STEPS, data.getInt(Const.KEY_GAME_STEPS))
            bundle.putInt(Const.KEY_GAME_TYPE, data.getInt(Const.KEY_PLAY_TYPE))
            bundle.putInt(Const.KEY_COLOR_TYPE, data.getInt(Const.KEY_COLOR_TYPE))
            bundle.putInt(Const.KEY_GAME_LEVEL, data.getInt(Const.KEY_GAME_LEVEL))
            bundle.putBoolean(
                Const.KEY_IS_PLAYER_1_GO_FIRST,
                data.getBoolean(Const.KEY_IS_PLAYER_1_GO_FIRST)
            )
            fragment.arguments = bundle
            return fragment
        }
    }

    @SuppressLint("StringFormatMatches")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter = PlayerPresenter(this)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mPresenter.onCreate(arguments!!)

        isPlayer1GoFirst = arguments!!.getBoolean(Const.KEY_IS_PLAYER_1_GO_FIRST)

        initView()
        initScaleDetector()
        initAI()
        initTimer()
        newGame(false)
        mScaleFactor = 1.5f
        scaleGameBoard(mScaleFactor)
        updateReDo(false)
        updateUnDo(false)
        txtScoreCount.text = String.format(getString(R.string.count_score, 0, 0))
        txtUnDoBadge.text = AppUtils.mUnDoTimes.toString()


    }

    private fun initView() {
        imgBackHome.setOnClickListener(this)
        imgMusic.setOnClickListener(this)
        imgSound.setOnClickListener(this)
        imgResetGame.setOnClickListener(this)
        imgReDo.setOnClickListener(this)
        imgUnDo.setOnClickListener(this)
        imgPauseGame.setOnClickListener(this)
        gridGameBoard.setOnTouchListener(this)
    }

    override fun displayJoinGamePopup(color: Int) {
        showJoinGamePopup(color)
    }

    override fun updateCustomActionBarColor(color: Int) {
        customActionbar.backgroundTintList = ContextCompat.getColorStateList(
            activity!!,
            color
        )
        customBottombar.backgroundTintList = ContextCompat.getColorStateList(
            activity!!,
            color
        )
    }

    override fun displayUndoReDo(visibility: Int) {
        imgUnDo.visibility = visibility
        imgReDo.visibility = visibility
    }

    override fun updateMusicView(b: Boolean) {
        imgMusic.isSelected = b
    }

    override fun updateSoundView(b: Boolean) {
        imgSound.isSelected = b
    }

    override fun isPlayVsCom(b: Boolean) {
        isPlayVsCom = b
    }

    override fun onClick(v: View?) {

        when (v) {
            imgBackHome -> {
                goBack()
            }

            imgPauseGame -> {
                pauseGame()
            }
            imgMusic -> {
                mPresenter.setMusicStatus()
            }
            imgSound -> {
                mPresenter.setSoundStatus()
            }
            imgResetGame -> {
                pauseTimer()
                newGame(true)
            }

            imgReDo -> {
                if (isInGame) {
                    ai.reDo()
                    adapter.notifyDataSetChanged()
                }
            }

            imgUnDo -> {
                if (isInGame) {
                    Log.d("TruongVM", "getReDoListSize= " + ai.getReDoListSize())
                    pauseTimer()
                    if (ai.getReDoListSize() >= 1) {
                        updateReDo(true)
                    } else {
                        updateReDo(false)
                    }

                    ai.unDo()
                    adapter.notifyDataSetChanged()
                    startTimer()
                }

            }

        }
    }

    private fun pauseGame() {
        if (isInGame) {
            mPresenter.stopTimer()
            showPauseGamePopup()
        }

    }

    private fun newGame(isNeedStartTimer: Boolean) {
        isInGame = true
        initAI()
        initGameBoard()
        if (isNeedStartTimer) {
            startTimer()
        }
    }


    override fun joinGame() {
        startTimer()
    }

    override fun cancelGame() {
        goBack()
    }

    lateinit var ai: AIBoard
    private fun initAI() {
        ai = AIBoard(this)
    }


    override fun oWin() {
        LogUtil.d("oWin")

        isInGame = false
        mPresenter.stopCountDown()
        mScorePlayer1 += 1
        txtScoreCount.text =
            String.format(getString(R.string.count_score, mScorePlayer1, mScorePlayer2))
        showEndGamePopup("Player 1", mColorType)

    }

    override fun colorType(color: Int) {
        mColorType = color
    }

    override fun closePopupResumeGame() {
        startTimer()
    }

    override fun closePopupAndNewGame() {
        newGame(true)
    }

    private fun showPauseGamePopup() {
        val popup = PauseGamePopup(activity!!, this, mColorType)
        popup.showPopup()
    }

    override fun xWin() {
        isInGame = false
        mPresenter.stopCountDown()
        LogUtil.d("xWin")
        mScorePlayer2 += 1
        txtScoreCount.text =
            String.format(getString(R.string.count_score, mScorePlayer1, mScorePlayer2))
        showEndGamePopup("Player 2", mColorType)
    }

    lateinit var adapter: GameBoardAdapter
    private fun initGameBoard() {

        adapter =
            GameBoardAdapter(ai.mArrGameBoard, ai.mUnDoList, ai.mReDoList, ::onItemBoardClick)
        gridGameBoard.adapter = adapter
        if (!isPlayer1GoFirst && isPlayVsCom) {
            ai.mArrGameBoard[180] = 1
            ai.mUnDoList.add(180)
            updateUiFollowTurn()
        }

        adapter.notifyDataSetChanged()


    }

    private fun initScaleDetector() {
        mScaleDetector = ScaleGestureDetector(
            context,
            ScaleListener()
        )
    }

    private fun onItemBoardClick(position: Int) {
        Log.d(
            "onItemBoardClick",
            "position= $position mPointerCount= $mPointerCount isOnTouch= $isOnTouch"
        )
        if (!isInGame || !isPlayerTurn) {
            return
        }
        if (mPointerCount < 2 && !isOnTouch) {
            if (isPlayVsCom) {
                pauseTimer()
                isPlayerTurn = false
                ai.playerMove(position)
                startTimer()
                adapter.notifyDataSetChanged()
                updateUiFollowTurn()

                Handler().postDelayed({
                    //Do something after 100ms
                    pauseTimer()
                    ai.aiMove(position)
                    Log.d("postDelayed", "isInGame= $isInGame")
                    if (isInGame) {
                        startTimer()
                    }
                    updateUiFollowTurn()
                    adapter.notifyDataSetChanged()
                    if (ai.getUnDoListSize() >= 2) {
                        updateUnDo(true)
                    } else {
                        updateReDo(false)
                    }
                    isPlayerTurn = true
                }, 500)
            } else {
                pauseTimer()
                ai.moveBetweenPlayerVsPlayer(position, isPlayer1GoFirst)
                adapter.notifyDataSetChanged()
                if (isInGame) {
                    startTimer()
                }
            }


        }


    }


    inner class ScaleListener :
        ScaleGestureDetector.SimpleOnScaleGestureListener() {

        override fun onScale(detector: ScaleGestureDetector): Boolean {
            mScaleFactor *= detector.scaleFactor
            mScaleFactor = Math.max(
                1f,
                Math.min(mScaleFactor, 1.7f)
            )
            Log.d("Nhinh", "ScaleListener")
            scaleGameBoard(mScaleFactor)
            return true
        }
    }

    private fun scaleGameBoard(scale: Float) {
        gridGameBoard.scaleX = scale
        gridGameBoard.scaleY = scale
    }


    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        Log.d("Nhinh", "onTouch mScaleDetector= ${mScaleDetector?.isInProgress}")
        mPointerCount = event?.pointerCount!!
        mScaleDetector!!.onTouchEvent(event)
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                gridGameBoard.isClickable = false
                isOnTouch = true
                mLastTouchX = event.x
                mLastTouchY = event.y
            }
            MotionEvent.ACTION_MOVE -> {
                gridGameBoard.isClickable = false
                isOnTouch = true
                Log.d("Nhinh", "point count " + event.pointerCount)
                var isScaling = mScaleDetector!!.isInProgress
                Log.d("TruongVM", "isScaling= $isScaling")
                if (event.pointerCount == 1) {
                    if (mScaleFactor > 1) {
                        Log.d("TruongVM", "ACTION_MOVE= $isScaling")
                        val xMove = event.rawX
                        val yMove = event.rawY
                        val dx = xMove - mLastTouchX
                        val dy = yMove - mLastTouchY
                        if (dx >= 5) {
                            mPosX += 15
                        } else if (dx < -5) {
                            mPosX -= 15
                        }
                        if (dy >= 5) {
                            mPosY += 15
                        } else if (dy < -5) {
                            mPosY -= 15
                        }

                        gridGameBoard.translationX = mPosX
                        gridGameBoard.translationY = mPosY

                        mLastTouchX = xMove
                        mLastTouchY = yMove
                    } else {
                        gridGameBoard.translationX = 0f
                        gridGameBoard.translationY = 0f
                    }
                } else {
                    return false
                }


            }
            MotionEvent.ACTION_UP -> {
                isOnTouch = false
                gridGameBoard.isClickable = true
            }

            MotionEvent.ACTION_POINTER_UP -> {
                isOnTouch = false
                gridGameBoard.isClickable = true
            }

        }

        return true
    }

    private fun updateUnDo(isEnable: Boolean) {
        imgUnDo.isSelected = isEnable
        imgUnDo.isEnabled = isEnable
    }

    private fun updateReDo(isEnable: Boolean) {
        imgReDo.isSelected = isEnable
        imgReDo.isEnabled = isEnable
    }

    private fun initTimer() {
        progressBarPlayer.max = CountDownTimerTask.MAX_PROGRESS
        progressbarPlayer2.max = CountDownTimerTask.MAX_PROGRESS
        progressBarPlayer.progress = CountDownTimerTask.MAX_PROGRESS
        progressbarPlayer2.progress = CountDownTimerTask.MAX_PROGRESS
        mPresenter.createCountDownTimer()

    }

    private fun pauseTimer() {
        mPresenter.stopCountDown()
    }

    private fun startTimer() {

        mPresenter.startTimer()
    }

    override fun timerFinish() {
        LogUtil.d("timerFinish")
        isInGame = false
        resetProgressbar()
        val turn = ai.getCurrentUser(isPlayer1GoFirst)
        Log.d("Khoai", "updateUIFromTimer turn= $turn")
        if (turn == Const.PLAYER_1) {
            mScorePlayer2 += 1
            showEndGamePopup("Player 2", mColorType)
        } else if (turn == Const.PLAYER_2) {
            mScorePlayer1 += 1
            showEndGamePopup("Player 1", mColorType)
        }
        txtScoreCount.text =
            String.format(getString(R.string.count_score, mScorePlayer1, mScorePlayer2))
    }

    override fun updateUIFromTimer(progress: Long) {
        LogUtil.d("progress= $progress")
        val turn = ai.getCurrentUser(isPlayer1GoFirst)
        Log.d("Khoai", "updateUIFromTimer turn= $turn")
        if (turn == Const.PLAYER_1) {
            progressBarPlayer.progress = progress.toInt()
        } else if (turn == Const.PLAYER_2) {
            progressbarPlayer2.progress = progress.toInt()
        }


    }

    private fun updateBadgeUnDo() {
        AppUtils.mUnDoTimes -= 1
        if (AppUtils.mUnDoTimes > 0) {
            txtUnDoBadge.text = AppUtils.mUnDoTimes.toString()
        } else if (AppUtils.mUnDoTimes == 0) {
            mPresenter.showAds(Const.SHOW_ADS_FROM_TIMES_PLAY)
        }

    }

    override fun updateUndoTimes(times: Int) {
        AppUtils.mUnDoTimes = times
    }

    override fun cancelCountDownTimer() {
        resetProgressbar()
    }

    private fun resetProgressbar() {

        progressbarPlayer2.max = CountDownTimerTask.MAX_PROGRESS
        progressbarPlayer2.progress = CountDownTimerTask.MAX_PROGRESS

        progressBarPlayer.progress = CountDownTimerTask.MAX_PROGRESS
        progressBarPlayer.max = CountDownTimerTask.MAX_PROGRESS
    }


    override fun onDestroyView() {
        super.onDestroyView()
        mPresenter.stopTimer()
    }

    private fun showJoinGamePopup(color: Int) {
        val popup = JoinGamePopup(activity!!, this, CountDownTimerTask.MAX_PROGRESS, color)
        popup.showPopup()
    }

    private fun showEndGamePopup(winner: String, color: Int) {
        val popup = EndGamePopup(activity!!, this, winner, color)
        popup.showPopup()
    }

    override fun closeAndNewGame() {
        newGame(true)
    }

    override fun closeWithoutAnyThing() {
    }

    private fun updateUiFollowTurn() {
        val turn = ai.getCurrentUser(isPlayer1GoFirst)
        Log.d("Khoai", "turn= $turn")
        if (turn == Const.PLAYER_1) {
            imgPlayer.isSelected = true
            imgPlayer2.isSelected = false

        } else if (turn == Const.PLAYER_2) {
            imgPlayer2.isSelected = true
            imgPlayer.isSelected = false
        }
    }


}

package com.cob.tictactoe.ui.chosegametype.presenter

import android.net.wifi.p2p.WifiP2pDevice
import android.net.wifi.p2p.WifiP2pManager

interface IChoseGameTypePresenter {
    fun connectWifiP2p(
        wifiP2pDevice: WifiP2pDevice,
        channel: WifiP2pManager.Channel,
        wifiP2pManager: WifiP2pManager
    )

    fun disConnectWifiP2p(
        channel: WifiP2pManager.Channel,
        wifiP2pManager: WifiP2pManager
    )
}
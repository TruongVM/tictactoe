package com.cob.tictactoe.ui.settings.view

import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.CompoundButton
import com.cob.tictactoe.MainActivity
import com.cob.tictactoe.R
import com.cob.tictactoe.basse.fragments.BaseFragment
import com.cob.tictactoe.ui.setpincode.view.SetPinCodeFragment
import com.cob.tictactoe.utils.AppUtils
import com.cob.tictactoe.utils.Const
import com.cob.tictactoe.utils.LogUtil
import com.cob.tictactoe.utils.SpManager
import kotlinx.android.synthetic.main.fragment_settings.*

class SettingFragment : BaseFragment(), View.OnClickListener {

    override fun getContentRes(): Int = R.layout.fragment_settings

    override fun actionBack(): Boolean = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        hideFloatButton()
        if (SpManager.getInstance().getString(
                AppUtils.KEY_SETTING_LANGUAGE,
                AppUtils.KEY_LANGUAGE_ENG
            ) == AppUtils.KEY_LANGUAGE_ENG
        ) {
            radioButtonEnglish.isChecked = true
            setLanguageEnglish()
        } else {
            setLanguageVietnamese()
            radioButtonVietnamese.isChecked = true
        }

        radioButtonEnglish.setOnCheckedChangeListener { buttonView, isChecked ->
            handleRadioButton(
                buttonView,
                isChecked
            )
        }

        radioButtonVietnamese.setOnCheckedChangeListener { buttonView, isChecked ->
            handleRadioButton(
                buttonView,
                isChecked
            )
        }

        txtSettingPinCode.setOnClickListener(this)

        txtChangePinCode.setOnClickListener(this)

        txtRemovePinCode.setOnClickListener(this)
    }

    private fun handleRadioButton(compoundButton: CompoundButton, isChecked: Boolean) {
        if (isChecked) {
            LogUtil.d("compoundButton= $compoundButton")
            if (compoundButton == radioButtonVietnamese) {
                setLanguageVietnamese()
            } else {
                setLanguageEnglish()
            }
        }
        val languageChanged: LanguageChanged = activity as MainActivity
        languageChanged.onLanguageChanged()

//        Handler().postDelayed({
//            AppUtils.restartApp(activity!!)
//        }, 100)
    }

    private fun setLanguageVietnamese() {
        settingLanguage()
        SpManager.getInstance()
            .putString(AppUtils.KEY_SETTING_LANGUAGE, AppUtils.KEY_LANGUAGE_VIE)
    }

    private fun setLanguageEnglish() {
        settingLanguage()
        SpManager.getInstance()
            .putString(AppUtils.KEY_SETTING_LANGUAGE, AppUtils.KEY_LANGUAGE_ENG)
    }

    private fun updateFunctionPinCode() {
        val pinCode = SpManager.getInstance().getString(Const.KEY_PIN_CODE, "")
        if (pinCode.isNotEmpty()) {
            txtChangePinCode.visibility = View.VISIBLE
            txtRemovePinCode.visibility = View.VISIBLE
            txtSettingPinCode.isEnabled = false
            txtSettingPinCode.alpha = 0.4F
        } else {
            txtChangePinCode.visibility = View.GONE
            txtRemovePinCode.visibility = View.GONE
            txtSettingPinCode.isEnabled = true
            txtSettingPinCode.alpha = 1F
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun onResume() {
        super.onResume()
        updateFunctionPinCode()
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        super.onPrepareOptionsMenu(menu)
        menu!!.findItem(R.id.action_delete_all_note)?.isVisible = false
    }

    override fun onClick(v: View?) {
        when (v) {
            txtSettingPinCode -> {
                replaceFragment(SetPinCodeFragment.newInstance(Const.KEY_SETTING_PIN_CODE))
            }

            txtChangePinCode -> {
                replaceFragment(SetPinCodeFragment.newInstance(Const.KEY_CHANGE_PIN_CODE))
            }

            txtRemovePinCode -> {
                replaceFragment(SetPinCodeFragment.newInstance(Const.KEY_REMOVE_PIN_CODE))
            }
        }
    }

    interface LanguageChanged {
        fun onLanguageChanged()
    }


}
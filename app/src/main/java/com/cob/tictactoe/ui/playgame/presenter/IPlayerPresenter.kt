package com.cob.tictactoe.ui.playgame.presenter

import android.os.Bundle

interface IPlayerPresenter {
    fun onCreate(bundle: Bundle)
    fun createCountDownTimer()
    fun stopCountDown()
    fun startTimer()
    fun stopTimer()
    fun setMusicStatus()
    fun setSoundStatus()
    fun showAds(showFrom: Int)
}
package com.cob.tictactoe.ui.chosegametype.ui

interface ChoseGameTypeView {
    fun showNoticeMessage(messageId: Int)
}
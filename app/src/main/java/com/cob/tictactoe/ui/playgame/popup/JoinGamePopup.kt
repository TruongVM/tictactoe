package com.cob.tictactoe.ui.playgame.popup

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.CountDownTimer
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.cob.tictactoe.R
import kotlinx.android.synthetic.main.dialog_ready_to_start.view.*

class JoinGamePopup(context: Context, joinGamePopupIpl: JoinGamePopupIpl, time: Int, color: Int) {
    private var mBuilder: AlertDialog.Builder
    private lateinit var mAlertDialog: AlertDialog
    private var timeInMilliSecond = 5000L
    private val mSecond = 1000L
    lateinit var mCountDownTimer: CountDownTimer

    init {
        //Inflate the dialog with custom view
        val mDialogView =
            LayoutInflater.from(context).inflate(R.layout.dialog_ready_to_start, null)
        //AlertDialogBuilder
        mBuilder = AlertDialog.Builder(context)
            .setView(mDialogView)
        //show dialog

        mBuilder.setCancelable(false)
//        //login button click of custom layout
//        val timeInSecond = time / 1000
//        mDialogView.txtJoinGame.setOnClickListener {
//            //dismiss dialog
//            mAlertDialog.dismiss()
//            joinGamePopupIpl.joinGame()
//        }
//        mDialogView.txtCancelGame.setOnClickListener {
//            //dismiss dialog
//            mAlertDialog.dismiss()
//            joinGamePopupIpl.cancelGame()
//        }
        mDialogView.txtReady.setTextColor(
            ContextCompat.getColorStateList(
                context,
                color
            )
        )

        mCountDownTimer = object : CountDownTimer(timeInMilliSecond, mSecond) {
            override fun onFinish() {
                mAlertDialog.dismiss()
                joinGamePopupIpl.joinGame()
            }

            override fun onTick(millisUntilFinished: Long) {
                val time = (millisUntilFinished / 1000).toString()
                if (millisUntilFinished > 0 && !time.equals("0")) {
                    mDialogView.txtReady.text = time
                } else {
                    mDialogView.txtReady.text = "Start"
                }

            }
        }

        mCountDownTimer.start()

    }

    fun showPopup() {
        mAlertDialog = mBuilder.create()
        mAlertDialog.window?.attributes?.windowAnimations = R.style.DialogTheme
        mAlertDialog.window?.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        mAlertDialog.show()
        mAlertDialog.window?.setLayout(1000, 1000)
    }
}
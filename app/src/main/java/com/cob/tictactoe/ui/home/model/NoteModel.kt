package com.cob.tictactoe.ui.home.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(
    tableName = "list_note"
)
data class NoteModel(
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "noteID")
    var noteID: Int?,

    @ColumnInfo(name = "noteTitle")
    var noteTitle: String,

    @ColumnInfo(name = "noteShortContent")
    var noteShortContent: String,

    @ColumnInfo(name = "noteContent")
    var noteContent: String,

    @ColumnInfo(name = "update")
    var update: String,

    @ColumnInfo(name = "audioUrl")
    var audioUrl: String?,

    @ColumnInfo(name = "imageUrl")
    var imageUrl: String?,

    @ColumnInfo(name = "videoUrl")
    var videoUrl: String?
) {

    override fun equals(other: Any?): Boolean {
        if (other is NoteModel) {
            return noteID == other.noteID
        }
        return false
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }

}
package com.cob.tictactoe.ui.inputpincodetologin.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.cob.tictactoe.MainActivity
import com.cob.tictactoe.R
import com.cob.tictactoe.utils.Const
import com.cob.tictactoe.utils.LogUtil
import com.cob.tictactoe.utils.SpManager
import kotlinx.android.synthetic.main.fragment_set_pin_code.*

class EnterPinCodeToLogin : AppCompatActivity(), View.OnClickListener {

    private var mOrder: Int = 0
    private var mCurrentPinCode = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_set_pin_code)
        actionBarOnTop.visibility = View.GONE
        mCurrentPinCode = SpManager.getInstance().getString(Const.KEY_PIN_CODE, "")

        txtPin1.setOnClickListener(this)
        txtPin2.setOnClickListener(this)
        txtPin3.setOnClickListener(this)
        txtPin4.setOnClickListener(this)
        txtPin5.setOnClickListener(this)
        txtPin6.setOnClickListener(this)
        txtPin7.setOnClickListener(this)
        txtPin8.setOnClickListener(this)
        txtPin9.setOnClickListener(this)
        txtPin0.setOnClickListener(this)
        imgDel.setOnClickListener(this)
        imgOk.setOnClickListener(this)
        imgDel.setOnLongClickListener {
            mOrder = -1
            setTextView("")
            false
        }

    }

    override fun onClick(v: View?) {
        when (v) {
            txtPin0 -> {
                setTextView("0")
            }
            txtPin1 -> {
                setTextView("1")
            }
            txtPin2 -> {
                setTextView("2")
            }
            txtPin3 -> {
                setTextView("3")
            }
            txtPin4 -> {
                setTextView("4")
            }
            txtPin5 -> {
                setTextView("5")
            }
            txtPin6 -> {
                setTextView("6")
            }
            txtPin7 -> {
                setTextView("7")
            }
            txtPin8 -> {
                setTextView("8")
            }
            txtPin9 -> {
                setTextView("9")
            }

            imgOk -> {
                handleImgOkAction()
            }

            imgDel -> {
                if (mOrder > 0) {
                    mOrder -= 1
                }
                setTextView("")
            }
        }

    }

    private fun setTextView(number: String?) {
        when (mOrder) {
            -1 -> {
                edtPinCode1.setText("")
                edtPinCode2.setText("")
                edtPinCode3.setText("")
                edtPinCode4.setText("")
                mOrder = 0
            }
            0 -> {
                edtPinCode1.setText(number)
            }
            1 -> {
                edtPinCode2.setText(number)
            }
            2 -> {
                edtPinCode3.setText(number)
            }
            3 -> {
                edtPinCode4.setText(number)
            }
        }
        if (!number.equals("")) {
            mOrder += 1
            if (mOrder > 4) {
                mOrder -= 1
            }
        }
        updateImgOk()
    }

    private fun updateImgOk() {
        if (edtPinCode4.text.isNotEmpty()) {
            imgOk.setImageResource(R.drawable.ic_img_ok_normal)
        } else {
            imgOk.setImageResource(R.drawable.ic_img_ok_disable)
        }
    }

    private fun handleImgOkAction() {
        LogUtil.d("mCurrentPinCode= $mCurrentPinCode")
        LogUtil.d("getPinCode()= " + getPinCode())
        if (mCurrentPinCode == getPinCode()) {
            val intent = Intent(this.applicationContext, MainActivity::class.java)
            startActivity(intent)
        } else {
            mOrder = -1
            setTextView("")
            Toast.makeText(
                this,
                resources.getString(R.string.validate_pin_code),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun getPinCode(): String = edtPinCode1.text.toString().plus(edtPinCode2.text.toString())
        .plus(edtPinCode3.text.toString()).plus(edtPinCode4.text.toString())

}

package com.cob.tictactoe.ui.setpincode.view


import android.os.Bundle
import android.view.View
import com.cob.tictactoe.R
import com.cob.tictactoe.basse.fragments.BaseFragment
import com.cob.tictactoe.ui.settings.popupremovepincode.DeletePinCodeIpl
import com.cob.tictactoe.ui.settings.popupremovepincode.DeletePinCodePopup
import com.cob.tictactoe.utils.Const
import com.cob.tictactoe.utils.LogUtil
import com.cob.tictactoe.utils.SpManager
import kotlinx.android.synthetic.main.fragment_set_pin_code.*
import kotlinx.android.synthetic.main.layout_action_bar_only_back.*


class SetPinCodeFragment : BaseFragment(), View.OnClickListener, DeletePinCodeIpl {

    private var mOrder: Int = 0

    private var mStepToSettingPinCode: Int = 0

    private var mStepToChangePinCode = 0

    private var mFirstPinCode = ""

    private var mCurrentPinCode = ""

    private var mKeyAction = -1

    override fun getContentRes(): Int = R.layout.fragment_set_pin_code

    override fun actionBack(): Boolean = false

    companion object {

        fun newInstance(keyAction: Int): SetPinCodeFragment {
            val fragment =
                SetPinCodeFragment()
            val bundle = Bundle()
            bundle.putInt(KEY_ACTION, keyAction)
            fragment.arguments = bundle
            return fragment
        }

        var KEY_ACTION = "key_action"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mKeyAction = arguments!!.getInt(KEY_ACTION)


        updateTitle()
        btnBack.setOnClickListener(this)
        txtPin1.setOnClickListener(this)
        txtPin2.setOnClickListener(this)
        txtPin3.setOnClickListener(this)
        txtPin4.setOnClickListener(this)
        txtPin5.setOnClickListener(this)
        txtPin6.setOnClickListener(this)
        txtPin7.setOnClickListener(this)
        txtPin8.setOnClickListener(this)
        txtPin9.setOnClickListener(this)
        txtPin0.setOnClickListener(this)
        imgDel.setOnClickListener(this)
        imgOk.setOnClickListener(this)
        imgDel.setOnLongClickListener {
            mOrder = -1
            setTextView("")
            false
        }

        mCurrentPinCode = SpManager.getInstance().getString(Const.KEY_PIN_CODE, "")

        setEnableDrawer(false)

    }

    private fun updateTitle() {
        when (mKeyAction) {
            Const.KEY_SETTING_PIN_CODE -> {
                tvTitle.text = resources.getString(R.string.setting_pin_code)
            }
            Const.KEY_CHANGE_PIN_CODE -> {
                tvTitle.text = resources.getString(R.string.change_pin_code)
            }
            Const.KEY_REMOVE_PIN_CODE -> {
                tvTitle.text = resources.getString(R.string.remove_pin_code)
            }
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            btnBack -> {
                goBack()
            }

            txtPin0 -> {
                setTextView("0")
            }
            txtPin1 -> {
                setTextView("1")
            }
            txtPin2 -> {
                setTextView("2")
            }
            txtPin3 -> {
                setTextView("3")
            }
            txtPin4 -> {
                setTextView("4")
            }
            txtPin5 -> {
                setTextView("5")
            }
            txtPin6 -> {
                setTextView("6")
            }
            txtPin7 -> {
                setTextView("7")
            }
            txtPin8 -> {
                setTextView("8")
            }
            txtPin9 -> {
                setTextView("9")
            }

            imgOk -> {
                when (mKeyAction) {
                    Const.KEY_SETTING_PIN_CODE -> {
                        handleSettingPinCode()
                    }

                    Const.KEY_CHANGE_PIN_CODE -> {
                        handleChangePinCode()
                    }

                    Const.KEY_REMOVE_PIN_CODE -> {
                        handleRemovePinCode()
                    }
                }


            }

            imgDel -> {
                if (mOrder > 0) {
                    mOrder -= 1
                }
                setTextView("")
            }

        }

    }

    private fun setTextView(number: String?) {
        when (mOrder) {
            -1 -> {
                edtPinCode1.setText("")
                edtPinCode2.setText("")
                edtPinCode3.setText("")
                edtPinCode4.setText("")
                mOrder = 0
            }
            0 -> {
                edtPinCode1.setText(number)
            }
            1 -> {
                edtPinCode2.setText(number)
            }
            2 -> {
                edtPinCode3.setText(number)
            }
            3 -> {
                edtPinCode4.setText(number)
            }
        }
        if (!number.equals("")) {
            mOrder += 1
            if (mOrder > 4) {
                mOrder -= 1
            }
        }
        updateImgOk()
    }

    private fun updateInputNotice() {
        LogUtil.d("updateInputNotice mStepToSettingPinCode= $mStepToSettingPinCode")
        when (mStepToSettingPinCode) {
            -1 -> {
                txtInputPinCode.text = resources.getString(R.string.validate_pin_code)
                mStepToSettingPinCode = 0
            }
            0 -> {
                txtInputPinCode.text = resources.getString(R.string.input_pin_code)
            }
            1 -> {
                txtInputPinCode.text = resources.getString(R.string.re_input_pin_code)
            }

            2 -> {
                txtInputPinCode.text = resources.getString(R.string.re_input_new_pin_code)
            }
        }
    }

    private fun updateImgOk() {
        if (edtPinCode4.text.isNotEmpty()) {
            imgOk.setImageResource(R.drawable.ic_img_ok_normal)
        } else {
            imgOk.setImageResource(R.drawable.ic_img_ok_disable)
        }
    }

    private fun savePinCode() {
        LogUtil.d("pinCode= $mFirstPinCode")
        SpManager.getInstance().putString(Const.KEY_PIN_CODE, mFirstPinCode)
    }

    private fun getPinCode(): String = edtPinCode1.text.toString().plus(edtPinCode2.text.toString())
        .plus(edtPinCode3.text.toString()).plus(edtPinCode4.text.toString())

    private fun handleSettingPinCode() {
        if (mStepToSettingPinCode == 0) {
            mFirstPinCode = getPinCode()
            mStepToSettingPinCode = 1
            updateInputNotice()
            mOrder = -1
            setTextView("")
        } else if (mStepToSettingPinCode == 1) {
            mStepToSettingPinCode = 0

            if (mFirstPinCode == getPinCode()) {
                savePinCode()
                showMessage(resources.getString(R.string.pin_code_saved))
                goBack()
            } else {
                updateInputNotice()
                mOrder = -1
                setTextView("")
                showMessage(resources.getString(R.string.validate_pin_code))
            }

        }
    }

    private fun handleChangePinCode() {
        if (mCurrentPinCode == getPinCode() && mStepToChangePinCode == 0) {
            mStepToChangePinCode = 1
            updateInputNoticeForChangePinCode()
            mOrder = -1
            setTextView("")
        } else if (mStepToChangePinCode == 1) {
            mFirstPinCode = getPinCode()
            mStepToChangePinCode = 2
            updateInputNoticeForChangePinCode()
            mOrder = -1
            setTextView("")
        } else if (mStepToChangePinCode == 2) {
            mStepToChangePinCode = 0
            if (mFirstPinCode == getPinCode()) {
                savePinCode()
                showMessage(resources.getString(R.string.pin_code_saved))
                goBack()
            } else {
                updateInputNoticeForChangePinCode()
                mFirstPinCode = ""
                mOrder = -1
                setTextView("")
                showMessage(resources.getString(R.string.validate_pin_code))
            }
        } else {
            mOrder = -1
            setTextView("")
            showMessage(resources.getString(R.string.validate_pin_code))
        }
    }

    private fun updateInputNoticeForChangePinCode() {
        LogUtil.d("updateInputNoticeForChangePinCode mStepToChangePinCode= $mStepToChangePinCode")
        when (mStepToChangePinCode) {
            -1 -> {
                txtInputPinCode.text = resources.getString(R.string.validate_pin_code)
            }
            0 -> {
                txtInputPinCode.text = resources.getString(R.string.input_pin_code)
            }
            1 -> {
                txtInputPinCode.text = resources.getString(R.string.input_new_pin_code)
            }

            2 -> {
                txtInputPinCode.text = resources.getString(R.string.re_input_new_pin_code)
            }
        }
    }

    private fun handleRemovePinCode() {
        mFirstPinCode = getPinCode()
        if (mCurrentPinCode == getPinCode()) {
            val deleteNotePopup = DeletePinCodePopup(this)
            deleteNotePopup.displayDeleteNotePopup(activity!!)
        } else {
            mFirstPinCode = ""
            mOrder = -1
            setTextView("")
            showMessage(resources.getString(R.string.validate_pin_code))
        }
    }


    override fun handleActionNo() {

    }

    override fun handleActionYes() {
        mFirstPinCode = ""
        savePinCode()
        showMessage(resources.getString(R.string.pin_code_removed))
        goBack()
    }

    override fun getPopupTitle(): String = resources.getString(R.string.confirm_remove_pin_code)

}

package com.cob.tictactoe.ui.settings.popupremovepincode

interface DeletePinCodeIpl {
    fun getPopupTitle(): String

    fun handleActionYes()

    fun handleActionNo()
}
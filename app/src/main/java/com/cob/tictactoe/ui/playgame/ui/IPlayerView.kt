package com.cob.tictactoe.ui.playgame.ui

interface IPlayerView {
    fun isPlayVsCom(b: Boolean)
    fun updateCustomActionBarColor(color: Int)
    fun updateUIFromTimer(progress: Long)
    fun timerFinish()
    fun displayUndoReDo(visibility: Int)

    fun cancelCountDownTimer()
    fun updateMusicView(b: Boolean)
    fun updateSoundView(b: Boolean)
    fun displayJoinGamePopup(color: Int)
    fun colorType(color: Int)
    fun updateUndoTimes(times: Int)
}
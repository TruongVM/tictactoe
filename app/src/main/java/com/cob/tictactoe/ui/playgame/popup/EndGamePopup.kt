package com.cob.tictactoe.ui.playgame.popup

import android.content.Context
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.cob.tictactoe.R
import kotlinx.android.synthetic.main.dialog_end_game.view.*
import kotlinx.android.synthetic.main.dialog_ready_to_start.view.*

class EndGamePopup(context: Context, endGamePopupIpl: EndGamePopupIpl, winner: String, color: Int) {
    private var mBuilder: AlertDialog.Builder
    private lateinit var mAlertDialog: AlertDialog

    init {
        //Inflate the dialog with custom view
        val mDialogView =
            LayoutInflater.from(context).inflate(R.layout.dialog_end_game, null)
        //AlertDialogBuilder
        mBuilder = AlertDialog.Builder(context)
            .setView(mDialogView)
        //show dialog

        mBuilder.setCancelable(false)
        mDialogView.txtEndGameNotice.setTextColor(
            ContextCompat.getColorStateList(
                context,
                color
            )
        )
        mDialogView.txtCloseAndNewGame.setTextColor(
            ContextCompat.getColorStateList(
                context,
                color
            )
        )
        mDialogView.txtOnlyClose.setTextColor(
            ContextCompat.getColorStateList(
                context,
                color
            )
        )

        mDialogView.view.setBackgroundColor(color)
        mDialogView.txtEndGameNotice.text =
            String.format(context.getString(R.string.winner_on_popup, winner))
        //login button click of custom layout
        mDialogView.txtOnlyClose.setOnClickListener {
            //dismiss dialog
            mAlertDialog.dismiss()
            endGamePopupIpl.closeWithoutAnyThing()
        }
        mDialogView.txtCloseAndNewGame.setOnClickListener {
            //dismiss dialog
            mAlertDialog.dismiss()
            endGamePopupIpl.closeAndNewGame()
        }

    }

    fun showPopup() {
        mAlertDialog = mBuilder.create()
        mAlertDialog.window?.attributes?.windowAnimations = R.style.DialogTheme
        mAlertDialog.show()
    }
}
package com.cob.tictactoe.ui.playgame.popup

interface EndGamePopupIpl {
    fun closeWithoutAnyThing()
    fun closeAndNewGame()
}
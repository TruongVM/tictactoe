package com.cob.tictactoe.ui.settings.popupremovepincode

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface

class DeletePinCodePopup(var deleteNotePopupIpl: DeletePinCodeIpl) {

    fun displayDeleteNotePopup(activity: Activity) {
        lateinit var dialog: AlertDialog
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(deleteNotePopupIpl.getPopupTitle())
        builder.setMessage("")
        val dialogClickListener = DialogInterface.OnClickListener { _, which ->
            run {
                when (which) {
                    DialogInterface.BUTTON_POSITIVE -> {
                        deleteNotePopupIpl.handleActionYes()
                    }
                    DialogInterface.BUTTON_NEGATIVE -> {
                        deleteNotePopupIpl.handleActionNo()
                    }
                }
            }

        }
        builder.setPositiveButton("Yes", dialogClickListener)
        builder.setNegativeButton("No", dialogClickListener)


        dialog = builder.create()
        dialog.show()
    }
}
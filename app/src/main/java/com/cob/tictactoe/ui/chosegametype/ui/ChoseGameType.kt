package com.cob.tictactoe.ui.chosegametype.ui

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.wifi.p2p.WifiP2pDevice
import android.net.wifi.p2p.WifiP2pInfo
import android.net.wifi.p2p.WifiP2pManager
import android.os.Bundle
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.cob.tictactoe.R
import com.cob.tictactoe.basse.fragments.BaseFragment
import com.cob.tictactoe.receiver.DirectActionListener
import com.cob.tictactoe.receiver.DirectBroadcastReceiver
import com.cob.tictactoe.ui.chosegametype.adapter.ListDeviceAdapter
import com.cob.tictactoe.ui.chosegametype.presenter.ChoseGameTypePresenter
import com.cob.tictactoe.ui.chosegametype.presenter.IChoseGameTypePresenter
import com.cob.tictactoe.ui.playgame.ui.PlayGameActivity
import com.cob.tictactoe.utils.Const
import com.cob.tictactoe.utils.LogUtil
import kotlinx.android.synthetic.main.activity_chose_game_type.*
import kotlinx.android.synthetic.main.layout_avatar_user.*

class ChoseGameType : BaseFragment(), View.OnClickListener, DirectActionListener,
    ChoseGameTypeView {


    private lateinit var mChoseGameTypePresenter: IChoseGameTypePresenter

    private var mGameSteps = 5

    private var mGameType = 1

    private var mColorForType = -1

    private var mWifiP2pEnable = false

    private val CODE_CHOOSE_FILE = 100

    private var wifiP2pManager: WifiP2pManager? = null

    private var channel: WifiP2pManager.Channel? = null

    private var mWifiP2pInfo: WifiP2pInfo? = null

    private var broadcastReceiver: BroadcastReceiver? = null

    private var mWifiP2pDevice: WifiP2pDevice? = null

    private val CODE_REQ_PERMISSIONS = 665

    private lateinit var mListDeviceAdapter: ListDeviceAdapter

    private val TYPE_IN_THIS_DEVICE = 0

    private val TYPE_IN_VIA_WIFI = 1
    private var mGameLevel = Const.mLevelHard

    private lateinit var mListTextView: ArrayList<TextView>

    override fun getContentRes(): Int = R.layout.activity_chose_game_type

    override fun actionBack(): Boolean = false


    companion object {

        fun newInstance(data: Bundle): ChoseGameType {
            val fragment =
                ChoseGameType()
            val bundle = Bundle()
            bundle.putInt(Const.KEY_PLAY_TYPE, data.getInt(Const.KEY_PLAY_TYPE))
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkPermission()
        initEventForWifiP2p()

        mChoseGameTypePresenter = ChoseGameTypePresenter(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        activity!!.unregisterReceiver(broadcastReceiver)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imgBack.setOnClickListener(this)
        imgStartPlayGame.setOnClickListener(this)

        mListTextView = ArrayList()
        mListTextView.add(txtLevelEasy)
        mListTextView.add(txtLevelMedium)
        mListTextView.add(txtLevelHard)
        mListTextView.add(txtThisDevice)
        mListTextView.add(txtViaWifi)
        registerOnClick()
        mGameType = arguments!!.getInt(Const.KEY_PLAY_TYPE)
        mColorForType =
            if (mGameType == 1) R.color.champion_color else R.color.game_mode_two_player_color
        updateViewColorForEachType(mColorForType)

        if (mGameType == 1) {
            updateTextViewStatus(Const.mLevelHard)
            rltPlayVcCom.visibility = View.VISIBLE
            rltPlayerVsPlayer.visibility = View.GONE

        } else {
            updateTextViewStatus(Const.mPlayOnThisDevice)
            rltPlayVcCom.visibility = View.GONE
            rltPlayerVsPlayer.visibility = View.VISIBLE
        }


        updateCheckbox(txtPlayer1Name.text.toString())
        txtPlayer1Name.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                updateCheckbox(s.toString())
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                updateCheckbox(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
        })
        displayViewForEachMode(TYPE_IN_THIS_DEVICE)

        mListDeviceAdapter = ListDeviceAdapter(activity!!, ::onItemNoteClick)
        rclListDevice.adapter = mListDeviceAdapter
        rclListDevice.layoutManager = GridLayoutManager(activity!!, 1)
    }

    private fun onItemNoteClick(device: WifiP2pDevice, connectionStatus: Int) {
        if (connectionStatus == WifiP2pDevice.CONNECTED || connectionStatus == WifiP2pDevice.INVITED) {
            disConnect()
        } else {
            mWifiP2pDevice = device
            connect()
        }

    }

    private fun connect() {
        mChoseGameTypePresenter.connectWifiP2p(mWifiP2pDevice!!, channel!!, wifiP2pManager!!)
    }

    private fun disConnect() {
        mChoseGameTypePresenter.disConnectWifiP2p(channel!!, wifiP2pManager!!)
    }

    private fun registerOnClick() {
        txtLevelEasy.setOnClickListener(this)
        txtLevelMedium.setOnClickListener(this)
        txtLevelHard.setOnClickListener(this)
        txtThisDevice.setOnClickListener(this)
        txtViaWifi.setOnClickListener(this)
        txtEnableWifiDirect.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            imgBack -> {
                goBack()
            }
            imgStartPlayGame -> {
                startPlayGameActivity()
            }


            txtViaWifi -> {
                showMessage("Developing Function")
//                updateTextViewStatus(Const.mPlayOnline)
//                discoverWifiP2p()
//                displayViewForEachMode(TYPE_IN_VIA_WIFI)
            }

            txtThisDevice -> {
                displayViewForEachMode(TYPE_IN_THIS_DEVICE)
                updateTextViewStatus(Const.mPlayOnThisDevice)
            }

            txtLevelHard -> {
                mGameLevel = Const.mLevelHard
                updateTextViewStatus(Const.mLevelHard)
            }

            txtLevelMedium -> {
                mGameLevel = Const.mLevelMedium
                updateTextViewStatus(Const.mLevelMedium)
            }

            txtLevelEasy -> {
                mGameLevel = Const.mLevelEasy
                updateTextViewStatus(Const.mLevelEasy)
            }


            txtEnableWifiDirect -> {
                openWifiSetting()
            }

        }
    }

    private fun openWifiSetting() {
        if (wifiP2pManager != null && channel != null) {
            startActivity(Intent(Settings.ACTION_WIFI_SETTINGS))
        } else {
            showMessage(R.string.not_support_wifi_direct)
        }
    }

    private fun startPlayGameActivity() {
        val bundle = Bundle()
        bundle.putInt(Const.KEY_GAME_STEPS, mGameSteps)
        bundle.putInt(Const.KEY_PLAY_TYPE, mGameType)
        bundle.putInt(Const.KEY_COLOR_TYPE, mColorForType)
        bundle.putInt(Const.KEY_GAME_LEVEL, mGameLevel)
        bundle.putBoolean(Const.KEY_IS_PLAYER_1_GO_FIRST, ckPlayerGoFirst.isChecked)
        replaceFragment(PlayGameActivity.newInstance(bundle))
    }

    private fun updateViewColorForEachType(color: Int) {
        rltCustomActionBar.backgroundTintList = ContextCompat.getColorStateList(
            activity!!,
            color
        )
        for (i in mListTextView.indices) {
            mListTextView[i].setTextColor(
                ContextCompat.getColorStateList(
                    activity!!,
                    color
                )
            )
        }

        imgStartPlayGame.backgroundTintList = ContextCompat.getColorStateList(
            activity!!,
            color
        )

        txtEnableWifiDirect.backgroundTintList = ContextCompat.getColorStateList(
            activity!!,
            color
        )

        txtPlayer1Name.setTextColor(
            ContextCompat.getColorStateList(
                activity!!,
                color
            )
        )

        txtPlayer2Name.setTextColor(
            ContextCompat.getColorStateList(
                activity!!,
                color
            )
        )


    }

    private fun updateTextViewStatus(position: Int) {
        for (i in mListTextView.indices) {
            LogUtil.d("i= " + mListTextView[i])
            mListTextView[i].isSelected = i == position

        }

    }

    private fun updateCheckbox(userName: String) {
        ckPlayerGoFirst.text = String.format(getString(R.string.who_go_first, userName))
    }

    private fun initEventForWifiP2p() {
        wifiP2pManager = activity!!.getSystemService(Context.WIFI_P2P_SERVICE) as WifiP2pManager
        channel = wifiP2pManager!!.initialize(activity!!, activity!!.mainLooper, this)
        broadcastReceiver = DirectBroadcastReceiver(wifiP2pManager!!, channel!!, this)
        activity!!.registerReceiver(broadcastReceiver, DirectBroadcastReceiver.getIntentFilter())

    }

    override fun wifiP2pEnabled(enabled: Boolean) {
        Log.d("TruongVM", "onConnectionInfoAvailable")
        mWifiP2pEnable = enabled
    }

    override fun onConnectionInfoAvailable(wifiP2pInfo: WifiP2pInfo?) {
        Log.d("TruongVM", " onConnectionInfoAvailable ")
        if (wifiP2pInfo!!.groupFormed && wifiP2pInfo.isGroupOwner) {
            mWifiP2pInfo = wifiP2pInfo
        }
    }

    override fun onDisconnection() {
        Log.d("TruongVM", "onDisconnection")
        mWifiP2pInfo = null
    }

    override fun onPeersAvailable(wifiP2pDeviceList: Collection<WifiP2pDevice?>?) {
        val wifiP2pDeviceListSize = wifiP2pDeviceList?.size
        Log.d("TruongVM", "onPeersAvailable wifiP2pDeviceList size= $wifiP2pDeviceListSize")
        if (wifiP2pDeviceListSize == null || wifiP2pDeviceListSize <= 0) {
            llFindingAvailableDevices.visibility = View.VISIBLE
        } else {
            llFindingAvailableDevices.visibility = View.GONE
        }
        mListDeviceAdapter.updateList(wifiP2pDeviceList)

    }

    override fun onSelfDeviceAvailable(wifiP2pDevice: WifiP2pDevice?) {
        Log.d("TruongVM", "onSelfDeviceAvailable")
    }

    override fun onChannelDisconnected() {
        Log.d("TruongVM", "onChannelDisconnected")
    }

    private fun discoverWifiP2p() {
        if (!mWifiP2pEnable) {
            showMessage(R.string.turn_on_wifi_first)
            return
        }

        wifiP2pManager?.discoverPeers(channel, object : WifiP2pManager.ActionListener {
            override fun onSuccess() {
                showMessage(R.string.discover_peer_success)
                Log.d("TruongVM", "onSuccess")
            }

            override fun onFailure(reason: Int) {
                showMessage(R.string.discover_peer_fail)
                Log.d("TruongVM", "onFailure")
            }
        })

    }

    private fun checkPermission() {
        ActivityCompat.requestPermissions(
            activity!!, arrayOf(
                Manifest.permission.CHANGE_NETWORK_STATE,
                Manifest.permission.ACCESS_NETWORK_STATE,
                Manifest.permission.ACCESS_WIFI_STATE,
                Manifest.permission.CHANGE_WIFI_STATE,
                Manifest.permission.ACCESS_FINE_LOCATION
            ), CODE_REQ_PERMISSIONS
        )
    }


    private fun displayViewForEachMode(type: Int) {
        //0 is on this device and 1 is wifi
        if (type == 0) {
            rltListDevice.visibility = View.GONE
            rltPlayerVsCom.visibility = View.VISIBLE
            ckPlayerGoFirst.visibility = View.VISIBLE
            imgStartPlayGame.visibility = View.VISIBLE
        } else if (type == 1) {
            rltListDevice.visibility = View.VISIBLE
            rltPlayerVsCom.visibility = View.GONE
            ckPlayerGoFirst.visibility = View.GONE
            imgStartPlayGame.visibility = View.GONE
        }

    }

    override fun showNoticeMessage(messageId: Int) {
        showMessage(messageId)
    }


}
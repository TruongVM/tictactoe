package com.cob.tictactoe.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import com.cob.tictactoe.R
import com.cob.tictactoe.basse.activities.BaseGameActivity
import com.cob.tictactoe.basse.effect.BackgroundSoundEffect
import com.cob.tictactoe.ui.chosegametype.ui.ChoseGameType
import com.cob.tictactoe.utils.Const
import kotlinx.android.synthetic.main.activity_main_game.*

class MainMenuGameActivity : BaseGameActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_main_game)
        btnPlayerVsCom.setOnClickListener(this)
        btnPlayerVsPlayer.setOnClickListener(this)
        btnSetting.setOnClickListener(this)
        btnOtherApp.setOnClickListener(this)
        btnRateApp.setOnClickListener(this)
//        BackgroundSoundEffect.getInstance().playSoundEffect()
    }

    override fun onClick(v: View?) {
        when (v) {
            btnPlayerVsCom -> {
//                startAnActivity(ChoseGameType(), 1)
            }
            btnPlayerVsPlayer -> {
//                startAnActivity(ChoseGameType(), 2)
            }
            btnSetting -> {
            }
            btnOtherApp -> {
            }
            btnRateApp -> {
            }
        }
    }

    private fun startAnActivity(activity: AppCompatActivity, type: Int) {
        val intent = Intent(this, activity::class.java)
        intent.putExtra(Const.KEY_PLAY_TYPE, type)
        startActivity(intent)

    }

    override fun onPause() {
        super.onPause()
        BackgroundSoundEffect.getInstance().pauseSoundEffect()
    }
}

package com.cob.tictactoe.ui.playgame.popup

import android.content.Context
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.cob.tictactoe.R
import kotlinx.android.synthetic.main.dialog_end_game.view.*
import kotlinx.android.synthetic.main.dialog_pause_game.view.*
import kotlinx.android.synthetic.main.dialog_pause_game.view.txtEndGameNotice
import kotlinx.android.synthetic.main.dialog_pause_game.view.view

class PauseGamePopup(context: Context, pauseGamePopupIpl: PauseGamePopupIpl, color: Int) {
    private var mBuilder: AlertDialog.Builder
    private lateinit var mAlertDialog: AlertDialog

    init {
        //Inflate the dialog with custom view
        val mDialogView =
            LayoutInflater.from(context).inflate(R.layout.dialog_pause_game, null)
        //AlertDialogBuilder
        mBuilder = AlertDialog.Builder(context)
            .setView(mDialogView)
        //show dialog

        mBuilder.setCancelable(false)

        mDialogView.txtNewGame.setTextColor(
            ContextCompat.getColorStateList(
                context,
                color
            )
        )
        mDialogView.txtResumeGame.setTextColor(
            ContextCompat.getColorStateList(
                context,
                color
            )
        )
        mDialogView.txtEndGameNotice.setTextColor(
            ContextCompat.getColorStateList(
                context,
                color
            )
        )

        mDialogView.view.setBackgroundColor(color)
        //login button click of custom layout
        mDialogView.txtNewGame.setOnClickListener {
            //dismiss dialog
            mAlertDialog.dismiss()
            pauseGamePopupIpl.closePopupAndNewGame()
        }
        mDialogView.txtResumeGame.setOnClickListener {
            //dismiss dialog
            mAlertDialog.dismiss()
            pauseGamePopupIpl.closePopupResumeGame()
        }

    }

    fun showPopup() {
        mAlertDialog = mBuilder.create()
        mAlertDialog.window?.attributes?.windowAnimations = R.style.DialogTheme
        mAlertDialog.show()
    }
}
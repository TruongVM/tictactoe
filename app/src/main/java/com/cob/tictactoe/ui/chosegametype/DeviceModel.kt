package com.cob.tictactoe.ui.chosegametype

data class DeviceModel(
    var deviceName: String,
    var deviceAddress: String,
    var detailDevice: String
) {
}
package com.cob.tictactoe.ui.sflashscreen.view

import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.cob.tictactoe.MainActivity
import com.cob.tictactoe.R
import kotlinx.android.synthetic.main.activity_sflash.*

class SflashActivity : AppCompatActivity() {

    private lateinit var mIntent: Intent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sflash)
        val frameAnimation: AnimationDrawable = imgAvatarGame.background as AnimationDrawable

        frameAnimation.start()

        mIntent =
            Intent(this.applicationContext, MainActivity::class.java)


        Handler().postDelayed({
            //Do something after 100ms
            startActivity(mIntent)
            finish()
        }, 6000)

    }

}

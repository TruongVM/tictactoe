package com.cob.tictactoe.ui.playgame.popup

interface JoinGamePopupIpl {
    fun joinGame()
    fun cancelGame()
}
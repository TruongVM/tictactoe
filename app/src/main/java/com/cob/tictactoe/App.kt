package com.cob.tictactoe

import android.app.Application
import com.cob.tictactoe.basse.effect.BackgroundSoundEffect
import com.cob.tictactoe.basse.effect.SoundEffect
import com.cob.tictactoe.utils.AppUtils
import com.cob.tictactoe.utils.SpManager
import org.koin.android.ext.android.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        SpManager.getInstance().init(applicationContext, AppUtils.KEY_PREF)
        startKoin(this, listOf(appModule))
        SoundEffect.getInstance().initPlayer(this)
        BackgroundSoundEffect.getInstance().initPlayer(this)
    }
}
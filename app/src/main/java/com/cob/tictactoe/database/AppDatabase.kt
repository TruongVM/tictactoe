package com.cob.tictactoe.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.cob.tictactoe.ui.home.model.NoteModel

@Database(entities = [NoteModel::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun noteDao(): NoteDao
}
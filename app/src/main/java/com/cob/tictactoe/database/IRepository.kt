package com.cob.tictactoe.database

import com.cob.tictactoe.ui.home.model.NoteModel
import io.reactivex.Observable

interface IRepository {

    fun insert(note: NoteModel)

    fun getAllNote(): Observable<List<NoteModel>>

    fun getNoteById(noteID: Int): Observable<List<NoteModel>>

    fun deleteNote(note: NoteModel)

    fun deleteAllNote()

    fun updateNote(note: NoteModel)
}
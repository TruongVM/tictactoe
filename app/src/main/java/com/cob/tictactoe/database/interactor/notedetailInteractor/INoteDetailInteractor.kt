package com.cob.tictactoe.database.interactor.notedetailInteractor

import com.cob.tictactoe.ui.home.model.NoteModel

interface INoteDetailInteractor {

    fun insertNote(note: NoteModel)

    fun updateNote(note: NoteModel)
}
package com.cob.tictactoe.database.interactor

import com.cob.tictactoe.ui.home.model.NoteModel
import io.reactivex.Observable

interface INoteInteractor {

    fun loadAllNote(): Observable<List<NoteModel>>
    fun delete(note: NoteModel)
    fun deleteAllNote()

}
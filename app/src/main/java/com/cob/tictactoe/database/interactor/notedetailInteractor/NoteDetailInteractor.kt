package com.cob.tictactoe.database.interactor.notedetailInteractor

import com.cob.tictactoe.database.IRepository
import com.cob.tictactoe.ui.home.model.NoteModel

class NoteDetailInteractor(val iRepository: IRepository) : INoteDetailInteractor {


    override fun updateNote(note: NoteModel) {
        iRepository.updateNote(note)
    }


    override fun insertNote(note: NoteModel) {
        iRepository.insert(note)
    }
}
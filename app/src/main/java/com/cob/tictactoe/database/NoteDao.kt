package com.cob.tictactoe.database

import androidx.room.*
import com.cob.tictactoe.ui.home.model.NoteModel
import io.reactivex.Completable
import io.reactivex.Observable

@Dao
interface NoteDao {

    @Insert
    fun insert(note: NoteModel): Completable

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(note: NoteModel): Completable

    @Query("SELECT * FROM list_note")
    fun getAllNote(): Observable<List<NoteModel>>


    @Query("SELECT * FROM list_note WHERE noteID= :noteID")
    fun getNoteById(noteID: Int): Observable<List<NoteModel>>

    @Query("DELETE FROM list_note WHERE noteID=:noteID")
    fun deleteNoteById(noteID: Int): Completable

    @Query("DELETE  FROM list_note")
    fun deleteAllNote(): Completable
}
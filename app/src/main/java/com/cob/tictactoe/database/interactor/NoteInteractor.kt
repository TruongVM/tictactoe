package com.cob.tictactoe.database.interactor

import com.cob.tictactoe.database.IRepository
import com.cob.tictactoe.ui.home.model.NoteModel
import io.reactivex.Observable

class NoteInteractor(val iRepository: IRepository) : INoteInteractor {

    override fun delete(note: NoteModel) {
        iRepository.deleteNote(note)
    }

    override fun loadAllNote(): Observable<List<NoteModel>> {
        return iRepository.getAllNote()
    }

    override fun deleteAllNote() {
        iRepository.deleteAllNote()
    }
}
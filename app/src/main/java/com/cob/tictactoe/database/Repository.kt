package com.cob.tictactoe.database

import com.cob.tictactoe.ui.home.model.NoteModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class Repository(var noteDao: NoteDao) : IRepository {


    override fun insert(note: NoteModel) {
        noteDao.insert(note).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    override fun updateNote(note: NoteModel) {
        noteDao.update(note).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }


    override fun getAllNote(): Observable<List<NoteModel>> {
        return noteDao.getAllNote()
    }

    override fun getNoteById(noteID: Int): Observable<List<NoteModel>> {
        return noteDao.getNoteById(noteID)
    }


    override fun deleteAllNote() {
        noteDao.deleteAllNote().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe()
    }

    override fun deleteNote(note: NoteModel) {
        noteDao.deleteNoteById(note.noteID!!).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }


}
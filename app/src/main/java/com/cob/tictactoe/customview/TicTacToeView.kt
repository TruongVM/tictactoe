package com.cob.tictactoe.customview

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.cob.tictactoe.R
import com.cob.tictactoe.utils.AppUtils
import com.cob.tictactoe.utils.Const


class TicTacToeView : View, ValueAnimator.AnimatorUpdateListener {

    private var mScaleDetector: ScaleGestureDetector? = null
    private var mScaleFactor = 1f

    constructor(ctx: Context) : super(ctx)

    constructor(ctx: Context, attrs: AttributeSet) : super(ctx, attrs)

    private val paint = Paint()
    private val animatePaint = Paint()
    private val textPaint = Paint()
    private val highLightPaint = Paint()
    private val path = Path()
    private lateinit var squares: Array<Array<Rect>>
    private lateinit var squareData: Array<Array<String>>
    var squarePressListener: SquarePressedListener? = null


    val COUNT = Const.NET_SIZE
    var X_PARTITION_RATIO = 0f
    var Y_PARTITION_RATIO = 0f
    var rectIndex = Pair(0, 0)
    var touching: Boolean = false
    var winCoordinates: Array<Int> = Array(4, { -1 })
    var shouldAnimate = false


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val size = Math.min(measuredHeight, measuredWidth)
        setMeasuredDimension(size, size)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        init()
    }

    private fun init() {
        mScaleDetector = ScaleGestureDetector(
            context,
            ScaleListener()
        )
        val screenWidth = AppUtils.getScreenWidth()
        val screenHeight = AppUtils.getScreenHeight()
        var originalSize = 0
        originalSize = if (screenWidth < screenHeight) screenWidth else screenHeight
        X_PARTITION_RATIO = (originalSize / Const.NET_SIZE).toFloat()
        Y_PARTITION_RATIO = (originalSize / Const.NET_SIZE).toFloat()

        paint.color = ContextCompat.getColor(context, R.color.colorPrimary)
        paint.isAntiAlias = true
        paint.style = Paint.Style.STROKE
        paint.strokeWidth = 2F

        animatePaint.color = paint.color
        animatePaint.isAntiAlias = paint.isAntiAlias
        animatePaint.style = paint.style
        animatePaint.strokeWidth = paint.strokeWidth

        textPaint.color = paint.color
        textPaint.isAntiAlias = true
        textPaint.typeface = ResourcesCompat.getFont(context, R.font.baloothambi)
        textPaint.textSize = resources.displayMetrics.scaledDensity * 10

        highLightPaint.color = ContextCompat.getColor(context, R.color.ripple_material_light)
        highLightPaint.style = Paint.Style.FILL
        highLightPaint.isAntiAlias = true
        initializeTicTacToeSquares()
    }

    inner class ScaleListener :
        ScaleGestureDetector.SimpleOnScaleGestureListener() {
        override fun onScale(detector: ScaleGestureDetector): Boolean {
            mScaleFactor *= detector.scaleFactor
            mScaleFactor = Math.max(
                1f,
                Math.min(mScaleFactor, 2.0f)
            )
            invalidate()
            return true
        }
    }

    private fun initializeTicTacToeSquares() {
        squares = Array(Const.NET_SIZE) { Array(Const.NET_SIZE) { Rect() } }
        squareData = Array(Const.NET_SIZE) { Array(Const.NET_SIZE) { "" } }

    }

    private fun setCoordinateForPosition() {
        val xUnit = (X_PARTITION_RATIO).toInt() // one unit on x-axis
        val yUnit = (Y_PARTITION_RATIO).toInt() // one unit on y-axis

        for (j in 0 until COUNT) {
            for (i in 0 until COUNT) {
                squares[i][j] = Rect(j * xUnit, i * yUnit, (j + 1) * xUnit, (i + 1) * yUnit)
            }

        }

    }

    private var mPosX = 0f
    private var mPosY = 0f
    private var mLastTouchX = 0f
    private var mLastTouchY = 0f

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (!isEnabled) {
            return false
        }

        val x = event.x
        val y = event.y
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                rectIndex = getRectIndexesFor(x, y)
                touching = true
                invalidate(squares[rectIndex.first][rectIndex.second])
                mLastTouchX = event.x
                mLastTouchY = event.y
            }
            MotionEvent.ACTION_MOVE -> {
                touching = false

                val xMove = event.x
                val yMove = event.y
                if (!mScaleDetector!!.isInProgress) {
                    val dx = xMove - mLastTouchX
                    val dy = yMove - mLastTouchY
                    mPosX += dx
                    mPosY += dy
                    Log.d("TruongVM", "mPosX= $mPosX mPosY= $mPosY")

                    invalidate(squares[rectIndex.first][rectIndex.second])
                }

                mLastTouchX = xMove
                mLastTouchY = yMove
            }
            MotionEvent.ACTION_UP -> {
                touching = false
                invalidate(squares[rectIndex.first][rectIndex.second])
                val (finalX1, finalY1) = getRectIndexesFor(x, y)

                if ((finalX1 == rectIndex.first) && (finalY1 == rectIndex.second)) { // if initial touch and final touch is in same rectangle or not
                    squarePressListener?.onSquarePressed(rectIndex.first, rectIndex.second)
                }

            }
            MotionEvent.ACTION_CANCEL -> {
                touching = false
            }

        }
        mScaleDetector!!.onTouchEvent(event)
        return true
    }

    fun getRectIndexesFor(x: Float, y: Float): Pair<Int, Int> {
        Log.d("getRectIndexesFor", "x= $x y= $y")
        squares.forEachIndexed { i, rects ->
            for ((j, rect) in rects.withIndex()) {
                if (rect.contains(x.toInt(), y.toInt()))
                    return Pair(i, j)
            }
        }
        return Pair(-1, -1) // x, y do not lie in our view
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.save()
        X_PARTITION_RATIO = (AppUtils.getScreenWidth() / Const.NET_SIZE).toFloat()
        Y_PARTITION_RATIO = (AppUtils.getScreenWidth() / Const.NET_SIZE).toFloat()
        Y_PARTITION_RATIO *= mScaleFactor
        X_PARTITION_RATIO *= mScaleFactor
//        if (!mScaleDetector!!.isInProgress) {
//            if (mScaleFactor > 1) {
//                canvas.translate(mPosX, mPosY)
//            } else {
//                canvas.translate(0f, 0f)
//            }
//        }
        setCoordinateForPosition()
        drawVerticalLines(canvas)
        drawHorizontalLines(canvas)
        drawSquareStates(canvas)
        if (shouldAnimate) {
            canvas.drawPath(path, animatePaint)
        }
        if (touching) {
            drawHighlightRectangle(canvas)
        }
        canvas.restore()
    }

    private fun animateWin() {
        val valueAnimator = ValueAnimator.ofFloat(1f, 0f)
        valueAnimator.duration = 600
        valueAnimator.addUpdateListener(this)
        valueAnimator.start()
    }

    override fun onAnimationUpdate(animation: ValueAnimator) {
        val measure = PathMeasure(path, false)
        val phase = (measure.length * (animation.animatedValue as Float))
        animatePaint.pathEffect = createPathEffect(measure.length, phase)
        invalidate()
    }

    private fun createPathEffect(pathLength: Float, phase: Float): PathEffect {
        return DashPathEffect(
            floatArrayOf(pathLength, pathLength),
            phase
        )
    }

    private fun drawSquareStates(canvas: Canvas) {
        for ((i, textArray) in squareData.withIndex()) {
            for ((j, text) in textArray.withIndex()) {
                if (text.isNotEmpty()) {
                    drawTextInsideRectangle(canvas, squares[i][j], text)
                }
            }
        }
    }

    private fun drawHighlightRectangle(canvas: Canvas) {
        canvas.drawRect(squares[rectIndex.first][rectIndex.second], highLightPaint)
    }

    private fun drawTextInsideRectangle(canvas: Canvas, rect: Rect, str: String) {
        val xOffset = textPaint.measureText(str) * 0.5f
        val yOffset = textPaint.fontMetrics.ascent * -0.3f
        val textX = (rect.exactCenterX()) - xOffset
        val textY = (rect.exactCenterY()) + yOffset
        canvas.drawText(str, textX, textY, textPaint)
    }

    private fun drawVerticalLines(canvas: Canvas) {
        for (i in 0 until COUNT + 1) {
            canvas.drawLine(
                i * X_PARTITION_RATIO,
                0f,
                i * X_PARTITION_RATIO,
                width.toFloat() * mScaleFactor,
                paint
            )
        }
    }

    private fun drawHorizontalLines(canvas: Canvas) {

        for (i in 0 until COUNT + 1) {
            canvas.drawLine(
                0f,
                i * Y_PARTITION_RATIO,
                width.toFloat() * mScaleFactor,
                i * Y_PARTITION_RATIO,
                paint
            )
        }
    }

    interface SquarePressedListener {
        fun onSquarePressed(i: Int, j: Int)
    }


    fun beatAtPosition(x: Int, y: Int, place: String) {
        squareData[x][y] = place
        invalidate(squares[x][y])
    }

    fun reset() {
        squareData = Array(Const.NET_SIZE) { Array(Const.NET_SIZE) { "" } }
        winCoordinates = Array(4) { -1 }
        path.reset()
        shouldAnimate = false
        invalidate()
    }

    fun animateWin(x1: Int, y1: Int, x3: Int, y3: Int) {
        winCoordinates = arrayOf(x1, y1, x3, y3)
        if (winCoordinates[0] < 0) return
        val centerX = squares[winCoordinates[0]][winCoordinates[1]].exactCenterX()
        val centerY = squares[winCoordinates[0]][winCoordinates[1]].exactCenterY()
        val centerX2 = squares[winCoordinates[2]][winCoordinates[3]].exactCenterX()
        val centerY2 = squares[winCoordinates[2]][winCoordinates[3]].exactCenterY()

        path.reset()
        path.moveTo(centerX, centerY)
        path.lineTo(centerX2, centerY2)
        shouldAnimate = true
        animateWin()
    }

}
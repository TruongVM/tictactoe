package com.cob.tictactoe

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.cob.tictactoe.basse.activities.BaseActivity
import com.cob.tictactoe.basse.fragments.BaseFragment
import com.cob.tictactoe.basse.transaction.StyleAnimation
import com.cob.tictactoe.ui.chosegametype.ui.ChoseGameType
import com.cob.tictactoe.ui.settings.view.SettingFragment
import com.cob.tictactoe.utils.AppUtils
import com.cob.tictactoe.utils.Const
import com.cob.tictactoe.utils.LocaleManager
import com.cob.tictactoe.utils.LogUtil
import kotlinx.android.synthetic.main.activity_main_game.*
import java.util.*

class MainActivity : BaseActivity(), SettingFragment.LanguageChanged,
    View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        settingLanguage()
        setContentView(R.layout.activity_main)
        registerOnClickEven()
    }

    private fun registerOnClickEven() {
        btnPlayerVsCom.setOnClickListener(this)
        btnPlayerVsPlayer.setOnClickListener(this)
        btnSetting.setOnClickListener(this)
        btnOtherApp.setOnClickListener(this)
        btnRateApp.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            btnPlayerVsCom -> {
                val bundle = Bundle()
                bundle.putInt(Const.KEY_PLAY_TYPE, Const.PLAYER_VS_COM)
                startAnActivity(ChoseGameType.newInstance(bundle))
            }
            btnPlayerVsPlayer -> {
                val bundle = Bundle()
                bundle.putInt(Const.KEY_PLAY_TYPE, Const.PLAYER_VS_PLAYER)
                startAnActivity(ChoseGameType.newInstance(bundle))
            }
            btnSetting -> {
                startAnActivity(SettingFragment())
            }
            btnOtherApp -> {
                otherApp()
            }
            btnRateApp -> {
                rateUs()
            }
        }
    }

    private fun startAnActivity(activity: BaseFragment) {
        getTransactionManager().replaceFragment(
            activity,
            isAddBackStack = true,
            styleAnimation = StyleAnimation.SLIDE_FROM_RIGHT
        )
    }

    override fun getPlaceHolder(): Int {
        return R.id.content_frame
    }

    fun hideFloatButton() {
        LogUtil.d("hideFloatButton")
    }

    fun showFloatButton() {
        LogUtil.d("showFloatButton")
    }

    private fun getCurrentForegroundFragment(positionOnNavHost: Int): Fragment? {
        val navHostFragment = supportFragmentManager.primaryNavigationFragment;
        val fragment = navHostFragment?.childFragmentManager?.fragments?.get(positionOnNavHost);
        return fragment
    }

    private fun settingLanguage() {
        LocaleManager.setUpLocale(this, AppUtils.getCurrentLanguage())
    }

    private fun recreateActivity() {
        if (Build.VERSION.SDK_INT in 26..27) {
            if (Locale.getDefault().language == "en" ||
                Locale.getDefault().language == "vi"
            ) {
                LogUtil.d("recreateActivity")
                window.decorView.layoutDirection = View.LAYOUT_DIRECTION_LTR
            } else {
                window.decorView.layoutDirection = View.LAYOUT_DIRECTION_LTR
            }

        }
        recreate()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        LogUtil.d("onActivityResult")
        for (fragment in supportFragmentManager.fragments) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }


    }

    fun setEnableDrawer(isEnable: Boolean) {
        val lockMode =
            if (isEnable) DrawerLayout.LOCK_MODE_UNLOCKED else DrawerLayout.LOCK_MODE_LOCKED_CLOSED
    }


    fun otherApp() {
        val otherPackage = "com.truongvm.smartnote"
        try {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=$otherPackage")
                )
            )
        } catch (e: ActivityNotFoundException) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$otherPackage")
                )
            )
        }
    }

    fun rateUs() {
        try {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=$packageName")
                )
            )
        } catch (e: ActivityNotFoundException) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                )
            )
        }
    }

    override fun onLanguageChanged() {
        recreateActivity()
    }

    interface CallbackForFragment {
        fun deleteAllNote()
    }
}

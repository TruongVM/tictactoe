package com.cob.tictactoe

import androidx.room.Room
import com.cob.tictactoe.database.AppDatabase
import com.cob.tictactoe.database.IRepository
import com.cob.tictactoe.database.Repository
import com.cob.tictactoe.database.interactor.INoteInteractor
import com.cob.tictactoe.database.interactor.NoteInteractor
import com.cob.tictactoe.database.interactor.notedetailInteractor.INoteDetailInteractor
import com.cob.tictactoe.database.interactor.notedetailInteractor.NoteDetailInteractor
import org.koin.dsl.module.module

val appModule = module {
    val DATABASE_NAME = "note_database"
    single { Room.databaseBuilder(get(), AppDatabase::class.java, DATABASE_NAME).build() }

    single<IRepository> {
        Repository(
            get()
        )
    }

    single<INoteInteractor> {
        NoteInteractor(
            get()
        )
    }


    single<INoteDetailInteractor> {
        NoteDetailInteractor(
            get()
        )
    }
    single {
        get<AppDatabase>().noteDao()
    }


}